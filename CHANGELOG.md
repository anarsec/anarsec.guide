# Tails for Anarchists 

This guide has not been changed since publication. 

# Tails Best Practices 

* 2024-11-25: [Link to Tails documentation on how to install Dangerzone](https://0xacab.org/anarsec/anarsec.guide/-/commit/2d6f88baa9cf8d58fad919d7bc76a675755c929c). 

# Qubes OS for Anarchists

This guide has not been changed since publication. 

# Kill the Cop in Your Pocket  

This guide has not been changed since publication. 

# GrapheneOS for Anarchists 

* 2024-11-25: [Recommend the Accrescent app store](https://0xacab.org/anarsec/anarsec.guide/-/commit/b21bc33a60ff3a186908dd1d98d2668ba91b7678), [recommend setting a duress password](https://0xacab.org/anarsec/anarsec.guide/-/commit/40b3b5d0c5b6f00fe0fcd686e06fb7048a5a21ae).

# Linux Essentials 

This guide has not been changed since publication. 

# Removing Identifying Metadata From Files 

This guide has not been changed since publication. 

# Encrypted Messaging for Anarchists 

This guide has not been changed since publication. 

# Making Your Electronics Tamper-Evident

This guide has not been changed since publication. 
