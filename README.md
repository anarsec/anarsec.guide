# AnarSec: Tech Guides for Anarchists 

Built with [Zola](https://www.getzola.org/).

If you would like to suggest edits to a guide, we prefer that you [contact us](https://www.anarsec.guide/contact/) rather than submit a merge request on 0xacab. This is to maintain a unified tone and style to the guides. 

We are also open to submitted guides - please get in touch with proposals.

Our PGP public key can be verified at a second location [here](https://0xacab.org/anarsec/anarsec.guide/-/blob/no-masters/static/anarsec.asc) - commit SHA should be 4ab7e7262f51a661b02e1cf6712b75101f4b25e1. WayBack Machine of PGP key: [anarsec.guide](https://web.archive.org/web/20230619164601/https://www.anarsec.guide/anarsec.asc) / [0xacab.org](https://web.archive.org/web/20230619164309/https://0xacab.org/anarsec/anarsec.guide/-/blob/no-masters/static/anarsec.asc)
