+++
title = "Séries"
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

AnarSec est une ressource conçue pour aider les anarchistes à se frayer un chemin à travers le terrain hostile de la technologie — des guides défensifs sur la sécurité numérique et l'anonymat, et des guides offensifs sur le piratage informatique. Tous les guides sont disponibles sous forme de brochures à imprimer et seront maintenus à jour.

## Défensif 

### Tails
* [Tails for Anarchists](/posts/tails/)
* [Tails Best Practices](/posts/tails-best/)

### Qubes OS
* [Qubes OS for Anarchists](/posts/qubes/)

### Téléphonie 
* [**Tue le flic dans ta poche**](/fr/posts/nophones/)
* [GrapheneOS for Anarchists](/posts/grapheneos/)

### Général 
* [Linux Essentials](/posts/linux/)
* [Remove Identifying Metadata From Files](/posts/metadata/)
* [Encrypted Messaging for Anarchists](/posts/e2ee/)
* [Make Your Electronics Tamper-Evident](/posts/tamper/)

## Offensif

*À venir*

</div>
