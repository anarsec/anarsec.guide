+++
title = "Series"
description = ""
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

AnarSec is a resource designed to help anarchists navigate the hostile terrain of technology — defensive guides for digital security and anonymity, as well as offensive guides for hacking. All guides are available in booklet format for printing and will be kept up to date. 

## Defensive

### Tails
* [Tails for Anarchists](/posts/tails/)
* [Tails Best Practices](/posts/tails-best/)

### Qubes OS
* [Qubes OS for Anarchists](/posts/qubes/)

### Phones 
* [Kill the Cop in Your Pocket](/posts/nophones/)
* [GrapheneOS for Anarchists](/posts/grapheneos/)

### General 
* [Linux Essentials](/posts/linux/)
* [Remove Identifying Metadata From Files](/posts/metadata/)
* [Encrypted Messaging for Anarchists](/posts/e2ee/)
* [Make Your Electronics Tamper-Evident](/posts/tamper/)

## Offensive

*Coming soon*

</div>
