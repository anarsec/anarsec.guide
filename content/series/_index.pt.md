+++
title = "Séries"
description = ""
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

AnarSec é um material criado para ajudar anarquistes navegarem o terreno hostil da tecnologia — guias defensivos para segurança digital e anonimidade, assim como guias ofensivos para hackeamentos. Todos os guias estão disponíveis em formato de livreto para impressão e serão mantidos atualizados.

## Defensivo

### Tails
* [Tails for Anarchists](/posts/tails/)
* [Tails Best Practices](/posts/tails-best/)

### Qubes OS
* [Qubes OS for Anarchists](/posts/qubes/)

### Telefones 
* [**Mate o Policial no seu Bolso**](/pt/posts/nophones/)
* [GrapheneOS for Anarchists](/posts/grapheneos/)

### Geral
* [Linux Essentials](/posts/linux/)
* [Remove Identifying Metadata From Files](/posts/metadata/)
* [Encrypted Messaging for Anarchists](/posts/e2ee/)
* [Make Your Electronics Tamper-Evident](/posts/tamper/)

## Ofensiva 

*Em Breve*

</div>
