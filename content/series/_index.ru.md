+++
title = "Серии"
description = ""
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

AnarSec это ресурс который призван помочь анархистам ориентироваться во враждебном мире технологий — подборка пособий по обеспечению цифровой безопасности и анонимности, а также по проведению хакерских атак. Все пособия доступны в виде буклетов, чтобы их можно было распечатать и будут постоянно обновляться.

## Защита

### Tails
* [Tails for Anarchists](/posts/tails/)
* [Tails Best Practices](/posts/tails-best/)

### Qubes OS
* [Qubes OS for Anarchists](/posts/qubes/)

### Телефоны
* [**Избавься от шпиона в твоем кармане**](/ru/posts/nophones/)
* [GrapheneOS for Anarchists](/posts/grapheneos/)

### Общие вопросы 
* [Linux Essentials](/posts/linux/)
* [Remove Identifying Metadata From Files](/posts/metadata/)
* [Encrypted Messaging for Anarchists](/posts/e2ee/)
* [Make Your Electronics Tamper-Evident](/posts/tamper/)

## Нападение 

*Скоро ожидается*

</div>
