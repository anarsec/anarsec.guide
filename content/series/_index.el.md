+++
title = "Σειρές"
description = ""
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

Το AnarSec είναι ένα νέο βοήθημα που έχει σχεδιαστεί για να βοηθήσει τους αναρχικούς να πλοηγηθούν στο εχθρικό έδαφος της τεχνολογίας — αμυντικοί οδηγοί για ψηφιακή ασφάλεια και ανωνυμία, καθώς και επιθετικοί οδηγοί για χάκινγκ. Όλοι οι οδηγοί διατίθενται σε μορφή booklet για εκτύπωση και θα ενημερώνονται.

## Άμυνα 

### Tails
* [Tails for Anarchists](/posts/tails/)
* [Tails Best Practices](/posts/tails-best/)

### Qubes OS
* [Qubes OS for Anarchists](/posts/qubes/)

### Τηλέφωνα
* [**Σκότωσε τον μπάτσο στην τσέπη σου**](/el/posts/nophones/)
* [GrapheneOS for Anarchists](/posts/grapheneos/)

### Γενικά
* [Linux Essentials](/posts/linux/)
* [Remove Identifying Metadata From Files](/posts/metadata/)
* [Encrypted Messaging for Anarchists](/posts/e2ee/)
* [Make Your Electronics Tamper-Evident](/posts/tamper/)

## Επίθεση

*Προσεχώς*

</div>
