+++
title = "Поиск"
sort_by = "date"
paginate_by = 5
+++
<br>
<p style="text-align:center">The search feature uses the external search engine DuckDuckGo.</p>

<div class="columns is-centered">
<form action="https://duckduckgo.com/" method="get">
  <input type="hidden" name="sites" value="anarsec.guide">
  <input type="search" name="q">
  <input type="submit" value="Поиск">
</form>
</div>
