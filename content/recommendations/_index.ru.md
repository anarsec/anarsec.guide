+++
title = "Рекомендации"
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

As anarchists, we must defend ourselves against police and intelligence agencies that conduct [targeted digital surveillance](https://notrace.how/threat-library/techniques/targeted-digital-surveillance.html) for the purposes of [incrimination](https://notrace.how/threat-library/tactics/incrimination.html) and [network mapping](https://notrace.how/threat-library/techniques/network-mapping.html). Our goal is to obscure the State's visibility into our lives and projects. Our recommendations are intended for all anarchists, and they are accompanied by guides to put the advice into practice. 

We agree with the conclusion of an overview of [targeted surveillance measures in France](https://actforfree.noblogs.org/post/2023/07/24/number-of-the-day-89502-preventive-surveillance-measures-france/): "So let’s be clear about our responsibilities: if we knowingly bring a networked device equipped with a microphone and/or a camera (cell phone, baby monitor, computer, car GPS, networked watch, etc.) close to a conversation in which “private or confidential words are spoken” and must remain so, even if it's switched off, we become a potential state informer…"

You may also be interested in the Threat Library's ["Digital Best Practices"](https://www.notrace.how/threat-library/mitigations/digital-best-practices.html).

## Your Phone

>**[Operating system](/glossary#operating-system-os)**: **GrapheneOS** is the only reasonably secure choice for cell phones. See [GrapheneOS for Anarchists](/posts/grapheneos/). If you decide to have a phone, treat it like an "encrypted landline" and leave it at home when you are out of the house. See [Kill the Cop in Your Pocket](/posts/nophones/). 

## Your Computer  

>**[Operating system](/glossary#operating-system-os)**: **Tails** is unparalleled for sensitive computer use (writing and sending communiques, moderating a sketchy website, researching for actions, reading articles that may be criminalized, etc.). Tails runs from a USB drive and is designed with the anti-forensic property of leaving no trace of your activity on your computer, as well as forcing all Internet connections through the [Tor network](/glossary#tor-network). See [Tails for Anarchists](/posts/tails/) and [Tails Best Practices](/posts/tails-best/).

>**[Operating system](/glossary#operating-system-os)**: **Qubes OS** has better security than Tails for many use cases, but has a steeper learning curve and no anti-forensic features. However, it is accessible enough for journalists and other non-technical users. Basic knowledge of using Linux is required — see [Linux Essentials](/posts/linux). Qubes OS can even run Windows programs such as Adobe InDesign, but much more securely than a standard Windows computer. See [Qubes OS for Anarchists](/posts/qubes/). 

See [When to Use Tails vs. Qubes OS](/posts/qubes/#when-to-use-tails-vs-qubes-os). We do not offer "harm reduction" advice for Windows or macOS computers, as this is already widespread and gives a false sense of privacy and security.  

## Encrypted Messaging

See [Encrypted Messaging for Anarchists](/posts/e2ee/)

## Storing Electronic Devices

See [Make Your Electronics Tamper-Evident](/posts/tamper/).

</div>
