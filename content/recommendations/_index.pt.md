+++
title = "Recomendações"
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

Como anarquistas, nós devemos nos defender da polícia e agências de inteligência que conduzem [vigilância digital individualizada](https://notrace.how/threat-library/techniques/targeted-digital-surveillance.html) para os propósitos de [incriminação](https://notrace.how/threat-library/tactics/incrimination.html) e [mapeamento de redes de contatos](https://notrace.how/threat-library/techniques/network-mapping.html). Nosso objetivo é atrapalhar a capacidade do Estado de vigiar nossas vidas e projetos. Nossas recomendações são direcionadas a todos anarquistas, e são acompanhadas de guias e conselhos práticos.

Nós concordamos com a conclusão da análise sobre [vigilância digital individualizada na França](https://actforfree.noblogs.org/post/2023/07/24/number-of-the-day-89502-preventive-surveillance-measures-france/): "Sejamos claros sobre nossas responsabilidades: se nós nós conscientemente levamos um dispositivo equipado com microfone e/ou câmera (celular, babá eletrônica, computador, carro com gps, relógio ligado a internet), mesmo que desligados,  para uma conversa na qual 'se fazem comentários e se usam palavras secretas e confidenciais' que devem permanecer secretas, nós nos tornamos possíveis informantes estatais..." 

Você talvez se interesse pelas ["Boas Práticas Digitais"](https://www.notrace.how/threat-library/mitigations/digital-best-practices.html) da nossa Biblioteca de Ameaças.

## Seu Telefone 

>**[Sistema Operacional](/pt/glossary#sistemas-operacionais-os)**: **GrapheneOS** é a única opção relmente segura para celulares. Veja [GrapheneOS for Anarchists](/posts/grapheneos/). Se você decidir ter um celular, trate-o como uma “linha telefônica criptografada fixa” e deixe-o em casa quando for sair. Vide [Mate o Policial no seu Bolso](/pt/posts/nophones/). 

## Seu computador  

>**[Sistema Operacional](/pt/glossary#sistemas-operacionais-os)**: Não há nada que se compare ao **Tails**, para o manejo de informações sensíveis no computador (escrever e enviar comunicados, moderar um site suspeito, pesquisar para açõs, ler artigos que talvez venham a ser criminalizados, etc.). O Tails opera a partir de um USB e foi criado com uma propriedade anti-forense que o permite não deixar rastros no seu computador, assim como forçar que todas as conexões da internet aconteçam pela rede [Tor](/pt/glossary#rede-tor). Veja [Tails for Anarchists](/posts/tails/) e [Tails Best Practices](/posts/tails-best/).

>**[Sistema Operacional](/pt/glossary#sistemas-operacionais-os)**: **Qubes OS** para muitos usos de caso, tem uma segurança melhor que o Tails, mas sua curva de aprendizado é mais fechada e  nenhuma ferramenta anti-forense. De todo modo é acessível o suficiente para jornalistas e outros usuários não técnicos. Conhecimento básico de Linux é necessário — veja a sessão de [Linux Essentials](/posts/linux). Qubes OS consegue até mesmo rodar alguns programas do Windows como Adobe InDesign, mas de forma muito mais segura. Vide [Qubes OS for Anarchists](/posts/qubes/). 

Veja quando [When to Use Tails vs. Qubes OS](/posts/qubes/#when-to-use-tails-vs-qubes-os). Nós não oferecemos dicas de “redução de danos” para o Windows ou macOS, já que esse tipo de material tende a ser popular e dá a falsa sensação de privacidade e segurança. 

## Mensagens Criptografadas 

Veja [Encrypted Messaging for Anarchists](/posts/e2ee/)

## Armazenando Dispositivos Eletrônicos 

Veja [Make Your Electronics Tamper-Evident](/posts/tamper/).

</div>
