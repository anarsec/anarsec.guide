+++
title = "Recommendations"
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

En tant qu'anarchistes, nous devons nous défendre contre la police et les agences de renseignement qui utilisent la [surveillance numérique ciblée](https://notrace.how/threat-library/fr/techniques/targeted-digital-surveillance.html) pour nous [incriminer](https://notrace.how/threat-library/fr/tactics/incrimination.html) et [cartographier nos réseaux](https://notrace.how/threat-library/fr/techniques/network-mapping.html). Notre but est de dissimuler nos vies et projets à l'État. Ces recommendations sont destinées à tous les anarchistes, et sont accompagnées de guides permettant de les mettre en pratique.

Nous sommes d'accord avec la conclusion d'une revue de [mesures de surveillance ciblées en France](https://sansnom.noblogs.org/archives/16942) : "Que chacun se le tienne donc pour dit en termes de responsabilités : lorsqu'on introduit volontairement un tel objet connecté doté de micro et/ou caméra (téléphone portable, babyphone, ordinateur, GPS de voiture, montre connectée, etc.), même éteint, près d’une conversation où des "paroles sont prononcées à titre privé ou confidentiel" et doivent le rester, on devient soi-même un potentiel mouchard d'État…"

Sur le même sujet, on pourra également lire les ["Bonnes pratiques numériques"](https://notrace.how/threat-library/fr/mitigations/digital-best-practices.html) de la Bibliothèque de menaces.

## Ton téléphone

>**[Système d'exploitation](/fr/glossary#systeme-d-exploitation-os)** : **GrapheneOS** est le seul choix suffisamment sécurisé pour les téléphones portables. Voir [GrapheneOS for Anarchists](/posts/grapheneos/). Si tu décides d'avoir un téléphone, traite-le comme une "ligne fixe chiffrée" et laisse-le chez toi quand tu sors. Voir [Tue le flic dans ta poche](/fr/posts/nophones/).

## Ton ordinateur

>**[Système d'exploitation](/fr/glossary#systeme-d-exploitation-os)** : **Tails** est hors pair pour un usage sensible des ordinateurs (écrire et envoyer des communiqués, administrer un site web sensible, faire des recherches pour des actions, lire des articles qui peuvent être criminalisés, etc.) Tails démarre depuis une clé USB et est conçu pour ne pas laisser de traces sur l'ordinateur sur lequel tu l'utilises, ainsi que pour forcer toutes les connexions Internet via le [réseau Tor](/fr/glossary#reseau-tor). Voir [Tails for Anarchists](/posts/tails) et [Tails Best Practices](/posts/tails-best/).

>**[Système d'exploitation](/fr/glossary#systeme-d-exploitation-os)** : **Qubes OS** est plus sécursé que Tails pour de nombreuses utilisations, mais a une courbe d'apprentissage plus raide et n'est pas conçu pour ne pas laisser de traces sur l'ordinateur. Ceci dit, il est quand même utilisable par des journalistes et autres personnes sans connaissances techniques poussées. Savoir à peu près utiliser Linux est nécessaire — voir [Linux Essentials](/posts/linux). Qubes OS peut même faire tourner des logiciels Windows comme Adobe InDesign, mais de manière bien plus sécurisée qu'un ordinateur Windows classique. Voir [Qubes OS for Anarchists](/posts/qubes/).

Voir [When to Use Tails vs. Qubes OS](/posts/qubes/#when-to-use-tails-vs-qubes-os). Nous ne proposons pas de conseils de "réduction des risques" pour des ordinateurs Windows ou macOS, parce que de tels conseils sont déjà facilement disponibles ailleurs et donnent une fausse impression de vie privée et de sécurité.

## Applications de messagerie chiffrées

Voir [Encrypted Messaging for Anarchists](/posts/e2ee/)

## Stockage des appareils électroniques

Voir [Make Your Electronics Tamper-Evident](/posts/tamper/).

</div>
