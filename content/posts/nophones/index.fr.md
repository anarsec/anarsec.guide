+++
title='Tue le flic dans ta poche'
date=2024-04-23

[taxonomies]
categories = ["Defensive"]
tags = ["mobile"]

[extra]
blogimage="/images/prison-pink.jpg"
toc=true
dateedit=2024-04-23
a4="nophones-a4-fr.pdf"
letter="nophones-letter-fr.pdf"
+++

La [culture de la sécurité et la sécurité opérationnelle](https://notrace.how/fr/blog/a-base-to-stand-on/une-base-sur-laquelle-s-appuyer.html), quand elles sont efficaces, empêchent les forces répressives de se renseigner sur nos activités criminelles, mais aussi sur nos vies, nos [relations](https://notrace.how/threat-library/fr/techniques/network-mapping.html), nos déplacements, et ainsi de suite. Ces renseignements sont un énorme atout pour préciser une liste de suspects et mettre en place de la surveillance ciblée. Cet article va présenter quelques stratégies pour tuer le flic dans ta poche.

<!-- more -->

La localisation de ton téléphone est [pistée en permanence](https://www.vice.com/en/article/m7vqkv/how-fbi-gets-phone-data-att-tmobile-verizon), et ces données de localisation sont récoltées par des entreprises privées, ce qui permet à la police de les obtenir sans avoir besoin de mandat. Les [identifiants matériels d’un téléphone et le nom lié à l’abonnement téléphonique](https://anonymousplanet.org/guide.html#your-imei-and-imsi-and-by-extension-your-phone-number) sont enregistrés par chaque antenne à laquelle il se connecte. Grâce à des services de piratage à distance comme [Pegasus](https://www.amnesty.org/fr/latest/research/2021/07/forensic-methodology-report-how-to-catch-nso-groups-pegasus), même des unités de police locales peuvent prendre le contrôle d’un téléphone, et ces services sont "zero-click," ce qui veut dire qu’il n’y a pas besoin que tu cliques sur un lien ou ouvres un fichier pour que ton téléphone soit piraté. D’un autre côté, après avoir échoué dans leur enquête sur plus d’une trentaine d’incendies volontaires dans une petite ville en France, [des enquêteurs se sont plaint](https://sansnom.noblogs.org/archives/11527) que "l’exploitation de la téléphonie ou des immatriculations de véhicules est impossible puisqu’ils opèrent sans téléphone et sans voiture !"

# Chiffrement et géolocalisation

Dans une [opération répressive récente](https://notrace.how/resources/fr/#ivan) contre un anarchiste, les flics ont pisté la localisation du téléphone du suspect en temps réel et établi une liste de toutes les personnes qu’il avait appelé. On sait bien que ce type de surveillance est courante, et pourtant nombre de camarades trimballent un téléphone où qu’iels aillent, ou appellent d’autres anarchistes "en clair". On pense qu’il faudrait mettre un terme à ces deux pratiques. Ne facilitons pas à ce point le travail des flics et des agences de renseignement en leur offrant nos relations sociales et nos données de localisation sur un plateau d’argent.

Si tu ne sors pas de chez toi avec un téléphone, les flics vont devoir recourir à la surveillance physique pour te localiser, ce qui leur demande beaucoup de ressources et peut être détecté. Et si jamais tu es placé·e sous surveillance physique, l’enquêteur va commencer par chercher à comprendre à quoi ressemblent tes déplacements habituels, et les données de localisation de ton téléphone offrent justement un aperçu détaillé de tes déplacements quotidiens.

Certain·e·s anarchistes pallient aux problèmes des smartphones en utilisant des bigos ou des téléphones fixes pour communiquer entre elleux, mais ce n’est pas une bonne solution. Les bigos et les téléphones fixes ne permettent pas de [communiquer de manière chiffrée](/fr/glossary/#chiffrement-de-bout-en-bout), et donc l’État sait qui parle avec qui et ce qui est discuté. Un but premier de la surveillance ciblée est de cartographier les relations sociales de la cible pour identifier d’autres cibles. Le seul moyen d’empêcher nos ennemis de cartographier nos relations est d’utiliser exclusivement des [moyens de communication chiffrés](/posts/e2ee/) quand on communique avec d’autres anarchistes par l’intermédiaire de la technologie.

# Motifs de métadonnées

La normalisation de la connectivité permanente au sein de la société dominante a mené certain·e·s anarchistes à noter que les [métadonnées](/fr/glossary/#metadonnees) téléphoniques sont utiles aux enquêteurs. Mais à partir de cette constatation, certain·e·s arrivent à la conclusion qu’on ne devrait ["jamais éteindre son téléphone,"](https://attaque.noblogs.org/post/2018/12/11/jamais-eteindre-son-telephone-une-nouvelle-approche-a-la-culture-de-la-securite) ce qui nous mène dans la mauvaise direction. Leur logique est que nos interactions avec la technologie forment un motif de métadonnées de base, et que les moments qui dévient de ce motif ont l’air suspects s’ils coïncident avec le moment où une action se produit, ce qui peut être utilisé par des enquêteurs pour préciser une liste de suspects. Même si c’est vrai, la conclusion opposée est bien plus sensée : les anarchistes devraient minimiser la création de motifs de métadonnées auxquels des enquêteurs pourraient avoir accès.

Nos connexions aux infrastructures de la domination doivent rester opaques et imprévisibles si on veut pouvoir continuer à frapper l’ennemi. Et si les repérages préalables à une action impliquent de passer un week-end entier sans appareils électroniques ? Il y a aussi le simple fait que les téléphones doivent être laissés chez soi pendant une action — cela ne dévie d’un motif que si on a habituellement toujours un téléphone avec soi. Dans une vie "toujours connectée", ces deux changements dans nos métadonnées feraient tâche, mais ce n’est pas le cas si l’on refuse d’être connecté en permanence. **Pour minimiser l’empreinte laissée par tes métadonnées, tu dois laisser ton téléphone chez toi par défaut.**

# As-tu vraiment besoin d’un téléphone ?

Les téléphones ont colonisé notre vie quotidienne parce qu’on nous a fait croire qu’on a besoin à tout moment de communication *synchrone*. La communication [*synchrone*](/fr/glossary/#communication-synchrone) c’est quand deux personnes communiquent en temps réel, à l’opposé d’une communication [*asynchrone*](/fr/glossary/#communication-asynchrone) comme les emails, où les messages sont envoyés à des moments différents. Ce "besoin" a été normalisé, mais cela vaut le coup d’y résister dans les réseaux anarchistes. [L’anarchie ne peut être qu’anti-industrielle](https://theanarchistlibrary.org/library/bismuto-beyond-the-moment#toc1). On doit apprendre à vivre sans les commodités qui nous sont vendues par les entreprises de télécommunications, on doit défendre (ou raviver) notre capacité à vivre sans être connecté en permanence à Internet, sans les itinéraires en temps réel d’un GPS, et sans la possibilité de pouvoir toujours changer de plans à la dernière minute.

Si tu décides d’utiliser un téléphone, pour que ce soit aussi difficile que possible pour un adversaire de le localiser, d’intercepter ses messages, ou de le pirater, utilise [GrapheneOS](/posts/grapheneos/). Si on se met d’accord **d’utiliser uniquement des [moyens de communication chiffrés](/posts/e2ee/) pour communiquer avec d’autres anarchistes**, cela exclut les bigos et les téléphones fixes. GrapheneOS est le seul système d’exploitation pour smartphones qui offre un respect de la vie privée et une sécurité raisonnables.

**Pour empêcher tes déplacements d’être pistés, considère le smartphone comme un téléphone fixe et laisse le chez toi quand tu sors.** Même si tu utilises une carte SIM achetée anonymement, si elle est un jour reliée à ton identité, l’opérateur de téléphonie mobile peut rétroactivement obtenir l’historique de ses données de localisation. Si tu utilises un téléphone en suivant nos recommendations (en le [connectant uniquement au Wi-Fi](/posts/grapheneos/#what-is-grapheneos) et en le laissant en permanence en mode avion), il ne se connectera pas aux antennes téléphoniques. Cela ne suffit pas de laisser le téléphone chez toi quand tu vas à une réunion, une manif ou une action parce que cela dévierait du motif formé par tes comportements habituels et serait un indice qu’une activité suspecte est en cours sur cette période.

Tu peux choisir de vivre entièrement sans téléphone, si tu penses ne pas avoir besoin d’une "ligne fixe chiffrée". Les stratégies qui suivent visent à minimiser la dépendance aux téléphones en utilisant des ordinateurs à la place (qui permettent aussi des formes de communication synchrone, bien que plus limitées).

## Bureaucratie

Il y a beaucoup d’institutions bureaucratiques avec lesquelles on est obligé de communiquer par téléphone : institutions de santé, banques, etc. Il n’y a pas besoin de chiffrer ces communications, donc tu peux utiliser une application de [voix sur IP (VoIP)](/fr/glossary/#voix-sur-ip-voip-voice-over-internet-protocol). Cela te permet de passer des coups de fil par Internet plutôt que par les antennes téléphoniques.

Les applications VoIP disponibles sur ordinateur sont asynchrones car elles ne sonnent pas quand l’ordinateur est éteint — quand tu loupes un appel il faut consulter la boîte vocale. Par exemple, un service comme [jmp.chat](https://www.kicksecure.com/wiki/Mobile_Phone_Security#Phone_Number_Registration_Unlinked_to_SIM_Card) te donne un numéro VoIP, que tu peux payer en Bitcoin, et tu peux passer des appels avec une application XMPP — par exemple [Cheogram](https://cheogram.com/).

Un numéro VoIP fonctionne généralement pour [l’authentification à deux facteurs](/fr/glossary/#authentification-a-deux-facteurs-2fa) (quand un service te demande de recevoir un code aléatoire par SMS pour te connecter). Pour ça, les [numéros de téléphone en ligne](https://anonymousplanet.org/guide.html#online-phone-number) sont une autre option.

Bien que ce soit souvent plus cher qu’un numéro VoIP, un bigo ou téléphone fixe dédié fonctionne bien aussi pour passer et recevoir des appels "bureaucratiques" depuis chez soi.

## Communication

Ne pas avoir de téléphone sur toi en permanence nécessite de changer la manière dont tu sociabilises si tu es déjà [pris·e dans la toile](https://returnfire.noblogs.org/en-francais/pris-e-s-dans-la-toile). Minimiser intentionnellement la médiation des écrans dans nos relations sociales est un but précieux en soi.

Utiliser une "ligne fixe chiffrée" pour passer des coups de fil et un ordinateur pour nos communications chiffrées nous permet d’éviter le flot sans fin des notifications sur un appareil toujours à portée de main.

Cela nous ferait beaucoup de bien d’examiner attentivement la monoculture des groupes Signal qui ont remplacé les rencontres en face-à-face dans certains réseaux anarchistes. La culture du smartphone capture nos relations d’organisation et nous force à participer à une longue réunion sans fin relativement facile à surveiller.

Ceci dit, les communications chiffrées peuvent être utiles pour décider d’une date et d’un lieu où se rencontrer, ou pour s’organiser quand on habite loin les un·e·s des autres. Voir [Encrypted Messaging for Anarchists](/posts/e2ee/) (*Applications de messagerie chiffrées pour anarchistes*) pour des options adaptées à un [modèle de menace](/fr/glossary/#modele-de-menaces) anarchiste.

## Appels d’urgence

C’est souvent possible d’emprunter le téléphone d’un passant dans la rue pour passer un appel d’urgence si on lui dit que notre téléphone n’a plus de batterie. Si on ne peut pas recevoir d’appels d’urgence, on peut prévoir de passer les un·e·s chez les autres ou de s’envoyer des messages chiffrés à des moments convenus à l’avance. Quels scénarios nécessitent vraiment que tu sois disponible pour recevoir un appel à tout moment ? Si c’est le cas dans ta vie, tu peux t’organiser en fonction de ça sans projeter cette urgence sur tous les aspects et moments de ta vie.

## Itinéraires 

Achète une carte papier de la zone et prends-la avec toi. Pour des trajets plus longs ou des trajets pour lesquels tu as besoin d’un itinéraire, note les itinéraires à l’avance avec [OpenStreetMap](https://openstreetmap.org/).

## Musique et podcasts 

Ils font toujours des lecteurs MP3 ! Pour un prix bien moindre qu’un téléphone, tu peux écouter de la musique et des podcasts, mais l’appareil n’a ni GPS ni module radio. Ceci dit, ça ne veut pas dire que tu ne peux pas être géolocalisé·e via un lecteur MP3. S’il se connecte au Wi-Fi, la position approximative de ton lecteur MP3 peut être déterminée à partir de son adresse IP.

# Annexe : Contre le smartphone 

*De [Fernweh (#24)](https://fernweh.noblogs.org/texte/24-ausgabe/gegen-das-smartphone/)* 

Il est toujours avec nous, toujours allumé, peu importe où on est ou ce qu’on fait. Il nous tient informé de tout et de tout le monde : ce que font nos ami·e·s, l’horaire du prochain métro, quel temps il fera demain. Il prend soin de nous, nous réveille le matin, nous rappelle nos rendez-vous importants, et est toujours à notre écoute. Il sait tout sur nous, quand est-ce qu’on se couche, où on est et quand, avec qui on communique, qui sont nos meilleur·e·s ami·e·s, quelle musique on écoute, quels sont nos hobbies. Et tout ce qu’il demande c’est un peu d’électricité de temps en temps ?

Quand je me promène ou que je prends le métro, tout le monde en a un, et personne ne tient plus de quelques secondes sans aller frénétiquement le chercher dans sa poche, le téléphone est sorti, un message est envoyé, un email lu, une photo likée. Il est rangé, petite pause, et on recommence, consultant les nouvelles du jour et ce que font nos ami·e·s...

Il est avec nous aux toilettes, au travail ou à l’école, et il aide apparemment à surmonter l’ennui quand on attend, on travaille, etc. Serait-ce peut-être une raison du succès de tous ces appareils technologiques, que la vraie vie est si ennuyeuse et monotone que quelques centimètres carrés d’écran sont presque toujours plus excitants que le monde et les personnes qui nous entourent ? Est-ce qu’il est comme une addiction (les gens ont clairement des symptômes de manque...) ou est-ce qu’il fait désormais partie de notre corps ? Sans lui, on perd nos repères et on a l’impression que quelque chose manque ? Donc ce n’est plus juste un outil ou un jouet, mais une partie de nous qui exerce aussi un certain contrôle sur nous, à laquelle on s’adapte, par exemple, en ne sortant pas de chez soi sans une batterie pleine ? Est-ce que le smartphone est la première étape qui brouillera la frontière entre humain et machine ?

Quand on voit ce que les technocrates en tout genres prophétisent (lunettes connectées Google, puces implantées, etc.), on dirait presque qu’on est en voie de devenir des cyborgs, des gens avec des smartphones implantés qu’on contrôle par la pensée (jusqu’à ce que nos pensées elles-même soient enfin contrôlées). Ce n’est pas surprenant que les médias, porte-paroles de la domination, ne nous montrent que les aspects positifs de cette évolution, mais c’est choquant que presque personne ne questionne cette vision. C’est probablement le rêve de tout chef d’État : pouvoir contrôler les pensées et actions de chacun et intervenir immédiatement en cas de problème. Des abeilles ouvrières complètement contrôlées et surveillées qu’on récompense par un peu de fun (virtuel) pendant que quelques-uns profitent.

Avec les immenses quantités de données facilement accessibles à propos de tout et de tout le monde à tout moment, le contrôle social et la surveillance ont atteint de nouveaux sommets. Cela va bien plus loin que la surveillance des téléphones et des SMS (comme on a pu voir pendant les émeutes de 2011 au Royaume-Uni). L’accès à une telle quantité d’informations permet aux agences de renseignement de définir ce qui est "normal". Ils peuvent déterminer quels endroits sont "normaux" pour nous, quels contacts sont "normaux", etc. En bref, ils peuvent repérer rapidement et presque en temps réel si des gens dévient de leur comportement "normal". Cela donne un immense pouvoir à certaines personnes, qui est utilisé à chaque opportunité (pour surveiller les gens). La technologie fait partie du pouvoir, provient du pouvoir et a besoin du pouvoir. Il faut un monde dans lequel des individus ont un pouvoir extrême pour permettre la production de quelque chose comme le smartphone. Toute technologie est un produit du monde oppressif actuel, fait partie de lui, et le renforce.

Dans le monde d’aujourd’hui, rien n’est neutre. À ce jour, tout ce qui a été ou est développé est conçu pour étendre le contrôle et faire de l’argent. De nombreuses innovations de ces dernières décennies (comme le GPS, l’énergie nucléaire, ou Internet) proviennent même directement de l’armée. La plupart du temps ces deux aspects vont main dans la main, mais le "bien-être de l’humanité" n’est certainement pas une motivation, surtout dans ce qui est développé par l’armée.

Peut-être que prendre l’exemple de l’architecture peut mieux illustrer quelque chose d’aussi complexe que la technologie : prenons une prison vide et désaffectée, que peut-on faire de cette structure sinon la détruire ? Son architecture même, ses murs, ses miradors, ses cellules, contiennent déjà la raison du bâtiment : emprisonner des gens et les détruire psychologiquement. Cela serait impossible pour moi de vivre là, parce que le bâtiment même est oppressif.

De la même manière, les technologies d’aujourd’hui nous sont présentées comme un progrès visant à nous faciliter la vie. Elles ont été conçues dans le but de faire de l’argent et de nous contrôler, et auront toujours cela en elles. Peu importe les bénéfices supposés que tu retires de ton smartphone, ceux qui s’enrichissent en collectant tes données et en te surveillant vont toujours retirer plus de bénéfices.

Si par le passé on disait que "la connaissance c’est le pouvoir", aujourd’hui on pourrait dire que "l’information c’est le pouvoir". Plus les chefs en savent sur leurs troupeaux, mieux ils peuvent les dominer — ainsi, la technologie dans son ensemble est un outil de contrôle puissant pour prévoir et donc empêcher les gens d’attaquer ensemble ce qui les oppresse.

Ces smartphones ont l’air d’avoir besoin d’un peu plus que juste un peu d’électricité... Au sein de notre génération, qui a au moins connu un monde sans smartphones, il y a peut-être encore quelques personnes qui comprennent ce dont je parle, qui savent encore ce que c’est que d’avoir une conversation sans vérifier son téléphone toutes les trente secondes, de se perdre et découvrir de nouveaux endroits, ou de débattre sans demander immédiatement la réponse à Google. Mais je ne veux pas revenir au passé, même si ce ne serait de toute façon pas possible. Mais plus la technologie pénètre nos vies, plus cela semble difficile de la détruire. Et si nous étions une des dernières générations à pouvoir empêcher l’évolution des êtres humains en machines entièrement contrôlées ?

Et si à un moment on devient incapables d’inverser cette évolution ? L’humanité a atteint une nouvelle étape historique dans le domaine de la technologie. Une étape où elle est capable d’annihiler toute vie humaine (énergie nucléaire) ou de la modifier (manipulation génétique). Ce fait démontre une fois de plus la nécessité d’agir aujourd’hui pour détruire cette société. Pour ça, on a besoin de se rencontrer les un·e·s les autres et d’échanger des idées.

N’est-ce pas évident que si, au lieu de se parler les un·e·s aux autres, on communique uniquement par messages de cinq phrases ou moins, cela aura des effets sur le long terme ? Apparemment non. Tout d’abord, la manière dont on pense influence la manière dont on parle, et inversement — la manière dont on parle et communique influence la manière dont on pense. Si on est seulement capable d’échanger des messages courts et concis, comment peut-on discuter d’un monde complètement différent ? Et si on ne peut même pas discuter d’un autre monde, comment l’atteindre ?

La communication directe entre individus autonomes est la base de toute rébellion partagée, c’est le point de départ de rêves partagés et de luttes communes. Sans communication directe, toute lutte contre ce monde et pour la liberté est impossible.

Donc débarrassons-nous du smartphone et rencontrons-nous en face-à-face dans une insurrection contre ce monde ! Devenons incontrôlables !
