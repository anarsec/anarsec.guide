+++
title="Remove Identifying Metadata From Files"
date=2024-04-20

[taxonomies]
categories = ["Defensive"]
tags = ["metadata", "tails", "qubes", "beginner"]

[extra]
blogimage="/images/app-dither.png"
toc=true
dateedit=2024-04-20
a4="metadata-a4-en.pdf"
letter="metadata-letter-en.pdf"
+++


[Metadata](/glossary/#metadata) is *data about data* or *information about information*. In the context of files, this can mean information that is automatically embedded in the file, and this information can be used to deanonymize you. For example, an image file will often have metadata about when it was taken, where it was taken, what camera it was taken with, etc. A PDF file may have information about what program created it, what computer, etc. This can be used by investigators to link a photo to the camera on which it was taken, a video to the computer on which it was edited, and so on. Before you put a sensitive file on the Internet, remove the metadata. 

<!-- more -->

# Metadata Anonymization Toolkit 

Fortunately, there is a tool that comprehensively cleans metadata, and it is available as both a [command line interface](/glossary#command-line-interface-cli) and a graphical user interface. The command line version is called `mat2` and is [open-source](https://0xacab.org/jvoisin/mat2), and the graphical version is called  Metadata Cleaner and is also [open-source](https://gitlab.com/rmnvgr/metadata-cleaner/). Both programs are included in [Tails](/tags/tails/) and [Qubes-Whonix](/posts/qubes/#whonix-and-tor) by default. 

# Using the Metadata Cleaner 

If you are not comfortable with the command line, we recommend using Metadata Cleaner — it uses `mat2` under the hood, so it has all the same functionality. Metadata Cleaner is better than Exiftool and other metadata removal software — see the [comparison docs](https://0xacab.org/jvoisin/mat2/-/blob/master/doc/comparison_to_others.md). 

Metadata Cleaner shows the metadata it detects, but "it doesn't mean that a file is clean from any metadata if mat2 doesn't show any. There is no reliable way to detect every single possible metadata for complex file formats." This means that you should clean the file even if no metadata is displayed.  

To use the Metadata Cleaner, first add a file. When you click it, the current metadata is displayed. Select the file, then select **Clean**. You can verify that the metadata has been removed by re-adding the cleaned file and viewing its metadata. 

When you clean a PDF file, it is converted to images, so the quality is downgraded and you cannot select the text in it. If you want to retain this ability, there is a *lightweight* cleaning mode that cleans only the superficial metadata of your file, but not the metadata of "embedded resources" (such as images in the PDF). If you are creating a PDF, use Metadata Cleaner on any images before importing them into the layout software, and use layout software on Tails or Qubes-Whonix such as Scribus that are generic for those operating systems. You can enable "lightweight cleaning" in the Metadata Cleaner settings. 

Note the limitations of Metadata Cleaner: "mat2 only removes metadata from your files, it does not anonymise their content, nor can it handle watermarking, steganography, or any too custom metadata field/system. If you really want to be anonymous, use file formats that do not contain any metadata, or better: use plain-text."

# Photo and Video Forensics

While it is possible to remove all metadata from an image or video, forensic examination may still reveal what device was used to capture it. As the Whonix [docs](https://www.whonix.org/wiki/Surfing_Posting_Blogging#Photographs) note:

> Every camera's sensor has a unique noise signature because of subtle hardware differences. The sensor noise is detectable in the pixels of every image and video shot with the camera and could be fingerprinted. In the same way ballistics forensics can trace a bullet to the barrel it came from, the same can be accomplished with adversarial digital forensics for all images and videos. Note this effect is different from file metadata.

Multiple photos or videos from the same camera can be tied together in this way, and if the camera is recovered, it can be confirmed where the files came from. Cheap cameras can be purchased from a pawn shop and used only once for pictures or videos that require high security. 

# Printer Forensics 

All modern printers leave invisible watermarks to encode information such as the serial number of the printer and when it was printed. When printed material is scanned, these marks are present in the file. To learn more, see [Revealing Traces in Printouts and Scans](https://dys2p.com/en/2022-09-print-scan-traces.html) and the Whonix documentation on [printing and scanning](https://www.whonix.org/wiki/Printing_and_Scanning). 

# Further Reading

* [Anonymous File Sharing](https://www.whonix.org/wiki/Surfing_Posting_Blogging#Anonymous_File_Sharing) from the Whonix documentation. 
* [Redacting Documents/Pictures/Videos/Audio safely](https://anonymousplanet.org/guide.html#redacting-documentspicturesvideosaudio-safely) for a table of recommended software for creating different types of files. 
* [Behind the Data: Investigating metadata](https://exposingtheinvisible.org/en/guides/behind-the-data-metadata-investigations/) for how metadata can be used to identify and reveal personal information. 
