+++
title="Linux Essentials"
date=2024-04-21

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "linux", "tails", "qubes", "beginner"]

[extra]
blogimage="/gifs/destroy2.gif"
blogimagepng="/images/destroy.png"
toc=true
dateedit=2024-04-21
a4="linux-a4-en.pdf"
letter="linux-letter-en.pdf"
+++

As an anarchist, someone's probably recommended that you use a Linux computer at some point. This article is intended to get you started by giving you a brief overview of what you need to know. 
<!-- more -->

# What is Linux and Why Use It? 

If you're reading this, you're probably using either Windows or macOS on your computer. These are both [operating systems](/glossary#operating-system-os), which is the system software that runs your device. They're also both "closed-source", which means that the software's "*source* code" is not available (*closed*) to the public, so it can't be audited for privacy and security. Windows and macOS computers send your data to Microsoft and Apple, and you can't trust their [full-disk encryption](/glossary#full-disk-encryption-fde) to protect your data if the computer is [physically accessed](/glossary/#physical-attacks) (like after a [house raid](https://notrace.how/threat-library/techniques/house-raid.html)). 

Linux is a set of operating systems that are [open-source](/glossary#open-source), which means that the *source* code can be analyzed by anyone. Linux is the name given to the core (**kernel**) of the operating system, and many different **distributions** (or 'distros') are based on it.  

Some Linux distributions you may have heard of are Debian, Ubuntu and [Tails](/tags/tails/). Each Linux distribution manages software differently, may use a different kernel version, etc., depending on what the specific distribution is geared towards. In fact, both Ubuntu and Tails are adaptations of the Debian distribution for being user-friendly (Ubuntu) and providing anonymity (Tails).   

# How Software Works

In Linux, the term for an application is a **package**. Instead of downloading applications from various sites on the Internet (as in Windows and macOS), a Linux distribution has a centralized **repository** where the software lives. The advantage of this is that the integrity of the software is verified by the distribution, and it is guaranteed to work with that distribution. It is still possible to install software from outside of a distribution's repository, but it is generally considered riskier, and verifying the integrity becomes your responsibility. Installing a package requires knowing its name, and all packages in a repository can be browsed using a web browser for both [Debian](https://www.debian.org/distrib/packages#search_packages) and [Fedora](https://packages.fedoraproject.org/).

How do you actually install from a software repository? Each distribution also has a **package manager**, which is an application that installs software from a repository. Debian and other distributions based on it use the `apt` package manager. In some distributions, it is possible to install software with a Graphical User Interface (GUI) that uses the package manager in the background, such as the [Synaptic Package Manager](/posts/tails/#installing-additional-software) in Tails. 

# Software Alternatives

Part of the learning curve for Linux is figuring out which open-source software to use instead of the closed-source options you are used to in Windows and macOS. For example, instead of using Microsoft Word, you might use LibreOffice. It's essential that the applications you use are open-source, but an application being open-source is not enough to consider it secure. For example, Telegram advertises itself as open-source, but its servers are not open-source and its cryptography is [garbage](https://buttondown.email/cryptography-dispatches/archive/cryptography-dispatches-the-most-backdoor-looking/). The list of [included software for Tails](/posts/tails/#included-software) will cover many of your needs with reputable choices, and you can also check out [switching.software](https://switching.software/). 

# The Command Line Interface

![](/posts/linux/cli.png)

The dreaded [command line](/glossary/#command-line-interface-cli)! What even is it? You are used to interacting with applications through a **Graphical User Interface (GUI)**, which means pointing and clicking with your mouse. Some applications can also be interacted with through a **Command Line Interface (CLI)**, which is textual. Many applications are available in both CLI and GUI versions. In a nutshell, the GUI is just a graphical depiction of the same things that you would do from the Command Line (CLI), designed to make it easier and more intuitive to navigate your computer. 

For example, navigating the contents of your computer with the File Manager GUI is pretty standard — you click on a folder (called a *directory* in Linux), and it opens. The same navigation through the file system is also possible from the CLI.

When you open a Terminal (the CLI application), you get a *prompt*. It is called a prompt because it is prompting you to say something in a language that the Terminal understands. Prompts differ in what information is displayed, but they all end with the `$` character. You then give *commands* to the Terminal. The Terminal responds, then redisplays the prompt for the next command.  

The best way to learn the basics of the command line is to interact with it. We recommend the [Foundations: Linux Journey](https://techlearningcollective.com/foundations/linux-journey/) "Command Line" module to learn some basic commands. The [Software Distribution and Packages](https://techlearningcollective.com/foundations/linux-journey/software-distribution) exercise will teach you what you need to know to [install software in Qubes](/posts/qubes/#how-to-install-software).  

Some commands require elevated privileges, equivalent to "Open as Administrator" in Windows. For example, installing software usually requires this privileged access. Prefixing a command with `sudo` will execute it as the administrative user, named root (note that the root user is not the same as the root directory, and the two should not be confused). A root prompt will display `#` instead of `$`. Be especially careful with any commands you run while using these elevated privileges, as you'll have the power to erase your entire hard drive or change important files. It is helpful to know that text is pasted into the Terminal with Ctrl+Shift+V (i.e. the Shift key must also be pressed). 

Most Linux users will rarely need to use the CLI. If you're using Tails, you shouldn't need it at all. If you're using Qubes OS, the CLI is only needed to install software:

* This will install packages on Debian: `apt install <PACKAGE_NAME>`
* This will install packages on Fedora: `dnf install <PACKAGE_NAME>`

Additionally, the CLI is needed for the more secure installation of both [Tails](/posts/tails-best/#appendix-gpg-explanation) and [Qubes OS](https://www.qubes-os.org/security/verifying-signatures/) to verify the download's authenticity. 

If you ever need to edit a text file from the command line, [you can use `nano`](https://phoenixnap.com/kb/use-nano-text-editor-commands-linux). If you ever don't understand what a command does, try looking it up on [explainshell](https://explainshell.com/).

# Going Further

If you want to learn more about Linux, we'd recommend:

* The rest of the Tech Learning Collective's [Foundations](https://techlearningcollective.com/foundations/) exercises will give you a much more comprehensive foundation than what you need to use Qubes or Tails.  
* [Linux Fundamentals at Hack The Box Academy](https://academy.hackthebox.com/course/preview/linux-fundamentals) is another interactive learning environment with a less comprehensive overview.


