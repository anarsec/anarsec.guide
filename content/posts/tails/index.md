+++
title="Tails for Anarchists"
date=2024-04-26

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "linux", "tails", "beginner"]

[extra]
blogimage="/images/tails-dither.png"
toc=true
dateedit=2024-04-26
a4="tails-a4-en.pdf"
letter="tails-letter-en.pdf"
+++

Tails is an [operating system](/glossary/#operating-system-os) that makes anonymous computer use accessible to everyone. Tails is [designed](https://tails.net/about/index.en.html) to leave no trace of your activity on your computer unless you explicitly configure it to save specific data. It accomplishes this by running from a DVD or USB, independent of the operating system installed on the computer. Tails comes with [several built-in applications](https://tails.net/doc/about/features/index.en.html) preconfigured with security in mind, and all anarchists should know how to use it for secure communication, research, editing, and publishing sensitive content. 

<!-- more -->

The [documentation on the Tails website](https://tails.net/doc/index.en.html) is excellent and easy to follow. This tutorial summarizes the most relevant documentation and additionally includes configuration and usage advice specific to an anarchist [threat model](/glossary/#threat-model). Our [Tails Best Practices](/posts/tails-best) article goes into more detail, but we recommend that you familiarize yourself with the basics of Tails before reading it.   

# TAILS: The Amnesic & Incognito Live System

Tails is an operating system. An operating system is the set of programs that run the various components (hard drive, screen, processor, memory, etc...) of the computer and allow it to function.

You have probably heard of "Windows" or "macOS", the two most common operating systems. There are other operating systems — maybe you have heard of Linux? Linux refers to a family of operating systems that branches off into several sub-families, or different versions of Linux, one of which is called Debian. In the Debian sub-family we find Ubuntu and Tails. Tails is a distribution (version) of Linux with several distinguishing features: 

* ***Live System***
	* Tails is a so-called live system. While other operating systems run from your computer's hard drive, Tails is installed on an external device such as a USB (or even an SD card or DVD). When you start your computer with the Tails device plugged in, your computer runs off of that device instead, leaving your hard drive untouched. You can even use Tails on a computer without a hard drive.  
* ***Amnesia***
	* Tails is designed to leave no data on the computer you are using; it writes nothing to the hard drive, and runs only in RAM (memory), which is automatically erased after shutdown. The Tails live system itself (usually running on a USB) is also left untouched. The only way to save information is to move it to another USB partition before shutting down (see below). The purpose of this is to avoid leaving forensic traces that someone with physical access to your computer or your Tails USB could later read. Things like Internet search history, "recently edited" documents, etc. are all erased.  
* ***Incognito***
	* Tails is also a system that allows you to be incognito, or anonymous. It hides the elements that could reveal your identity, location, etc. Tails uses the [Tor anonymity network](/glossary#tor-network) to protect your anonymity online by forcing all default software to connect to the Internet through Tor. If an application tries to connect to the Internet directly, Tails will automatically block the connection. Tails also changes the "MAC address" of your network hardware, which can be used to uniquely identify your laptop.

![](/posts/tails/tor-features.png)

* ***Security***
	* Tails was designed with security in mind. A minimal, functional, and verified environment is already installed (with everything needed for basic word processing, image editing, encryption, etc.).  

Today's digital security is not necessarily tomorrow's. **Protecting personal data requires regular updates.** Digital tools are unreliable if they are never updated, and to have lasting confidence in these tools, it is good to know that teams are actively maintaining them and that they have a good reputation. It is important to understand the spirit of Tails: everything is designed with security in mind. However, in software, there is no such thing as a perfect tool; there are always limits. Also, **the way you use Tails can create security problems.** 

Tails is free and [open-source](/glossary/#open-source) software. Anyone can view, download and modify the source code (the recipe)... It is absolutely necessary to make sure that the version of Tails you have is genuine. Don't neglect the verification steps during installation, which are well explained on the Tails website.  

Tails allows non-experts to benefit from digital security and anonymity without a steep learning curve. Using Tor is central to digital anonymity, and Tails helps us make as few mistakes as possible when using Tor and some other tools. Using Tails takes very little effort to make everyday digital behavior more secure, even if it seems “inconvenient” at times. The "convenient" alternative, on the other hand, means an increased risk of repression — not only for you, but also for those you communicate with.

This tutorial is divided into several sections. The first covers the basics for getting started with Tails. The second section covers tips for using the software included in Tails, as well as what you need to know about how Tor works. The third section is about troubleshooting any problems that you might encounter with your Tails USB, so do not give up at the first problem — most of the time the solution is simple!

## The Threat Model Concept

Tails is not magic and has many limitations. The Internet and computers are hostile territory designed to steal your data. Tails will not protect you from human error, compromised hardware, compromised firmware, being hacked, or certain other types of attacks. There is no such thing as perfect security on the Internet, which is why building a [threat model](/glossary/#threat-model) is so important.

Building a threat model is simply a matter of asking yourself certain questions. Who am I defending against? What are their capabilities? What would be the consequences if they had access to that data? And then, based on the particular situation, assess how you can protect yourself. 

It makes no sense to say "this tool is secure". Security always depends on the threat model and it takes place on multiple levels (network, hardware, software, etc.). For more information on this topic, see the [Threat Library](https://notrace.how/threat-library/). 

# I) The Basics of Using Tails

## Prerequisites

### Select a USB/DVD:

Tails will only work with USBs that are at least 8GB, DVDs, or SD cards. Any data on the USB will be completely erased during installation, so save it somewhere else before, and if you don't want any trace of what was there before, use a new USB. 

The [Tails Best Practices](/posts/tails-best/#using-a-write-protect-switch) article recommends using a USB with a write-protect switch (an unmodifiable disk). When locked, the switch prevents the contents of the USB from being changed at all. This prevents a compromised Tails session from compromising your Tails USB. The write-protect switch must be turned off during installation. If you are unable to obtain such a USB, you can run Tails from a SD card, DVD-R/DVD+R, or always boot with the `toram` option (described in the article).

### Select a laptop:

Although it is possible to use Tails on a desktop computer, it is not recommended because it is only possible to [detect physical tampering](/posts/tamper/#tamper-evident-laptop-screws) on a laptop. See [Tails Best Practices](/posts/tails-best/#reducing-risks-when-using-untrusted-computers) for more information on obtaining a laptop. 

Some laptop and USB models will not work with Tails, or some features will not work. To see if your model has any known issues, see the [Tails known issues page](https://tails.net/support/known_issues/).

If Tails is too slow, make sure the USB is 3.0 or higher and that you are using a USB 3.0 port on the laptop. If Tails freezes frequently, you can add more RAM to your computer. 8GB should be sufficient. 

## Installation

To install Tails on a USB, you need a "source" and a USB (8GB or larger).

There are two solutions for the "source".

### Solution 1: Install by download (preferred)

Follow the [Tails installation instructions](https://tails.net/install/index.en.html); it is important to follow the entire tutorial. It is possible for an attacker to intercept and modify the data on its way to you (this is called a [man-in-the-middle attack](/glossary#man-in-the-middle-attack)), so do not skip the verification steps. As discussed in [Tails Best Practices](/posts/tails-best/#reducing-risks-when-using-untrusted-computers), the [GnuPG installation method](https://tails.net/install/expert/index.en.html) is preferable because it more thoroughly verifies the integrity of the download.

### Solution 2: Install from another Tails USB

This requires knowing a Tails user you trust. A very simple software called the Tails Installer allows you to "clone" an existing Tails USB to a new one in a few minutes; see the documentation for cloning from a [PC](https://tails.net/install/clone/pc/index.en.html) or [Mac](https://tails.net/install/clone/mac/index.en.html). Any Persistent Storage data won't be transferred. The downside of this method is that it may spread a compromised installation.

## Booting from your Tails USB 

Once you have a Tails USB, follow the Tails instructions [for booting Tails on a Mac or PC](https://tails.net/doc/first_steps/start/index.en.html). The Tails USB must be inserted before turning on your laptop. The Boot Loader screen will appear and Tails will start automatically after several seconds. 

![](/posts/tails/grub.png)

After about 30 seconds of loading, the [Welcome Screen](https://tails.net/doc/first_steps/welcome_screen/index.en.html) will appear.

![](/posts/tails/welcome_screen.png)

On the Welcome Screen, select your language and keyboard layout in the **Language & Region** section. For Mac users, there is a keyboard layout for Macintosh. Under "Additional Settings" you will find a **+** button, click it and more configuration options will appear:

* Administration Password 
	* Set this if you need administration rights. This is necessary, for example, to install additional software that you want to use during your Tails session. In the following dialog you can enter any password (and you have to remember it!). It will only be valid for this one Tails session. Restart the Tails session without an administration password as soon as you are done the activity that required it. 
* MAC Address Spoofing 
	* We recommend that you never disable this. It is enabled by default. 
* Network Connection 
	* "Disable all networking" allows you to disable all software network adapters at startup. If you intend to have an 'offline' Tails session, it makes sense to do this before Tails starts its networking functionality.  
* Unsafe Browser 
	* The Unsafe Browser is enabled by default and doesn't use Tor. An attacker could [exploit](/glossary/#exploit) a vulnerability in another application in Tails to launch an invisible Unsafe Browser and reveal your real IP address. This is possible even if you're not using the Unsafe Browser. For example, an attacker could exploit a vulnerability in Thunderbird by sending you a [phishing](/glossary/#phishing) email that launches an invisible Unsafe Browser that visits a website and reveals your IP address. Such an attack is very unlikely, but it could be carried out by a strong attacker, such as a government or a hacking company. For this reason, we **recommend that you disable Unsafe Browser for each session**. Leave Unsafe Browser enabled only when you need to go through a "captive portal" to connect to the Internet (when you have to click a box or log in to connect to the internet, common in Internet cafes, public Wi-Fi, etc.). 

If you have Persistent Storage enabled, the passphrase to unlock it will appear in this window. If you haven't enabled Persistent Storage, no data will be stored on your Tails USB beyond this session. Click **Start Tails**. After 15 to 30 seconds, the Tails desktop will appear.

## Using the Tails Desktop

![](/posts/tails/desktop-label.png)

Tails is a simple operating system. 

1. The Activities menu. Allows you to see an overview of your windows and applications. It also allows you to search for applications, files, and folders. You can also access Activities by sending your mouse to the top left corner of your screen or by pressing the Command/Windows (❖) key.
2. The Applications menu. Lists available applications (software), organized by category. 
3. The Places menu. Shortcuts to various folders and storage devices, which can also be accessed through the Files browser (**Applications → Accessories → Files**). 
4. Date and time. Once connected to the Internet, all Tails systems around the world [share the same time](https://tails.net/doc/first_steps/desktop/time/index.en.html). 
5. The Tor status indicator. Tells you if you are connected to the Tor network. If there is an X over the onion icon, you are not connected. You can open the Onion Circuits application from here. Check your Tor connection by visiting `check.torproject.org` in the Tor Browser. 
6. The "Universal Access" button. This menu allows you to enable accessibility software such as the screen reader, visual keyboard, and large text display.
7. Choice of keyboard layouts. An icon showing the current keyboard layout (in the example above, `en` for an English layout). Clicking it provides options for other layouts selected at the Welcome Screen.
8. The System menu. From here, you can access the volume and screen brightness, the Wi-Fi and Ethernet connection, the battery status, and the restart and shutdown buttons. 
9. The Workspaces icon. This button toggles between multiple views of the desktop (called "workspaces”), which can help reduce visual clutter on a small screen.  

If your laptop is equipped with Wi-Fi, but there is no Wi-Fi option in the system menu, see the [troubleshooting documentation](https://tails.net/doc/anonymous_internet/no-wifi/index.en.html). Once you connect to Wi-Fi, a Tor Connection assistant will appear to help you connect to the Tor network. Select **Connect to Tor automatically**, unless you are in a country where you need to hide that you're using Tor (in which case you'll need to configure [a bridge](https://tails.net/doc/anonymous_internet/tor/index.en.html#hiding)).

## Optional: Create and Configure Persistent Storage

Tails is amnesiac by default. It will forget everything you have done as soon as you end the session. This isn't always what you want — for example, you may want to install additional software without needing to re-install it each time you start up. Tails has a feature called Persistent Storage, which allows you to save data between sessions. This is explicitly less secure, but necessary for some activities. 

The principle behind Persistent Storage is to create a second storage area (called a partition) on your Tails USB that is encrypted. This new partition allows you to make some data persistent — that is, to keep it between Tails sessions. It's very easy to enable Persistent Storage. To create the [Persistent Storage](https://tails.net/doc/persistent_storage/create/index.en.html), choose **Applications → Tails → Persistent Storage**. 

A window will pop up asking you to enter a passphrase; see [Tails Best Practices](/posts/tails-best/#passwords) for information on passphrase strength. You'll then [configure](https://tails.net/doc/persistent_storage/configure/index.en.html) what you want to keep in Persistent Storage. Persistent Storage can be enabled for several types of data:

**Personal Documents:**

* **Persistent Folder**: Data such as your personal files, documents, or images you're working on. 

**System Settings:**

* **Welcome Screen**: Settings from the Welcome Screen: language, keyboard, and additional settings. 
* **Printers**: [Printer configuration](https://tails.net/doc/sensitive_documents/printing_and_scanning/index.en.html). 

**Network:**

* **Network Connections**: The passwords for Wi-Fi networks can be saved so you don't have to enter them every time.  
* **Tor Bridge**: If Tor Bridge is enabled (for users in countries that censor Tor), the last Tor Bridge you used will be remembered. 

**Applications:**

* **Tor Browser Bookmarks**: Tor Browser bookmarks. 
* **Electrum Bitcoin Wallet**: The bitcoin wallet and settings. 
* **Thunderbird Email Client**: The Thunderbird email inbox, feeds, and OpenPGP keys. 
* **GnuPG**: The OpenPGP keys you create or import into GnuPG and Kleopatra. 
* **Pidgin**: The account files of this chat application (using the XMPP protocol).
* **SSH Client**: All files related to SSH, a protocol used to connect to servers.

**Advanced Settings:**

* **Additional Software**: If this feature is enabled, a list of additional software of your choice will be automatically installed each time you start Tails. These software packages are stored in Persistent Storage. They are automatically updated when you connect to the Internet. [Be careful what you install](https://tails.net/doc/persistent_storage/additional_software/index.en.html#warning).
* **Dotfiles**: In Tails and Linux in general, the names of configuration files often start with a dot, so they are sometimes called "dotfiles". These can be saved in the Persistent Storage. Be careful what configuration settings you change, as changing the defaults can break your anonymity. 

To use Persistent Storage, you must unlock it on the Welcome Screen. If you want to change the passphrase, see the [documentation](https://tails.net/doc/persistent_storage/passphrase/index.en.html). If you ever forget your passphrase, it's impossible to recover it; you'll have to [delete](https://tails.net/doc/persistent_storage/delete/index.en.html) the Persistent Storage and start over. 

In [Tails Best Practices](/posts/tails-best/#using-a-write-protect-switch), we recommend against using Persistent Storage in most cases; most Persistent Storage features do not work well with USBs that have a write-protect switch, any files stored on a Tails USB will leave forensic traces on it, and storing personal data on the Tails USB also prevents it from being compartmentalized when Persistent Storage is unlocked. Any files that need to be persistent can be stored on a second [LUKS-encrypted USB](/posts/tails/#how-to-create-an-encrypted-usb) instead. 

## Upgrading the Tails USB

In order for Tails to remain secure, the operating system must be continually developed and any security vulnerabilities must be addressed through upgrades. It is important to always use the latest version (Tails is updated approximately every month), as security vulnerabilities are regularly discovered in the programs used by Tails, which in the worst case could lead to your identity, IP address, etc. being exposed. A Tails upgrade will fix these vulnerabilities and usually improve other features as well. 

Every time you start Tails, right after you connect to the Tor network, the Tails Upgrader checks to see if you have the latest version of Tails. There are two types of upgrades.

![](/posts/tails/upgrader_automatic.png)

### The automatic upgrade

When an [automatic upgrade](https://tails.net/doc/upgrade/index.en.html) is available, a window will appear with information about the upgrade, and you will need to click **Upgrade now**. Wait a while for it to complete, then click 'Apply upgrade' and your internet will be interrupted for a moment. Wait until you see the Restart Tails window. If the upgrade fails (for example, because you shut down before it was finished), your Persistent Storage will not be affected, but you may not be able to restart your Tails USB. If you are using a USB with a write-protect switch, you will need to unlock it for the dedicated session in which you are performing the upgrade.

### The manual upgrade

Sometimes the upgrade window will tell you that you need to do a manual upgrade. This type of upgrade is only used for major upgrades (which happen approximately every two years) or if there is a problem with automatic upgrades. See the [documentation for manual upgrades](https://tails.net/upgrade/tails/index.en.html). 

# II) Going Further: Several Tips and Explanations

##  Tor 

### What is Tor?

[Tor](/glossary/#tor-network), which stands for The Onion Router, is the best way to be anonymous on the Internet. Tor is open-source software connected to a public network of thousands of relays (servers). Instead of connecting directly to a location on the Internet, Tor takes a detour through three intermediate relays. The Tor Browser uses the Tor network, but other applications can as well if they are configured properly. All default applications included in Tails use Tor if they need to connect to the Internet. 

![](/posts/tails/tor.png)

Internet traffic, including the IP address of the final destination, is encrypted in layers like an onion. Each hop along the three relays removes one layer of encryption. Each relay only knows the relay before it and the relay after it (the exit relay knows that it came from the middle relay and that it goes to such-and-such a website, but not the entry relay). 

![See *anarsec.guide* for the animation.](/posts/tails/anonymous-browsing.gif)

This means that any intermediaries between you and the entry relay know that you're using Tor, but they don't know what site you're going to. Any intermediaries after the exit relay know that someone in the world is going to that site, but they don't know who it is. The site's web server sees you coming from the IP address of the exit relay. 

Tor has several limitations. For example, if someone with the technical and legal means believes you're connecting from a particular Wi-Fi connection to visit a particular site, they can try to match your Wi-Fi connection with what the website activity (a "correlation attack"). However, to our knowledge, this type of attack has never been used by itself to incriminate someone in court. For sensitive activities, use Internet connections that are not tied to your identity to protect yourself in case Tor fails.  

### What is HTTPS?

Virtually all websites today use [HTTPS](/glossary/#https) — the S stands for "secure" (e.g., `https://www.anarsec.guide`). If you try to visit a website without `https://` in the Tor Browser, you will receive a warning before proceeding. If you see `http://` instead of `https://` in front of a website's address, it means that all intermediaries after the exit relay of the Tor network know what you are exchanging with the website (including your credentials). HTTPS means that the digital record of what you do on the site you are visiting is protected by an encryption key that belongs to the site. Intermediaries after the exit relay will know that you are visiting riseup.net, for example, but they will not have access to your emails and passwords, nor will they know if you are checking your emails or reading a random page on the site. A small padlock appears to the left of the site address when you are using HTTPS. 

If there's a yellow warning on the padlock, it means that some elements on the page you're viewing are not encrypted (they use HTTP), which could reveal the exact page or allow intermediaries to partially modify the page. By default, the Tor Browser uses HTTPS-Only Mode to prevent users from visiting HTTP sites. 

![](/posts/tails/http.png)

HTTPS is essential both to limit your web fingerprint and to prevent an intermediary from modifying the data you exchange with websites. If the intermediary cannot decrypt the data, they cannot modify it. For an overview of HTTP / HTTPS connections with and without Tor, and what information is visible to various third parties, see the EFF's [interactive graphic](https://www.eff.org/pages/tor-and-https). 

In short, don't visit websites that aren't using HTTPS. 

### Onion Services: what is .onion?

Have you ever seen a strange website address with 56 random characters ending in .onion? This is called an onion service, and the only way to visit a website using such an address is to use the Tor Browser. The "deepweb" and "darkweb" are terms that have been popularized in the media to describe these onion services. 

![](/posts/tails/lead.webp)

Anyone can set up an .onion site. But why would they want to? Well, the server location is anonymized, so authorities cannot find out where the site is hosted in order to shut it down. When you send data to an .onion site, you enter the site's three Tor relays after the standard Tor circuit. So we have 6 Tor relays between us and the site; we know the first 3 relays, the site knows the last 3, and each Tor node only knows the relay before and after. Unlike a normal HTTPS website, it's all Tor encrypted from end to end. 

This means that both the client (your laptop) and the server (where the site lives) remain anonymous, whereas with a normal website, only the client is anonymous. In addition to being more anonymous for the server, it is also more anonymous for the client: you never leave the Tor network, so it is not possible to intercept you after the exit relay.  

The .onion site address is long because it includes the site's certificate. HTTPS is unnecessary; security depends on knowing the site's .onion address. 

Some sites offer both a classic URL and an .onion address. In this case, if the site has been configured to do so, an indication of ".onion available" should appear next to the URL. If not, sometimes the site will list the .onion address somewhere on its page. To find out the addresses of sites that are only available as .onion, you will need to either find them by word of mouth, or through websites that list other .onion sites, such as this [GitHub page](https://github.com/alecmuffett/real-world-onion-sites).

### Sites that block Tor

Some sites block users who visit through the Tor network, or otherwise make it inconvenient to visit the site. Some sites may force you to complete CAPTCHAs or provide additional personal information (ID, phone number…) before continuing, or they may block Tor altogether. 

![](/posts/tails/new_identity.png)

The site may only block certain Tor relays. In this case, you can change the Tor exit node being used for this site: click the **≣	→ "New Tor circuit for this site"** button. The Tor circuit (path) will change for the current tab, including other open tabs or windows from the same website. You may need to do this several times in a row if you're unlucky enough to encounter multiple banned relays. 

Since all Tor relays are public, it is also possible that the site is blocking the entire Tor network. In this case, you can try using a proxy to access the site, such as `https://hide.me/en/proxy` (but only if you don't have to enter personal information like login credentials). You can also check if the page you want to access has been saved to the Wayback Machine: `web.archive.org`.  

### Cleanly Separate Anonymous Identities 

It is not recommended to perform different Internet tasks that should not be associated with each other during the same Tails session. You must separate different (contextual) identities carefully! For example, it is dangerous to check your personal email and publish an anonymous text during the same session. In other words, you should not be identifiable and anonymous on the Tor network at the same time. You also shouldn't use the Tor network under both pseudonym A and pseudonym B in the same session, as these pseudonyms could be connected through a monitored or compromised Tor exit relay. Shut down and restart Tails between Internet activities under different identities!

The Tor Browser's 'New Identity' feature is not sufficient to completely separate contextual identities in Tails, since it does not reestablish connections outside the Tor Browser, and you keep the same Tor entry node. Restarting Tails is a better solution.

![](/posts/tails/onion-circuits.png)

The Onion Circuits application shows which Tor circuit a server connection (website or otherwise) is using. Sometimes it can be useful to make sure that the exit relay is not located in a certain country, to be further away from the easiest access for investigating authorities. In the example above, the connection to check.torproject.org goes through the relays tor7kryptonit, Casper03, and the exit node blackfish. Clicking on a circuit will display technical details about its relays in the right pane. The Tor Browser's 'New Identity' feature is useful for changing this exit relay without restarting the Tails session, which can be repeated until you have an exit relay you are happy with. We do not recommend using 'New Identity' to switch between identities, but only if you want to change the exit node within the same identity's activities. 

### Tor Browser security settings

![](/posts/tails/safest.png)

Like any software, the Tor Browser has vulnerabilities that can be exploited — various police agencies have Tor Browser exploits for serious cases. To mitigate this, it's important to keep Tails up to date, and you should increase the Tor Browser's security settings: click the shield icon, and then click **Settings...**. By default, it's set to Standard, which maintains a browsing experience comparable to a regular browser. **We strongly recommend that you set it to the most restrictive setting before you start browsing: Safest**. The vast majority of exploits against Tor Browser will not work with the Safest setting. 

The layout of some pages may be changed, and some types of content may be disabled (SVG images, click-to-play videos, etc.). For example, anarsec.guide has two things that will be broken in Safest mode because they rely on Javascript: dark mode and the article's table of contents. Some sites will not work at all with these restrictions; if you have reason to trust them, you can view them with a less restrictive setting on a site-by-site basis. Remember that both "Standard" and "Safer" settings allow scripts to work, which can [break your anonymity](https://arstechnica.com/information-technology/2013/08/attackers-wield-firefox-exploit-to-uncloak-anonymous-tor-users/) in a worst-case scenario.

### Downloading/uploading and the Tor Browser folder

The Tor Browser on Tails is kept in a ["sandbox"](/glossary/#sandboxing) to prevent it from snooping on all your files if a malicious site compromised it. This means there are special considerations when uploading or downloading files using the Tor Browser.

#### Downloading

When you download something using the Tor Browser, it is stored in the Tor Browser folder (`/home/amnesia/Tor Browser/`), which is inside the sandbox. If you want to do anything with the file, you should move it out of the Tor Browser folder. You can use the file manager (**Applications → Accessories → Files**) to do this. 

![](/posts/tails/nautilus.png)

#### Uploading

Similarly, if you want to upload something using the Tor Browser (for example, to include a file in a blog post), you will first need to move or copy the file to the Tor Browser folder. Then it will be available when you select the file to upload in the Tor Browser.

#### RAM

Be aware that if you are downloading or otherwise working with very large files, your RAM (memory) may fill up. This is because your entire Tails session is running in RAM (unless you have set up Persistent Storage, which uses the USB). If the RAM fills up, Tails will slow down or crash. You can mitigate this by closing unneeded applications and deleting other files you have downloaded. In the worst case, you may need to temporarily enable Persistent Storage to download or upload large files via the persistent Tor Browser folder, which uses the USB instead of RAM.

### Share Files with Onionshare

![](/posts/tails/onionshare.png)

It is possible to send a document through an .onion link thanks to [OnionShare](https://tails.net/doc/anonymous_internet/onionshare/index.en.html) (**Applications → Internet → OnionShare**). By default, OnionShare stops the hidden service after the files have been downloaded once. If you want to offer the files for multiple downloads, you need to go to the settings and uncheck "Stop sharing after first download". As soon as you close OnionShare, disconnect from the Internet, or shut down Tails, the files will no longer be accessible. This is a great way to share files because it doesn't require you to plug a USB into someone else's computer, which we [don't recommend](/posts/tails-best/#reducing-risks-when-using-untrusted-computers). The long .onion address can be shared through another channel (such as a [Riseup Pad](https://pad.riseup.net/) you create that is easier to type). 

### Make Correlation Attacks More Difficult

When you request a web page through a web browser, the site's server sends it to you in small "packets" that have a specific size and timing (among other characteristics). When using the Tor Browser, the sequence of packets can also be analyzed to look for patterns that can be matched to those of websites. To learn more, see ["1.3.3. Passive Application-Layer Traffic Patterns"](https://spec.torproject.org/proposals/344-protocol-info-leaks.html). Tor [plans to mitigate this issue in the future](https://gitlab.torproject.org/tpo/team/-/wikis/Sponsor-112). 

To make this ["correlation attack"](/glossary/#correlation-attack) more difficult, disable JavaScript by using Tor Browser on the **Safest** setting. 

Additionally, [doing multiple things at once with your Tor client](https://blog.torproject.org/new-low-cost-traffic-analysis-attacks-mitigations/) is recommended by the Tor team. 

## Included Software

Tails comes with [many applications](https://tails.net/doc/about/features/index.en.html) by default. The documentation gives an overview of [Internet applications](https://tails.net/doc/anonymous_internet/index.en.html), applications for [encryption and privacy](https://tails.net/doc/encryption_and_privacy/index.en.html), and applications for [working with sensitive documents](https://tails.net/doc/sensitive_documents/index.en.html). In the rest of this section, we will only highlight common use cases relevant to anarchists, but read the documentation for more information. 

## Password Manager (KeePassXC)

When you need to know a lot of passwords, it can be nice to have a secure way to store them (i.e. not a piece of paper next to your computer). KeePassXC is a password manager included in Tails (**Applications → Favorites → KeePassXC**) that allows you to store your passwords in a file and protect them with a single master password. 

We recommend that you compartmentalize your passwords — have a different KeePassXC file for each separate project. They can share the same Master Password — the point of compartmentalization is that only one project's passwords are unlocked at any given time. If the Tails session is compromised, the adversary won't get all of your passwords in one fell swoop, just the ones that are currently unlocked. 

>In the terminology used by KeePassXC, a *password* is a random sequence of characters (letters, numbers, and other symbols), while a *passphrase* is a random sequence of words.

![](/posts/tails/seconds.png)

When you [create a new KeePassXC database](https://tails.net/doc/encryption_and_privacy/manage_passwords/index.en.html#index1h1), increase the decryption time in the **Encryption settings** window from the default to the maximum (5 seconds). Then choose a [strong passphrase](/posts/tails-best/#passwords) and save your KeePassXC file. We recommend that you click the small dice icon in the password field to generate a random passphrase of 7-10 words.

This KeePassXC database file will contain all your passwords/passphrases and must persist between sessions on your Persistent Storage or on a separate LUKS-encrypted USB as described in [Tails Best Practices](/posts/tails-best/#using-a-write-protect-switch). As soon as you close KeePassXC or don't use it for a few minutes, it will lock. Make sure you do not forget your KeePassXC passphrase.  

After creating the database itself, you should see an empty “Root” folder. If you'd like to organize your passwords into different groups, right-click this folder and select "New Group...". 

You can now add your first entry. Click **Entries → New Entry**, or click the “plus” icon. Enter the title of the account, your username for the account, and your password. Click the “dice” icon to generate a random password or passphrase for the entry.

![](/posts/tails/entry.png)

To copy a password from the database, select the entry and press CTRL + C. To copy a username, select the entry and press CTRL + B.  

## Really delete data from a USB 

Clicking "Permanently delete" or sending files to the "trash" does not delete data... and it can be very easy to recover it. When you "delete" a file, you are simply telling the operating system that you are no longer interested in the contents of that file. It then deletes its entry in the index of existing files. It can then reuse the space that the data occupied to write something else. 

However, it can take weeks or years before that space is actually used for new files, at which point the old data actually disappears. In the meantime, if you look directly at what is written to the drive, you can find the contents of the files. This is a fairly simple process, automated by many software programs that allow you to "recover" or "restore" data. You can't really delete data, but you can overwrite data, which is a partial solution. 

There are two types of storage: magnetic (HDD) and flash (SSD, NVMe, USB, memory cards, etc.). The only way to erase a file on either is to [reformat the entire drive](/posts/tails/#how-to-create-an-encrypted-usb) and select **Overwrite existing data with zeros**. 

However, traces of the previously written data may still remain. If you have sensitive documents that you really want to erase, it is best to physically destroy the USB after reformatting it. Fortunately, USBs are cheap and easy to steal. Be sure to reformat the drive before destroying it; destroying a drive is often a partial solution. Data can still be recovered from disk fragments, and burning a drive requires temperatures higher than a normal fire (e.g. thermite) to be effective.  

For flash memory drives (USBs, SSDs, SD cards, etc.), use pliers to break the circuit board out of the plastic casing. Use a high-quality house-hold blender to shred the memory chips, including the circuit board, into pieces that are ideally less than two millimeters in size. This blender should not be used for food afterwards, because cleaning it will not adequately remove toxic traces. 

## How to create an encrypted USB 

Store data only on encrypted drives. This is necessary if you want to use a separate LUKS USB instead of Persistent Storage on the Tails USB as advised in [Tails Best Practices](/posts/tails-best/#using-a-write-protect-switch). [LUKS](/glossary/#luks) is the Linux encryption standard. To encrypt a new USB, go to **Applications → Utilities → Disks**. 

* When you insert the USB, a new "device" should appear in the list. Select it and make sure that the description (brand, name, size) matches your device. Be careful not to make a mistake!
* Format it by clicking **≣  → Format the disk**. 
	* In the Erase drop-down list, select **Overwrite existing data with zeroes**. Note that this is not enough to remove all traces of sensitive documents stored on the USB.  
	* In the Partitioning drop-down list, select **Compatible with all systems and devices (MBR/DOS)** .
	* Then click **Format…**

![](/posts/tails/empty_device.png)

* Now you need to add the encrypted partition. 
	* Click on the "**+**" button 
	* Select the size of your partition (all free space) 
	* For "Type" select **internal disk to be used with Linux systems only (Ext4)**; check **Password protected volume (LUKS)**
	* Enter a [strong passphrase](/posts/tails-best/#passwords)

If you insert an encrypted USB, you will be prompted to enter the passphrase. Before removing the drive after you are finished working with it, you must right-click it in **Places → Computer** and select Eject.  

## Encrypting a file with a password or public key

In Tails, you can use the Kleopatra application to [encrypt a file](https://tails.net/doc/encryption_and_privacy/kleopatra/index.en.html#index1h1) with a password or public PGP key. This creates a .pgp file. If you want to encrypt a file, do so in RAM before saving it to a LUKS USB. Once the unencrypted version of a file is saved on a USB, the USB must be reformatted to remove it. 

For the same reason, before decrypting a file first copy it to a Tails folder that's only in RAM (e.g. **Places → Documents**).

## Adding administration rights

Tails requires an administration password (also called a "root" password) to perform system administration tasks. For example:

- Installing additional software 
- Accessing the computer's internal hard drives 
- Running [commands](/glossary/#command-line-interface-cli) in the root terminal
- Accessing certain privileges, such as when you see a window that asks for administration authentication  

By default, the administration password is disabled for added security. This can prevent an attacker with [physical](/glossary/#physical-attacks) or [remote](/glossary/#remote-attacks) access to your Tails system from gaining administration privileges. If you set an administration password for your session, you are creating another vector to potentially bypass Tails security. 

To set an administration password, you must select an administration password on the Welcome Screen when you start Tails. This password is only valid for the duration of the session. 

## Installing additional software

If you install new software, it's up to you to make sure it's secure. Tails forces all software to connect to the internet through Tor, so you will need to [configure this for applications that use the Internet](https://tails.net/doc/persistent_storage/additional_software/index.en.html#index5h2). The software used in Tails is audited for security, but this may not be the case for what you install. Before installing new software, it's best to make sure that Tails doesn't already have software that does the job you want it to do. If you want additional software to persist beyond a single session, you need to enable "Additional Software" in the Persistent Storage [configuration](https://tails.net/doc/persistent_storage/configure/index.en.html). 

For more information, see the documentation on [installing additional software](https://tails.net/doc/persistent_storage/additional_software/index.en.html#index3h1).  

## Remember to make backups!

A Tails USB is easily lost, and USBs have a much shorter lifespan than hard drives (especially the cheap ones). If you have important data on it, remember to back it up regularly. If you use a second LUKS-encrypted USB, this is as simple as using the File Manager to copy files to a backup LUKS-encrypted USB. 

If you use Persistent Storage, see the [documentation for backing it up](https://tails.net/doc/persistent_storage/backup/index.en.html). 

## Privacy screen 

A [privacy screen](https://en.wikipedia.org/wiki/Monitor_filter) can be added to your laptop screen to prevent people (or hidden cameras) from seeing the content unless they are positioned directly in front of it.

# III) Troubleshooting Issues 

***The computer tries to boot the USB but it doesn't work***

Check the error messages you get (for example, if you have an old 32-bit computer, it won't work with Tails). If it says `Error starting GDM with your graphics card`, the issue is with the graphics card; check the documentation for [Known issues with graphics cards](https://tails.net/support/known_issues/graphics/index.en.html). You can also check the list of [known issues](https://tails.net/support/known_issues/index.en.html) on the Tails site for your computer model.

If the Tails Boot Loader page appears, try booting into Tails troubleshooting mode. 

***My Tails USB won't start anymore! (and it did start before on the same computer)***

After an upgrade or otherwise, Tails no longer starts on your computer. You have three options:

1) See if the [Tails news page](https://tails.net/news/index.en.html) mentions any problems with the upgrade. 
2) [Perform a manual upgrade](/posts/tails/#the-manual-upgrade), which may be necessary if the computer was turned off before an automatic upgrade was complete.
3) If the first two solutions don't work, the USB is too old, of poor quality, or has been broken. If you need to recover data from Persistent Storage, plug that USB into a Tails session using another USB. It will appear as a normal USB that you will need to unlock with your password. If you can't access your data on another Tails USB that has Persistent Storage enabled, your USB may be dead. 

***I can't connect to a public Wi-Fi network with an authentication page (a captive portal)***

If you need to connect to Wi-Fi using a captive portal, you must enable Unsafe Browser in the Welcome Screen. Connect to Wi-Fi, and then open **Applications → Internet → Unsafe Browser**. You enter the URL of a site that isn't sketchy (e.g. wikipedia.org) to access the authentication page. Once you've completed the captive portal page, wait until Tor is ready, and then close the unsafe browser.  

***What if I run out of space on a USB?***

If you run out of space on a USB drive, or if you see less data than you actually have on your USB, check "Show hidden files" in the file browser. There you will see new files named `.something`. The file `.Trash-10xx` is taking up space (and if you right-click on it and select "Move to Trash" it will be removed completely). Don't change any other hidden files.  

***A file always opens in read-only mode or does not open at all?***

In some programs, this is normal if the same file is already open. If this isn't the case, use the same trick as in the paragraph above. You enable Show hidden files. There will be a .lock file with the same name as the file you have a problem with. Delete this file, which indicates that it is already open elsewhere. If that's not the issue, you need to change the permission rights of the file.

***I can't install Tails on a USB***

Make sure your USB is not [known to have issues](https://tails.net/support/known_issues/index.en.html#problematic-usb-sticks) with Tails. [Format](/posts/tails/#how-to-create-an-encrypted-usb) the entire USB and try the installation again. 

***Is an application slowing down Tails? The screen is glitching?***

Try pressing the Windows key, or the Cmd key for Mac, which will open the window with all your running applications, from where you can exit them. If that doesn't work, you'll need to force a shutdown by holding down the power button.

***Add a printer***

You go to **Applications → System Tools → Settings → Devices → Printers → "+" → Add a printer**. Some printer models may not work with Tails (or may be difficult to set up).

***Unable to install new software***

Sometimes the Synaptic Package Manager will refuse to install software. In this case, use a root terminal (which requires an administration password): install with the command `apt update && apt install [package_name]`

# Best Practices 

[Tails Best Practices](/posts/tails-best) are important to establish before using Tails for highly sensitive activities like [claiming an action](https://www.notrace.how/resources/#how-submit). To avoid overwhelming yourself, start by learning how to use Tails in basic ways, such as reading anarchist websites or writing texts. See the [Tails tag](/tags/tails/) for tutorials on topics like [removing identifying metadata from files](/posts/metadata/).

*This article draws from* [TuTORiel Tails](https://infokiosques.net/spip.php?article1726) *(in French), and* [Capulcu #1](https://capulcu.blackblogs.org/neue-texte/bandi/) *(in German).*

