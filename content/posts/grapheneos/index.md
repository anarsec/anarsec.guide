+++
title="GrapheneOS for Anarchists"
date=2024-04-22

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "mobile", "beginner"]

[extra]
toc = true
blogimage="/images/graphene-pink.png"
dateedit=2024-11-25
a4="grapheneos-a4-en.pdf"
letter="grapheneos-letter-en.pdf"
+++

While [anarchists should minimize the presence of phones in their lives](/posts/nophones/), if you do decide to use a phone, make it as difficult as possible for an adversary to geotrack it, intercept its messages, or hack it. This means using GrapheneOS. 
<!-- more -->

# What is GrapheneOS?

GrapheneOS is a security-focused version of the Android [operating system](/glossary#operating-system-os). Standard Android smartphones have Google baked into them (for example, [Google Play Services](https://en.wikipedia.org/wiki/Google_Play_Services) has irrevocable access to your files, call logs, location, etc.). GrapheneOS removes all Google apps and services by default, uses hardware-based security to [make it far more difficult](https://grapheneos.org/faq#encryption) to bypass the disk encryption, and it is significantly [hardened](/glossary#hardening) against hacking. There are other alternative Android operating systems, [but they don't have comparable security](https://eylenburg.github.io/android_comparison.htm). See the [GrapheneOS documentation](https://grapheneos.org/features) for an extensive list of privacy and security improvements over standard Android. 

Due to the nature of [how the technology works](https://citizenlab.ca/2023/10/finding-you-teleco-vulnerabilities-for-location-disclosure/), cell phones connecting to cell towers give the provider a history of your geolocation. For this reason, we recommend that you leave your smartphone at home and use it like a landline, connecting to the Internet via Wi-Fi in airplane mode, rather than using a SIM card to connect through cell towers. Even if you use an anonymously purchased SIM card, if it is linked to your identity in the future, the service provider can be retroactively queried for all geolocation data. Furthermore, it's not enough to only leave your phone at home when you're going to a demo or action, as this will [stand out](/posts/nophones/#metadata-patterns) as an outlier and serve as an indication of conspiratorial activity in that time window. 

# Installation 

[Google Pixel](https://www.privacyguides.org/android/#google-pixel) phones are currently the only devices that meet the hardware security requirements of GrapheneOS — see [supported](https://grapheneos.org/faq#device-support) and [recommended devices](https://grapheneos.org/faq#recommended-devices). "Hardware memory tagging support" is a very powerful security feature that was introduced with the Pixel 8, making it substantially harder to remotely exploit user installed apps such as Signal which has a ["massive amount of remote attack surface"](https://grapheneos.social/@GrapheneOS/111479318824446241). 

Starting with the Pixel 8, Pixel devices will receive at least 7 years of security updates from the date of release. End-of-life devices (GrapheneOS "extended support" devices) do not receive full security updates and therefore are not recommended. See [how long GrapheneOS will support the device for](https://grapheneos.org/faq#device-lifetime). 

Avoid carrier variants of the phone, i.e. don't buy one from a mobile operator, which may prevent you from installing GrapheneOS. The cheapest option is to buy the "a" model right after the next flagship model is released — for example, the Google Pixel 6a after the Pixel 7 is released. 

[GrapheneOS can be installed](https://grapheneos.org/install/) using a web browser or the [command line](/glossary#command-line-interface-cli). If you are uncomfortable with command line, the web browser installer is fine; as the [instructions note](https://grapheneos.org/install/cli#verifying-installation), "Even if the computer you used to flash GrapheneOS was compromised and an attacker replaced GrapheneOS with their own malicious OS, it can be detected with Auditor", which is explained below. Both methods list the officially supported operating systems that you can install from. 

The first time you boot Graphene, it will ask you if you want to connect to Wi-Fi. Don't, we need to do [hardware-based attestation](/posts/grapheneos/#auditor) first. Never set up fingerprint authentication. Set a [strong password](/posts/tails-best/#passwords).  

There is no official support for installing from Qubes OS, but it is possible with the following steps.

<details>
<summary>

**Installation on Qubes OS**

</summary>
<br>

*These instructions assume that your sys-usb qube is disposable, which is the default in the [post-installation settings](/posts/qubes/#getting-started), and that it uses a Debian template.*

* In a disposable Whonix-Workstation qube, open the [command line installation guide](https://grapheneos.org/install/cli) using Tor Browser.  
* You will read "Installing from an OS in a virtual machine is not recommended. USB passthrough is often not reliable." This means we will be doing everything from sys-usb, which does not use USB passthrough. If you set sys-usb to be disposable when you installed Qubes OS, it will be reset after a reboot. 
* For simplicity, we will temporarily enable networking in sys-usb. It is also possible to keep sys-usb offline by copying platform-tools and the factory image from the disposable Whonix-Workstation qube into sys-usb, and getting udev rules from [Github](https://github.com/M0Rf30/android-udev-rules) instead of apt. In the **Settings → Basic** tab of sys-usb, make the following changes:
    * Private storage max size: 10.0 GB
    * Net qube: sys-firewall
    * Press **Apply**
* Follow the installation instructions in the sys-usb terminal. When you get to **Flashing factory images**, don't run `./flash-all.sh`. Instead, scroll down to Troubleshooting and run the command that uses a different temporary directory. The flash script is expected to print out messages like `archive does not contain 'example.img'`. 
* When you're done, restart sys-usb. If it is disposable, the changes you made will be gone. Don't forget to change the sys-usb qube settings back:
	* Net qube: (none)

</details>

# System navigation

GrapheneOS uses [gesture navigation](https://grapheneos.org/usage#gesture-navigation) by default. The essentials are:

* The bottom of the screen is a dedicated touch zone for system navigation. 
* Swiping up from the navigation bar while removing your finger from the screen is the **Home** gesture.
* Swiping up from the navigation bar while keeping your finger on the screen before letting go is the **Recent Apps** gesture.
* Swiping from the left or right side of the screen within the app (not the navigation bar) is the **Back** gesture. 
* The launcher uses a swipe-up gesture from anywhere on the screen to open the app drawer from the home screen. You must start this gesture above the system navigation bar.

# Auditor 

In the post-installation instructions, **Hardware-based attestation** is the last step. The Auditor app included in GrapheneOS uses hardware security features to monitor the integrity of the device's firmware and OS software. This is critical because it will alert you if these components of the device are maliciously tampered with. Note that Auditor doesn't necessarily check whether the user-level apps running on your device are malicious. The Auditor app must be configured immediately after GrapheneOS is installed, before any Internet connection is made. 

How does it work? Your new device is the *auditee*, and the *auditor* can be either another instance of the Auditor app on a friend's phone or the [Remote Attestation Service](https://attestation.app/) — we recommend doing both. The *auditor* and *auditee* pair to create a private key, and if the *auditee's* operating system is tampered with after the pairing is complete, the *auditor* will be alerted during the next test.

First, immediately after installing the device and before connecting to the Internet, [perform a "local verification"](https://attestation.app/tutorial#local-verification). This requires the presence of a friend whom you see semi-regularly and who has the Auditor app (on any Android device). The first pairing will show a brown background, and subsequent audits will show attestation results with a green background if nothing is remiss. There is no remote connection established between the phones of the auditor and auditee; you must perform these verifications in person. 

We recommend using the phone as a Wi-Fi only device. Turn on airplane mode. It "will fully disable the cellular radio transmit and receive capabilities, which will prevent your phone from being reached from the cellular network and stop your carrier (and anyone impersonating them to you) from tracking the device via the cellular radio." Leave airplane mode enabled at all times — otherwise the phone will interact with cellular networks even if there is no SIM card in the phone.

You are now ready to connect to Wi-Fi. Once you have an Internet connection, we recommend that you immediately set up a [scheduled remote verification](https://attestation.app/tutorial#scheduled-remote-verification) with an email that you check regularly. You can always log back in to view your attestation history. The default delay until alerts is 48 hours; if you know your phone will be off for a longer period, you can update the configuration to a maximum of two weeks. If your phone will be off for more than two weeks (for example, if you leave it at home while traveling), simply ignore the notification emails.  

If Auditor ever detects tampering, you should immediately treat the device as untrusted. [Forensic analysis](https://www.notrace.how/threat-library/mitigations/computer-and-mobile-forensics.html) may be able to reveal how the compromise occured, which helps to prevent it from happening again. You can get in touch with a service like [Access Now’s Digital Security Helpline](https://accessnow.org/help), though we recommend not sending them any personal data.

# User Profiles 

User profiles are a feature that allows you to compartmentalize your phone, similar to how [Qubes OS](/posts/qubes/#what-is-qubes-os) compartmentalizes your computer. User profiles have their own instances of apps, app data, and profile data. Apps can't see the apps in other user profiles and can only communicate with apps within the same user profile. In other words, user profiles are isolated from each other — if one is compromised, the others aren't necessarily. 

The Owner user profile is the only profile that is present when you turn on the phone. You can create additional user profiles. Each profile is [encrypted](/glossary/#encryption) with its own encryption key and cannot access the data of other profiles. Even the device owner cannot view the data of other profiles without knowing their password. 

We'll now create a second user profile for all applications that don't require Google Play services:

* **Settings → System → Multiple users**, press **Add user**. You can name it Default and press **Switch to Default**. 
* Set a password that is different from your Owner user profile password. 
	* This is the profile that you will be regularly unlocking throughout the day. This means that you only have to enter the Owner password upon boot, which allows it to be very strong. For the Default password, choose either the combination of a weak password + a short locking time, or a [strong password](/posts/tails-best/#passwords) + a longer locking time. The first option puts trust in the rate-limiting of password attempts [enforced by the secure element](https://grapheneos.org/faq#encryption). The second option doesn't put trust in the rate-limiting, given it could be bypassed through a secure element vulnerability, but has the trade-off that the profile data is vulnerable if the device is left unattended while unlocked. You can also have a strong password + a short locking time if you don't unlock the device many times a day. Keep in mind that if police ever seize your device (such as during a daytime house raid), it should ideally be turned off, and at minimum, it should be locked (which starts the countdown to the Auto reboot feature mentioned below). 
* In the Default user profile, you can set the locking time with **Settings → Security → Screen lock settings → Lock after screen timeout**, and the screen timeout with **Settings → Display → Screen timeout**.

Later, we will optionally create a third user profile for applications that require Google Play services. 

When you press **End session** on a profile, that profile's data is encrypted at rest. A shortcut for switching between different user profiles is located at the bottom of Quick Settings (accessible by swiping down twice from the top of the screen). 

To reiterate, the user profiles and their purposes are:

**1) Owner**

* Where applications are installed 	

**2) Default**

* Where applications are used  

**3) Google (optional)**

* Where applications that require Google Play services are used

# How to Install Software

The GrapheneOS app store contains the standalone applications developed by the GrapheneOS project, such as Vanadium, Auditor, Camera, and PDF Viewer. These are automatically updated.  

To install additional software, the GrapheneOS app store can install two other app stores: [Sandboxed](/glossary/#sandboxing) Google Play and [Accrescent](https://accrescent.app/). ["Google Play receives absolutely no special access or privileges on GrapheneOS."](https://grapheneos.org/features#sandboxed-google-play) Accrescent currently only has a small selection of apps. 

Avoid the F-Droid app store due to its numerous [security issues](https://www.privacyguides.org/en/android/obtaining-apps/#f-droid). We also don't recommend the [Aurora Store](https://www.privacyguides.org/en/android/obtaining-apps/#aurora-store), as it has [some of the same security issues as F-Droid](https://privsec.dev/posts/android/f-droid-security-issues/#conclusion-what-should-you-do).  

The approach we will take is that all applications will be installed in the Owner user profile, using Sandboxed Google Play and/or Accrescent. We'll then "disable" these installed apps in the Owner user profile and delegate them to the Default profile. This is because we will only actually use them from the Default user profile (except, [if you ever use the phone away from home](/posts/grapheneos/#force-all-network-traffic-through-a-vpn), a VPN app that needs to run in all profiles).   

## Software from Sandboxed Google Play and Accrescent

To install and configure Sandboxed Google Play:

* In the Owner user profile, install Sandboxed Google Play by opening Apps and installing Google Play services (this will also install the Google Services Framework and the Google Play Store).  
* The Google Play Store requires a Google account to sign in, but one with false info can be created for exclusive use with the Google Play Store.   
* Once installed and signed in, disable the advertising ID: **Settings → Apps → Sandboxed Google Play → Google Settings → Ads**, and select *Delete advertising ID*. 
* Automatic updates are enabled by default on the Google Play Store: **Google Play Store Settings → Network Preferences → Auto-update apps**.   
* Notifications for Google Play Store and Google Play Services must be enabled for auto-updates to work: **Settings → Apps → Google Play Store / Google Play Services → Notifications**. If you get notifications from the Play Store that it wants to update itself, [accept them](https://discuss.grapheneos.org/d/4191-what-were-your-less-than-ideal-experiences-with-grapheneos/18).

For Accrescent, simply install it through Apps in the Owner user profile. 

You are now ready to install applications from the Google Play Store and Accrescent. See [Encrypted Messaging for Anarchists](/posts/e2ee/) for ideas. 

### Delegating apps 

Now we will delegate apps to the profiles they are needed in:

* In the Owner profile, disable all applications downloaded from the Play Store or Accrescent (except for a VPN app): **Settings → Apps → [Example] → Disable**. 
* To install any app in the Default user profile: **Settings → System → Multiple users → Default → Install available apps**, then select it. 

## Software That Isn't On the Play Store or Accrescent

Most software is available either through the Play Store or Accrescent. For the small number of apps which aren't, downloading individual APK installer files isn't a good solution because you would then have to remember to update them yourself (there are exceptions, like Signal, which is designed to update itself). 

[Obtainium](https://www.privacyguides.org/en/android/obtaining-apps/#obtainium) is an app manager which allows you to automatically update apps after installing them from an APK file (an APK is found from the developer's own releases page such as GitHub or the developer's website). It is available on their [GitHub Releases page](https://github.com/ImranR98/Obtainium/releases) — `app-arm64-v8a-release.apk` of the latest release is what you want (arm64-v8a is the processor architecture). 

If you need apps that aren't available in the Play Store or Accrescent, install Obtainium in the Owner user profile (and don't disable it), after first verifying its authenticity with AppVerifier (which is available through Accrescent). Use the same process as above to install apps into the Owner user profile (but this time through Obtainium), disabling them then delegating them to a secondary profile. AppVerifier integrates with Obtainium so that before Obtanium installs an APK you can verify its authenticity — AppVerifier can approve selected apps, or you can manually compare the APK's fingerprint to somewhere that the developer has published it.  

## Software That Requires Google Play Services

If there is an app you want to use that requires Google Play services, create another secondary user profile for it. This is also a good way to isolate any app you need to use that isn't [open-source](/glossary/#open-source) or reputable. You will need to install and configure Sandboxed Google Play in this "Google" user profile. 

Many [banking apps](https://grapheneos.org/usage#banking-apps) will require Sandboxed Google Play. However, banking can simply be accessed through a computer to avoid the need for this "Google" user profile.  

# VoIP

A Wi-Fi only smartphone doesn't require a service plan. As explained in [Kill the Cop in Your Pocket](/posts/nophones#bureaucracy), bureaucracies often require a phone number that can be called from a normal phone (without encryption). [VoIP](/glossary#voip-voice-over-internet-protocol) applications allow you to create a number and make calls over Wi-Fi rather than through cell towers. A phone number is also occasionally required to register for an application, and a VoIP number will usually work. 

Some of the VoIP applications [that work on computers](/posts/nophones#bureaucracy) also work on smartphones. The [jmp.chat](https://www.kicksecure.com/wiki/Mobile_Phone_Security#Phone_Number_Registration_Unlinked_to_SIM_Card) VoIP service can be paid for in Bitcoin, and it can be used with their [Cheogram app](https://cheogram.com/). There are also mobile-only paid options such as MySudo (although it only works in a [handful of countries](https://support.mysudo.com/hc/en-us/articles/360019983274-Which-countries-are-supported-for-Sudo-phone-numbers)). A MySudo subscription can be purchased anonymously with [Google Play gift cards](https://support.google.com/googleplay/answer/3422734), but this is probably unnecessary if the point is to give the number to bureaucracies. MySudo requires Google Play Services.  

# Force All Network Traffic Through a VPN 

It is best to force all of GrapheneOS's network traffic through a [VPN](/glossary/#vpn-virtual-private-network) — this puts your trust in your VPN instead of an inherently untrustworthy Internet Service Provider. As the [Security Lab](https://securitylab.amnesty.org/latest/2023/10/technical-deep-dive-into-intellexa-alliance-surveillance-products/) notes:

>Using a reputable VPN provider can provide more privacy against surveillance from your ISP or government and prevent network injection attacks from those entities. A VPN will also make traffic correlation attacks — especially those targeting messaging apps — more difficult to perform and less effective.

There are two ways you can run a VPN: from your phone or from your networking device (either a router or a hardware firewall). When using your phone from home, we recommend the latter. 

It's unnecessary to "double up" a VPN — if its running on your networking device, it doesn't need to be running on your phone, and vice-versa. This means that a phone running a VPN should disable it before connecting to Wi-Fi configured with a "VPN Kill Switch". 

If you ever use the phone away from home, you should configure GrapheneOS to force all network traffic through a VPN — install the VPN app in every user profile. All standard GrapheneOS connections will be forced through the VPN (except for [connectivity checks](https://grapheneos.org/faq#default-connections), which can be optionally [disabled](https://privsec.dev/posts/android/android-tips/#connectivity-check)). Note that **Always-on VPN** and **Block connections without VPN** are enabled by default on GrapheneOS. Keep in mind that you'll want to disable the VPN app before connecting to your home's "VPN Kill Switch" Wi-Fi. 

If you can afford to pay for a VPN, we recommend both [Mullvad](https://www.privacyguides.org/en/vpn/#mullvad) and [IVPN](https://www.privacyguides.org/en/vpn/#ivpn). Otherwise, you can use RiseupVPN, although it has far fewer users to blend in with, and it doesn't meet several important [security criteria for VPN providers](https://www.privacyguides.org/en/vpn/#criteria), such as published security audits of its code and infrastructure. A VPN subscription should be purchased anonymously — vouchers are available from [Mullvad](https://mullvad.net/en/blog/2022/9/16/mullvads-physical-voucher-cards-are-now-available-in-11-countries-on-amazon/) and [IVPN](https://www.ivpn.net/knowledgebase/billing/voucher-cards-faq/) to purchase the subscription anonymously without [Monero](https://www.privacyguides.org/en/cryptocurrency/#monero). 

# Tor 

You may want to use [Tor](/glossary/#tor-network) from a smartphone. However, if you need the anonymity of Tor rather than the privacy of Riseup VPN, you should use [either Qubes OS or Tails](/posts/qubes/#when-to-use-tails-vs-qubes-os) on a computer. The [Graphene docs](https://grapheneos.org/usage#web-browsing) recommend avoiding Gecko-based browsers like Tor Browser, as these browsers "do not have internal sandboxing on Android." Orbot is an app that can route traffic from any other app on your device through the Tor network, but simply using the Vanadium browser through Orbot is [not recommended by the Tor Project](https://support.torproject.org/tbb/tbb-9/). 

# Recommended Settings and Habits

Turn off the phone overnight and when you leave it at home. [Full Disk Encryption](/glossary/#full-disk-encryption-fde) is most effective when the device is turned off. Additionally, if the operating system is compromised by [malware](/glossary/#malware) a reboot can [clean the malware from your system](https://www.privacyguides.org/en/os/android-overview/#verified-boot), so it is best practice to shut down the device daily.  

In the Owner user profile:

* **Settings → Security → Auto reboot:** 18 hours or less
	* The automatic reboot, if no profile has been unlocked for several hours, will put the device fully at rest again. It will reboot overnight if you forget to turn it off. If the police ever manage to get their hands on your phone while it is in a lock-screen state, this setting [will return it to more effective encryption once the time has elapsed](https://grapheneos.social/@GrapheneOS/112204443938445819). 
* **Settings → Security → USB-C Port:** [Charging-only or Off](https://grapheneos.social/@GrapheneOS/112204446073852302)
* **Settings → System → Multiple users → [Username] → App installs and updates:** Disabled 
	* Once you have all the applications you need in a secondary user profile, disable app installation in that profile — apps that are delegated to a secondary user profile from the Owner profile (via "Install available apps", as described above) will still be updated.
* **Settings → System → Multiple users:** Send notifications to current user (enabled) 	
	* It is convenient to be able to receive notifications from any user profile. 
* **Settings → Security & privacy → Device unlock → Duress Password:** Set a [duress password](https://grapheneos.org/features#duress) that can be used to irreversibly wipe the device. 

In all profiles:

* Leave the Global Toggles for Bluetooth, location, camera access, and microphone access disabled when you don't need them for a specific purpose. Apps cannot use disabled features (even with individual permissions) until they are re-enabled. Also set a Bluetooth timeout: **Settings → Connected devices → Bluetooth timeout:** 2 minutes 
* In the "Messaging" app, disable **Settings → Advanced → Auto-retrieve**
* Many applications allow you to "share" a file with them for media upload. For example, if you want to send a picture on Signal, do not grant Signal access to "photos and videos" because it will have access to all of your pictures. Instead, in the Files app, long-press to select the picture, and then share it with Signal.
* When an app asks for storage permissions, select [Storage Scopes](https://grapheneos.org/usage#storage-scopes). This will make the app think that it has all the storage permissions it is requesting, when in fact it has none. The same is true for [Contact Scopes](https://grapheneos.org/usage#contact-scopes).

# How to Backup 

Don’t use cloud backups. You can't trust the corporate options, and they're the easiest way for the police to access your data. If you must back up your phone, back it up to your encrypted computer.

GrapheneOS currently offers [Seedvault](https://grapheneos.org/features#encrypted-backups) as a backup solution, but it's not very reliable. As the [documentation notes](https://grapheneos.org/faq#file-transfer), connecting directly to a computer requires "needing to trust the computer with coarse-grained access", so it is best to avoid it. Instead, you can manually back up files by copying them to a USB-C flash drive using the Files app, or sending them to yourself using an [encrypted messaging app](/posts/e2ee/). 

# Password Management

If you feel you need a password manager, [KeePassDX](https://www.privacyguides.org/en/passwords/#keepassdx-android) is a good option. However, most app credentials can be stored in [KeePassXC](/posts/tails/#password-manager-keepassxc) on a computer because they don't need to be entered regularly. The setup described in this guide requires memorizing two passwords: 

1) The Owner user profile (boot password)
2) The Default user profile  
3) (Optional) Apps like [Cwtch](/posts/e2ee/#cwtch) and [Molly](/posts/e2ee/#signal) have their own passwords. 

For advice on password quality, see [Tails Best Practices](/posts/tails-best/#passwords).

# Linux Desktop Phones

Why recommend a Pixel over a Linux desktop phone? Linux desktop phones like the [PinePhone Pro](https://en.wikipedia.org/wiki/PinePhone_Pro) are [much easier to hack than GrapheneOS](https://madaidans-insecurities.github.io/linux-phones.html) because they lack modern security features like full system MAC policies, verified boot, strong app sandboxing, and modern [exploit](/glossary/#exploit) mitigations. Their hardware architecturally lacks modern security features like hardware based encryption (via a Trusted Execution Environment/Secure Element) and has questionable integration of components such as the modem. 

# Wrapping Up

With the set-up described in this guide, if a cop starts with your name, they won’t be able to simply look it up in a cellular provider database to get your phone number. If you use the phone as a Wi-Fi only device and always leave it at home, it cannot be used to determine your movement profile and history. If you use a VoIP number accessed through a VPN, even if that number is known it can't be used to locate you. All communications with comrades use [end-to-end encryption](/posts/e2ee/) so they do not facilitate [network mapping](https://notrace.how/threat-library/techniques/network-mapping.html). Even if you are unlucky enough to be targeted by a well-funded investigation, the hardened operating system makes it difficult to compromise with spyware, and such a compromise should be [detectable](/posts/grapheneos/#auditor). 

By storing the phone in a tamper-evident manner when it's not in use, you'll be able to tell if it's been physically accessed. See the guide [Make Your Electronics Tamper-Evident](/posts/tamper/). 

The GrapheneOS [forum](https://discuss.grapheneos.org/) is generally very helpful for any remaining questions you may have. 

For information on burner phones, see the [No Trace Project](https://notrace.how/threat-library/mitigations/anonymous-phones.html).
