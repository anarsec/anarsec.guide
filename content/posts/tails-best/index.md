+++
title="Tails Best Practices"
date=2024-04-25

[taxonomies]
categories = ["Defensive"]
tags = ["linux", "tails", "beginner"]

[extra]
blogimage="/images/bestpractices-dither.png"
toc=true
dateedit=2024-11-25
a4="tails-best-a4-en.pdf"
letter="tails-best-letter-en.pdf"
+++

All anarchists should know how to use Tails — this text describes some additional precautions you can take that are relevant to an anarchist [threat model](/glossary/#threat-model). Not all anarchist threat models are the same, and only you can decide which mitigations are worth putting into practice for your activities, but we aim to provide advice that is appropriate for high-risk activities like claiming an action. If you are new to Tails, start with [Tails for Anarchists](/posts/tails/). 

<!-- more -->

We'll begin by looking at the three topics covered on the [Tails Warnings page](https://tails.net/doc/about/warnings/index.en.html): protecting your identity, limitations of the Tor network, and untrusted computers. 

# Protecting your identity when using Tails

![](/posts/tails-best/identity.png)

> Tails is designed to hide your identity. But some of your activities could reveal your identity:
>
> 1. Sharing files with [metadata](/glossary/#metadata), such as date, time, location, and device information
> 2. Using Tails for more than one purpose at a time

## 1. Sharing files with metadata

You can mitigate this first issue by **cleaning metadata from files before sharing them**:

* To learn how, see [Remove Identifying Metadata From Files](/posts/metadata/). 

## 2. Using Tails for more than one purpose at a time

You can mitigate this second issue by what's called **"compartmentalization"**:

* [Compartmentalization](https://notrace.how/threat-library/mitigations/compartmentalization.html) means keeping different activities or projects separate. If you use Tails sessions for more than one purpose at a time, an adversary could link your different activities together. For example, if you log into different accounts on the same website in a single Tails session, the website could determine that the accounts are being used by the same person. This is because websites can tell when two accounts are using the same Tor circuit.
* To prevent an adversary from linking your activities while using Tails, restart Tails between different activities. For example, restart Tails between checking different project emails.
* Tails is amnesiac by default, so to save any data from a Tails session, you must save it to a USB. If the files you save could be used to link your activities together, use a different encrypted ([LUKS](/glossary/#luks)) USB stick for each activity. For example, use one Tails USB stick for moderating a website and another for researching actions. Tails has a feature called Persistent Storage, but we do not recommend using it for data storage, which we explain [below](/posts/tails-best/#using-a-write-protect-switch).  

# Limitations of the Tor network

![](/posts/tails-best/tor.png)

> Tails uses the [Tor network](/glossary/#tor-network) because it is the strongest and most popular network to protect from surveillance and censorship. But Tor has limitations if you are concerned about:
>
> 1. Hiding that you are using Tor and Tails
> 2. Protecting your online communications from determined, skilled attackers

## 1. Hiding that you are using Tor and Tails

You can mitigate this first issue by **[Tor bridges](https://tails.net/doc/anonymous_internet/tor/index.en.html#bridges)**:

* Tor Bridges are secret Tor relays that hide your connection to the Tor network. However, this is only necessary where connections to Tor are blocked, such as in heavily censored countries, by some public networks, or by some parental control software. This is because Tor and Tails don't protect you by making you look like any other Internet user, but by making all Tor and Tails users look the same. It becomes impossible to tell who is who among them.

## 2. Protecting against determined, skilled attackers

An [*end-to-end correlation* attack](/glossary/#correlation-attack) is a theoretical way that a global adversary could break Tor's anonymity:

> A powerful adversary, who could analyze the timing and shape of the traffic entering and exiting the Tor network, might be able to deanonymize Tor users. These attacks are called *end-to-end correlation* attacks, because the attacker has to observe both ends of a Tor circuit at the same time. [...] End-to-end correlation attacks have been studied in research papers, but we don't know of any actual use to deanonymize Tor users.

### Non-Targeted and Targeted Correlation Attacks

As described in the quotation above, a global adversary (i.e. the NSA) may be capable of breaking Tor through a correlation attack. If this happens, the Internet address you used in a coffee shop without CCTV cameras will only lead to your general area (e.g. your city) because it is not associated with you. Of course, this is less true if you use the location routinely. Correlation attacks are even less feasible against connections to an .onion address because you never leave the Tor network, so there is no "end" to correlate with through network traffic analysis (if the server location is unknown to the adversary). It is worth emphasizing that "End-to-end correlation attacks have been studied in research papers, but we don't know of any actual use to deanonymize Tor users." 

What we will term a "targeted" correlation attack is much more likely because a non-global adversary (i.e. local law enforcement) is capable of it, if you are already in their sights and a target of [physical surveillance](https://notrace.how/threat-library/techniques/physical-surveillance/covert.html) and/or [digital surveillance](https://notrace.how/threat-library/techniques/targeted-digital-surveillance.html). This is a subtype of correlation attack where the presumed target is already known, thus making the attack easier to achieve because it vastly reduces the amount of data to filter through for correlation. A non-targeted correlation attack used to deanonymize a Tor user is unprecedented in current evidence used in court, although [a "targeted" correlation attack has been used](https://medium.com/beyond-install-tor-signal/case-file-jeremy-hammond-514facc780b8) as corroborating evidence — a suspect had already been identified, which allowed investigators to correlate their observed footprint with specific online activity. Specifically, they correlated Tor network traffic coming from the suspect's house with the times their anonymous alias was online in chatrooms. 

To explain how this works, it helps if you have a basic understanding of what Tor information is visible to various third parties — see the EFF's [interactive graphic](https://www.eff.org/pages/tor-and-https). For a non-targeted correlation attack, the investigator will need to *start from after Tor's exit node*: take the specific online activity coming from the exit node and try to correlate it with an enormous amount of global data that is entering Tor entry nodes. However, if a suspect is already identified, the investigator can instead do a "targeted" correlation attack and *start from before Tor's entry node*: take the data entering the entry node (via the suspect's *physical or digital footprint*) and try to correlate it with *specific online activity* coming from an exit node. 

For your *physical footprint*, a surveillance operation can observe you go to a cafe regularly, then try to correlate this with online activity they suspect you of (for example, if they suspect you are a website moderator, they can try to correlate these time windows with web moderator activity). For your *digital footprint*, if you are using Internet from home, an investigator can observe all your Tor traffic and then try to correlate this with online activity they suspect you of. For your *specific online activity*, a more sophisticated analysis would involve logging the connections to the server for detailed comparison, and a simple analysis would be something that is publicly visible to anyone (such as when your alias is online in a chatroom, or when a post is published to a website).

You can mitigate the techniques available to powerful adversaries by **prioritizing .onion links when available**, by **taking the possibility of targeted surveillance into account** and by **using an Internet connection that is not tied to your identity**.

### An Internet connection not tied to your identity

Using an Internet connection that is not tied to your identity means that if an attack on the Tor network succeeds, it still doesn't deanonymize you. You have two options: using Wi-Fi from a public space (like going to a cafe without CCTV cameras), or using a Wi-Fi antenna through a window from a private space. 

#### Working from a public space

If you only need to use the Internet irregularly, such as to submit a communique or do action research, you can **do [surveillance detection](https://notrace.how/threat-library/mitigations/surveillance-detection.html) and [anti-surveillance](https://notrace.how/threat-library/mitigations/anti-surveillance.html) before going to a coffee shop**, just like you would prior to a direct action. See ["How to submit an anonymous communiqué and get away with it"](https://www.notrace.how/resources/#how-submit) for more information on what submitting a communique involves.  

When using Wi-Fi in a public space, keep the following operational security considerations in mind: 

* Timing is a relevant consideration. If you want to submit a report-back the morning after a riot, or a communique shortly after an action (times when there may be a higher risk of targeted surveillance), consider waiting instead. In 2010, the morning after a bank arson in Canada, police surveilled a suspect as he traveled from his home to an Internet cafe, and watched him post the communique and then bury the laptop in the woods. More recently, investigators physically surveilling [an anarchist in France](https://notrace.how/resources/#ivan) installed a hidden camera to monitor access to an Internet cafe near the comrade's home and requested CCTV footage for the day an arson communique was sent.
* Do not get into a routine of using the same cafes repeatedly if you can avoid it. The more regularly you use a space, the more the Internet is tied to your identity. Additionally, if a surveillance effort knows your destination, anti-surveillance will not be effective.
* If you have to buy a coffee to get the Wi-Fi password, pay in cash! 
* Position yourself with your back against a wall so that no one can "shoulder surf" to see your screen, and ideally install a [privacy screen](/posts/tails/#privacy-screen) on your laptop. If you write a communique in an offline Tails session before your trip to the public space, you only need a few minutes locked in a public bathroom to send it in.
* If coffee shops without CCTV cameras are few and far between, you can try accessing a coffee shop's Wi-Fi from outside, out of view of the cameras. 
* Maintain situational awareness and be ready to pull out the Tails USB to shut down the computer at a moment's notice. It is very difficult to maintain adequate situational awareness while staying focused on your Tails session — consider asking a trusted friend to hang out who can dedicate themselves to keeping an eye on your surroundings. If the Tails USB is removed, Tails will shut down and [overwrite the RAM with random data](https://tails.net/doc/advanced_topics/cold_boot_attacks/index.en.html). Any LUKS USBs that were unlocked in the Tails session will now be encrypted again. Note that [Tails warns](https://tails.net/doc/first_steps/shutdown/index.en.html) "Only physically remove the USB stick in case of emergency as doing so can sometimes break the file system of the Persistent Storage." 
	* One person in charge of a darknet marketplace had his Tails computer seized while distracted by a fake fight next to him. Similar tactics have been used [in other police operations](https://dys2p.com/en/2023-05-luks-security.html#attacks). If his Tails USB had been attached to a belt with a short piece of fishing line, the police would most likely have lost all evidence when the Tails USB was pulled out. A more technical equivalent is [BusKill](https://www.buskill.in/tails/) — however, we only recommend buying this [in person](https://www.buskill.in/leipzig-proxystore/) or [3D printing it](https://www.buskill.in/3d-print-2023-08/). This is because any mail can be [intercepted](https://docs.buskill.in/buskill-app/en/stable/faq.html#q-what-about-interdiction) and altered, making the hardware [malicious](https://en.wikipedia.org/wiki/BadUSB). 

#### Working from a private space

If you need to regularly use the Internet for projects like moderating a website or hacking, going to a new Wi-Fi location after doing surveillance countermeasures might not be realistic on a daily basis. Additionally, a main police priority will be to seize the computer while it is unencrypted, and this is much easier for them to achieve in a public space, especially if you are alone. In this scenario, the ideal mitigation is to **use a Wi-Fi antenna positioned behind a window in a private space to access from a few hundred metres away** — a physical surveillance effort won't observe you entering a cafe or be able to easily seize your powered-on laptop, and a digital surveillance effort won't observe anything on your home Internet. To protect against [hidden cameras](https://www.notrace.how/earsandeyes), you should still be careful about where you position your screen. 

If a Wi-Fi antenna is too technical for you, you may even want to **use your home internet** for some projects that require frequent internet access. This contradicts the previous advice to not use an Internet connection that is tied to your identity. It's a trade-off: using Tor from home avoids creating a physical footprint that is so easy to observe, at the expense of creating a digital footprint which is more technical to observe, and may be harder to draw meaningful conclusions from. There are two main deanonymization risks to consider when using your home internet: that the adversary deanonymizes you through a Tor correlation attack, or that they deanonymize you by hacking your system (such as through [phishing](/posts/tails-best/#phishing-awareness)) which [enables them to bypass Tor](/posts/qubes/#when-to-use-tails-vs-qubes-os). To make both of these attacks more difficult, we recommend connecting to a VPN *before* connecting to Tor (i.e. [You → VPN → Tor → Internet](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorPlusVPN#you-vpnssh-tor)) when using Tails from home, which requires running the VPN from your networking device (either a router or a hardware firewall). For more information on the rationale, see [Privacy Guides](https://privacyguides.org/en/advanced/tor-overview/#safely-connecting-to-tor). 

#### To summarize

For sensitive and irregular Internet activities, use an Internet connection from a random cafe, preceeded by surveillance detection and anti-surveillance. For activities that require daily Internet access such that taking surveillance countermeasures and finding a new cafe isn't realistic, it's best to use a Wi-Fi antenna. If this is too technical for you, using your home Wi-Fi is an option, but this requires trusting Tor's resilience to correlation attacks, the measures you take against being hacked, and your VPN provider. 

# Reducing risks when using untrusted computers

![](/posts/tails-best/warning_compromisedpc.png)

> Tails can safely run on a computer that has a virus. But Tails cannot always protect you when:
>
> 1. Installing from an infected computer
> 2. Running Tails on a computer with a compromised BIOS, firmware, or hardware

## 1. Installing from an infected computer

You can mitigate this first issue by **using a computer you trust to install Tails**:

* According to our [recommendations](/recommendations/#your-computer), this would ideally be a [Qubes OS](/posts/qubes/) system, as it is much harder to infect than a normal Linux computer.  
* Use the "Terminal" installation method ["Debian or Ubuntu using the command line and GnuPG"](https://tails.net/install/expert/index.en.html), as it more thoroughly verifies the integrity of the download using [GPG](/glossary/#gnupg-openpgp). If using the [command line](/glossary/#command-line-interface-cli) is over your head, learn the basics of the command line with [Linux Essentials](/posts/linux/) and see the [Appendix below](/posts/tails-best/#appendix-gpg-explanation).
* Once installed, do not plug your Tails USB stick (or any [LUKS](/glossary/#luks) USBs used during Tails sessions) into any other computer; if the computer is infected, the infection can [spread to the USB](https://en.wikipedia.org/wiki/BadUSB). 

## 2. Running Tails on a computer with a compromised BIOS, firmware, or hardware

This second issue requires several mitigations. Let's start with a few definitions. 

* *Software* is the instructions for the computer, which are written in "code". 
* *Hardware* is the physical computer you are using. 
* *Firmware* is the low-level software that's embedded in a piece of hardware; you can simply think of it as the glue between the hardware and higher-level software of the operating system. It can be found in [several different components](https://www.kicksecure.com/wiki/Firmware_Security_and_Updates#Firmware_on_Personal_Computers) (hard drives, USB drives, graphics processor, etc.). 
* *BIOS* is the specific firmware that's embedded in the "motherboard" hardware and responsible for booting your computer when you press the power button. 

Our adversaries have two categories of attack vectors: [physical attacks](/glossary/#physical-attacks) (via physical access) and [remote attacks](/glossary/#remote-attacks) (via the remote access of the Internet). An adversary with physical access can compromise the software (e.g. by replacing the operating system with a malicious version), the hardware (e.g. by adding a keylogger), and the firmware (e.g. by replacing the BIOS with a malicious version). An adversary with remote access starts by hacking you (a software compromise) and can then proceed to compromise the firmware.

If an adversary has compromised the hardware or firmware of a laptop, this would also compromise a Tails session, given that the operating system would be running on a malicious foundation.

Not everyone will need to apply all of the advice below. For example, if you're only using Tails for anonymous web browsing and writen correspondence, some of this may be overkill. However, if you're using Tails to claim actions that are highly criminalized, a more thorough approach is likely relevant. 

### To mitigate against physical attacks:

* First, **get a fresh computer**. A laptop from a random refurbished computer store is unlikely [to already be compromised](https://arstechnica.com/tech-policy/2014/05/photos-of-an-nsa-upgrade-factory-show-cisco-router-getting-implant/). Buy your computer with cash so it cannot be traced back to you, and in person because mail can be intercepted — a used Thinkpad is a cheap and reliable option. It is best to use Tails with a dedicated laptop, which prevents the adversary from targeting the firmware through a less secure operating system or through your normal non-anonymous activities. Another reason to have a dedicated laptop is that if something in Tails breaks, any information that leaks and exposes the laptop won't automatically be tied to you and your daily computer activities.

<p>
<span class="is-hidden">
![](/posts/tails-best/X230.jpg)
</span>
<img src="/posts/tails-best/X230.jpg" class="no-dark">
</p>

* **Make the laptop's screws tamper-evident, store it in a tamper-evident manner, and monitor for break-ins**. With these precautions in place, you'll be able to detect any future physical attacks. See the guide ["Make Your Electronics Tamper-Evident"](/posts/tamper/) to adapt your laptop's screws, use some form of intrusion detection, and store your laptop properly. Store any external devices you’ll be using with the laptop in the same way (USB, external hard drive, mouse, keyboard). When physical attack vectors are mitigated, an adversary can only use remote attacks. 

### To mitigate against remote attacks:

* **Use Wi-Fi that is unrelated to your identity**. We recommend this not only to protect against deanonymization, but also to protect against hacking. It is best to never use the dedicated Tails laptop on your home Wi-Fi. This makes the laptop much less accessible to a remote attacker than a laptop that is regularly connected to your home Wi-Fi. An attacker targeting you needs a starting point, and your home Wi-Fi is a pretty good one. 
* **Remove the hard drive** — it's easier than it sounds. If you buy the laptop, you can ask the store to do it and potentially save some money. If you search on youtube for "remove hard drive" for your specific laptop model, there will probably be an instructional video. Make sure you remove the laptop battery and unplug the power cord first. We remove the hard drive to completely eliminate the hard drive firmware, which has been known to be [compromised by hackers](https://www.wired.com/2015/02/nsa-firmware-hacking/). A hard drive is part of the attack surface and it is unnecessary on a live system like Tails that runs from a USB. 
* Consider **removing the Bluetooth interface, camera, and microphone** while you're at it, although this is more involved — you'll need the user manual for your laptop model. The camera can at least be "disabled" by putting a sticker over it. The microphone is often connected to the motherboard via a plug — in this case just unplug it. If this is not obvious, or if there is no connector because the cable is soldered directly to the motherboard, or if the connector is needed for other purposes, cut the microphone cable with a pair of pliers. The same method can be used to permanently disable the camera. It is also possible to use Tails on a dedicated "offline" computer by removing the network card as well. Some laptops have switches on the case that can be used to disable the wireless interfaces, but for an "offline" computer it is preferable to actually remove the network card.  
* **Establish boot integrity by replacing the BIOS with [Heads](https://osresearch.net/)**. Security researchers [demonstrated an attack](https://www.youtube.com/watch?v=sNYsfUNegEA) on the BIOS firmware of a Tails user, allowing them to steal GPG keys and emails. Unfortunately, the BIOS cannot be removed like the hard drive. It is needed to turn on the laptop, so it must be replaced with [open-source](/glossary/#open-source) firmware. This is an advanced process because it requires opening the computer and using special tools. Most anarchists will not be able to do this themselves, but hopefully there is a trusted person in your networks who can set it up for you. The project is called Heads because it's the other side of Tails — where Tails secures software, Heads secures firmware. It has a similar purpose to the [Verified Boot](https://www.privacyguides.org/en/os/android-overview/#verified-boot) found in GrapheneOS, which establishes a full chain of trust from the hardware. Heads has [limited compatibility](https://osresearch.net/Prerequisites#supported-devices), so keep that in mind when buying your laptop if you plan to install it — we recommend the ThinkPad X230 because it's less involved to install than other models. The CPUs of this generation are capable of effectively removing the [Intel Management Engine](https://en.wikipedia.org/wiki/Intel_Management_Engine#Assertions_that_ME_is_a_backdoor) when flashing Heads, but this is not the case with later generations of CPUs on newer computers. Heads can be configured to verify the integrity and authenticity of a Tails USB — [see the documentation](https://osresearch.net/InstallingOS/#generic-os-installation), preventing it from booting if it has been tampered with. Heads protects against physical and remote classes of attacks on the BIOS firmware and the operating system software! If Heads ever detects tampering, you should immediately treat the device as untrusted. [Forensic analysis](https://www.notrace.how/threat-library/mitigations/computer-and-mobile-forensics.html) may be able to reveal how the compromise occured, which helps to prevent it from happening again. You can get in touch with a service like [Access Now’s Digital Security Helpline](https://accessnow.org/help), though we recommend not sending them any personal data.
* **Use USBs with secure firmware**, such as the [Kanguru FlashTrust](https://www.kanguru.com/products/kanguru-flashtrust-secure-firmware-usb-3-0-flash-drive), so that the USB will [stop working](https://www.kanguru.com/blogs/gurublog/15235873-prevent-badusb-usb-firmware-protection-from-kanguru) if the firmware is compromised. Kanguru has [retailers worldwide](https://www.kanguru.com/pages/where-to-buy), allowing you to buy them in person to avoid the risk of mail interception.

![](/posts/tails-best/flashtrust.webp)

* **Run Tails from a USB with a physical write-protect switch**. 

# Using A Write-Protect Switch

> What's a *write-protect* switch? When you insert a normal USB into a computer, the computer does *read* and *write* operations with it, and a *write* operation can change the data on the USB. Some special USBs developed for malware analysis have a physical switch that can lock the USB, so that data can be *read* from it, but no new data can be *written* to it.

If your Tails USB stick has a write-protect switch like the [Kanguru FlashTrust](https://www.kanguru.com/products/kanguru-flashtrust-secure-firmware-usb-3-0-flash-drive), when the switch is locked you are protected from an attacker compromising the Tails software stored on the USB. This is critical. To compromise your Tails USB stick, an attacker would need to be able to write to it. This means that even if a Tails session is infected with malware, your Tails USB is immutable, so the compromise cannot carry over to subsequent Tails sessions ("malware persistence") by modifying operating system files. The only other way to establish "malware persistence" is firmware compromise, which you have already mitigated.  

Note that Heads firmware makes a write-protect switch unnecessary because it can be configured to [verify the integrity and authenticity of your Tails USB](https://osresearch.net/InstallingOS/#generic-os-installation) before booting. 

If you aren't using Heads and you are unable to obtain a USB with a write-protect switch, you have three options.

1) Install Tails on a SD card, and use a USB 3.0 to SD card adapter, because SD cards have a write-protect switch. 
2) [Burn Tails to a new DVD-R/DVD+R](https://tails.net/install/dvd/index.en.html) (write once) for each new version of Tails — this is quite inconvenient. Don't use DVDs labeled "DVD+RW" or "DVD+RAM", which can be rewritten.
3) Boot Tails with the `toram` option, which loads Tails completely into memory. Eject the Tails USB at the beginning of your session before you do anything else (whether it is connecting to the Internet or plugging in another USB), and then use Tails like normal. How you use the `toram` option depends on whether your Tails USB boots with [SYSLINUX or GRUB](https://tails.net/doc/advanced_topics/boot_options/index.en.html).
	* For SYSLINUX, when the boot screen appears, press Tab, and type a space. Type `toram` and press Enter. 
	* For GRUB, when the boot screen appears, press `e` and use the keyboard arrows to move to the end of the line that starts with `linux`. The line is probably wrapped and displayed on multiple lines, but it is a single configuration line. Type `toram` and press F10 or Ctrl+X. 

## Unlocking the switch

On a USB with a write-protect switch, you will not be able to make any changes to the Tails USB when the switch is locked. If you can make changes, so can malware. There are only two cases where the switch must be unlocked:

### 1. For a dedicated upgrade session.

If you need to upgrade Tails, you can do so in a dedicated session with the switch unlocked — this is necessary because the upgrade needs to be written to the Tails USB. Once you are done, you should restart Tails with the switch locked.

### 2. For a dedicated configuration session, if you decide to use Persistent Storage.

[Persistent Storage](/posts/tails/#optional-create-and-configure-persistent-storage) is a Tails feature that allows data to carry over between sessions that would otherwise be amnesiac, by saving data onto the Tails USB itself. Because Persistent Storage requires writing to the Tails USB, it is generally impractical to use with a write-protect switch. An alternative to the write-protect switch is using Heads — Heads verifies the authenticity and integrity of the Tails USB through a digital signature upon boot, and this makes it safe to write to the Tails USB, so Persistent Storage will work as expected.  

Another reason to avoid using Persistent Storage features is that many of them store personal data to the Tails USB. If your Tails session is compromised, the data you access during that session can be used to tie your activities together. If there is personal data on the Tails USB, such as an email inbox, compartmentalization of Tails sessions is no longer possible *when Persistent Storage is unlocked*. To achieve compartmentalization with Persistent Storage unlocked, you would need a dedicated Tails USB for each identity, and updating them all every month would be a lot of work. 

However, you may want to use some Persistent Storage features that don't store personal data, such as the additional software feature. This requires unlocking the switch for a dedicated Persistent Storage configuration session: 

* Start an "unlocked" session, [create Persistent Storage](/posts/tails#optional-create-and-configure-persistent-storage) with additional software enabled, [install the additional software](/posts/tails#installing-additional-software), and select to "Install Every Time" when prompted. 
* Now that the configuration is complete, restart Tails into a "locked" session before actually using the software. Don't set an Administration password, which is only required during the initial installation. In a "locked" session, none of the files you work on are saved to the Tails USB because it is "locked", but now the additional software is configured to install every time you enter your Persistent Storage password at the Welcome Screen. To have a "locked" session with Persistent Storage, the USB switch will need to be switched to the read-only position *after* you receive the notification "Additional Software installed succesfully" (and before you connect to the Internet).  

The Persistent Storage feature is not possible with the DVD or `toram` boot option. 

## "Personal data" USBs 

Where can we store personal data for use between Tails sessions if the write-protect switch prevents us from using Persistent Storage? We recommend storing personal data on a second LUKS USB. This "personal data" USB should not look identical to your Tails USB to avoid confusion. To create this separate USB, see [How to create an encrypted USB](/posts/tails/#how-to-create-an-encrypted-usb). If you are reading this from a country like the UK where not providing encryption passwords can land you in jail, this second drive should be an HDD containing a [Veracrypt Hidden Volume](https://www.veracrypt.fr/en/Hidden%20Volume.html) (SSD and USB drives are [not suitable for Hidden Volumes](https://www.veracrypt.fr/en/Trim%20Operation.html)).

The compartmentalization approach [discussed above](/posts/tails-best/#2-using-tails-for-more-than-one-purpose-at-a-time) neatly separates different identities by using separate Tails sessions for separate activities — for example, in Tails session #1 you do website moderation activities, and in Tails session #2 you do action research activities. This approach has implications for how you organize your "personal data" USBs. If the files you save could be used to link your activities together, use a different "personal data" USB for each activity. 

![](/posts/tails-best/luks.png)

If a "personal data" USB is used to save very sensitive files (such as the text of a communique), it is best to overwrite and then destroy the USB once you no longer need the files (see [Really delete data from a USB drive](/posts/tails/#really-delete-data-from-a-usb)). This is another reason to use a separate USB for any files that need to be saved — you don't accumulate the forensic history of all your files on your Tails Persistent Storage, and you can easily destroy these "personal data" USBs as needed.

If you already use Tails and encrypted email, you may be familiar with Thunderbird's Persistent Storage feature for your inbox and PGP keys. This feature won't work with a write-protect switch enabled. Instead of using Persistent Storage for email, simply login to Thunderbird with IMAP in each new session. PGP keys can be stored on the "personal data" USB like any other file, and imported when needed with Thunderbird's "OpenPGP Key Manager" (File → Import Public Key(s) from File / Import Secret Key(s) from File). This approach has the advantage that if law enforcement manages to bypass LUKS, they still don't have your inbox without knowing your email password.

# Phishing Awareness 

Let's return to the subject of how an adversary would conduct a [remote attack](/glossary/#remote-attacks) targeting you or your project for hacking; the answer is most likely ["phishing"](/glossary/#phishing). *Phishing* is when an adversary crafts an email (or a message in an application) to trick you into revealing information or to introduce malware onto your machine. [*Spear phishing*](/glossary/#spear-phishing) is when the adversary has done some reconnaissance and uses information they already know about you to tailor their phishing attack. 

Phishing only works if the adversary has a way of sending you a message: you don't need to worry about this attack vector for activities like submitting a communique or doing action research, but it is relevant for public-facing projects that have a communication channel. Be aware that the "from" field in emails can be spoofed to fool you — [PGP signing](/posts/e2ee/#pgp-email) mitigates this to prove that the email is actually from who you expect it to be from.

You have probably heard the advice to be skeptical about clicking on links and opening file attachments — this is why. Phishing relies on your actions to succeed, so your awareness is your best defense. 

A malicious file or link works by [executing code](https://en.wikipedia.org/wiki/Arbitrary_code_execution) on your machine. For malicious files, the code executes when the file is opened. For malicious links, the code executes when you visit the website, usually with the help of JavaScript. The point of this code execution is to give an entry point ("inital access") to infect your machine with malware. 

Tails protects against malware deanonymizing you by forcing all internet connections through the Tor network. However, once the adversary has "initial access" they will try to further their attack; 

* [to make the infection persistent](https://attack.mitre.org/tactics/TA0003/), 
* [to install a screen or key logger](https://attack.mitre.org/tactics/TA0009/), 
* [to exfiltrate your data](https://attack.mitre.org/tactics/TA0010/), 
* [to achieve "privilege escalation"](https://en.wikipedia.org/wiki/Privilege_escalation)  

Privilege escalation (i.e. going from an unprivileged user to the administration user on the system) is usually necessary to bypass Tor. Tails does not have a default Administration password (it must be set on the session's Welcome Screen if needed) in order to make "privilege escalation" more difficult. 

The most recent [Tails audit](https://tails.net/news/audit_by_ROS/index.en.html) found several "privilege escalation vulnerabilities," and even a vulnerability that leaked the IP address from the non-privileged user. If resilience to malware attacks is an important part of your threat model, see [When to Use Tails vs. Qubes OS](/posts/qubes#when-to-use-tails-vs-qubes-os).

## Files

In 2017, the FBI and Facebook worked together to develop a malicious video file [that deanonymized a Tails user](https://www.vice.com/en/article/v7gd9b/facebook-helped-fbi-hack-child-predator-buster-hernandez) after he opened it while using his home Wi-Fi.

For untrusted attachments, you would ideally use [Dangerzone](https://dangerzone.rocks/) to **sanitize all files sent to you before opening them**. Dangerzone takes untrusted PDFs, office documents, or images and turns them into trusted PDFs. See the [documentation](https://tails.net/doc/persistent_storage/additional_software/dangerzone/index.en.html) for how to install Dangerzone on Tails — unfortunately, it currently requires using the [command line](/glossary/#command-line-interface-cli). 

If you are not using Dangerzone, **it is best to open untrusted files in a dedicated ['offline mode'](https://tails.net/doc/first_steps/welcome_screen/index.en.html#index3h2) Tails session**. This will prevent code execution from establishing a remote connection to the adversary, which is usually needed to further the attack. Shutting the session down immediately afterward will minimize the chance of malware persisting. However, unless you use Dangerzone to sanitize the files, they will remain untrusted. 

## Links 

With untrusted links, there are two things you must protect: your anonymity and your information. 

* **It is best to open untrusted links in a dedicated Tails session without unlocked Persistent Storage or attached "personal data" USBs.** You can put the link on a Riseup Pad to access it. 
* **[Use Tor Browser on the Safest security setting](/posts/tails/#tor-browser-security-settings)**! The vast majority of exploits against Tor Browser will not work with the Safest setting. 
* **Manually copy and paste the address into your browser, and retype the domain**. For example, after pasting the link `anarsec.guide/posts/tails`, retype `anarsec.guide` yourself. Do not click through a hyperlink (i.e. always copy and paste) because it can be used to mislead you about where you are going. Retyping the domain protects against "typo-squatting" (mailriseup.net instead of mail.riseup.net) as well as ["homograph attacks"](https://www.theguardian.com/technology/2017/apr/19/phishing-url-trick-hackers) (where Cyrillic letters are substituted for normal letters).
* **Never follow a shortened link** (e.g. a site like bit.ly that takes long web addresses and makes a short one) because it cannot be verified before redirection. [Unshorten.me](https://unshorten.me/) can reveal shortened links. 
* **If you don't recognize the domain, research it**. Search for the domain with the domain name in quotation marks using a privacy-preserving search engine (such as DuckDuckGo) to see if it’s a legitimate website. This isn’t a surefire solution, but it’s a good precaution to take.

![](/posts/tails-best/duckduck.cleaned.png)

* **Don't enter any identifying information into the website**. If you follow a link from an email and are asked to log in, be aware that this is a common endgame for phishing campaigns. Instead, manually go to the website of the service you are trying to access and sign in there. That way, you’ll know you’re logging in to the right website because you’ve typed in the address yourself, rather than having to trust the link in the email. 

## Watering hole attacks 

An adversary can also compromise a "trusted" website — this allows them to install malware on the computers of anyone who visits the website, without needing to engage in phishing. This is called a ["watering hole attack" or a "drive-by compromise"](https://attack.mitre.org/techniques/T1189/) because it attacks many people simultaneously. For example, the [FBI hacked a website then used a Tor Browser exploit](https://www.vice.com/en/article/53d4n8/fbi-hacked-over-8000-computers-in-120-countries-based-on-one-warrant) to hack 8,000 users who visited it. 

This is why its important to **[use Tor Browser on the Safest security setting](/posts/tails/#tor-browser-security-settings)** by default, even for "trusted" websites, to greatly reduce the risk of a successful malware attack on Tor Browser. 

# Encryption  

## Passwords

[Encryption](/glossary/#encryption) is the only thing standing in the way of our adversaries reading all our data, if it's used well. The first step in securing your encryption is to make sure that you use very strong passwords — most passwords don't need to be memorized because they are stored in a password manager called KeePassXC, so they can be completely random. Never reuse a password for multiple things ("password recycling") — KeePassXC makes it easy to store unique passwords that are dedicated to one purpose. To learn how to use KeePassXC, see [Password Manager](/posts/tails/#password-manager-keepassxc). 

>In the terminology used by KeePassXC, a [*password*](/glossary/#password) is a random sequence of characters (letters, numbers and other symbols), while a [*passphrase*](/glossary/#passphrase) is a random sequence of words. 

[LUKS](/glossary/#luks) encryption **is only effective when the device is powered off** — when the device is powered on, the password can be retrieved from memory. Adversaries can attempt to [brute-force attack](/glossary/#brute-force-attack) encryption with [massive amounts of cloud computing](https://blog.elcomsoft.com/2020/08/breaking-luks-encryption/). The newer version of LUKS (LUKS2 using Argon2id) is [less vulnerable to brute-force attacks](https://mjg59.dreamwidth.org/66429.html) — this is the default as of Tails 6.0 and Qubes OS 4.1. If you'd like to learn more about this change, we recommend [Systemli's overview](https://www.systemli.org/en/2023/04/30/is-linux-hard-disk-encryption-hacked/) or [dys2p's](https://dys2p.com/en/2023-05-luks-security.html). 

Password strength is measured in "[bits of entropy](https://en.wikipedia.org/wiki/Password_strength#Entropy_as_a_measure_of_password_strength)". Your passwords/passphrases should ideally have an entropy of about 128 bits (diceware passphrases of **ten words**, or passwords of **21 random characters**, including uppercase, lowercase, numbers, and symbols) and shouldn't have less than 90 bits of entropy (diceware passphrases of seven words).

![](/posts/tails-best/passphrase.png)

>What is a diceware passphrase? As [Privacy Guides notes](https://www.privacyguides.org/en/basics/passwords-overview/#diceware-passphrases), "Diceware passphrases are a great option when you need to memorize or manually input your credentials, such as for your password manager's master password or your device's encryption password. An example of a diceware passphrase is `viewable fastness reluctant squishy seventeen shown pencil`." The Password Generator feature in KeePassXC can generate diceware passphrases and random passwords. If you prefer to generate diceware passphrases using real dice, see [Privacy Guides](https://www.privacyguides.org/en/basics/passwords-overview/#diceware-passphrases).

### General recommendations

* Memorize diceware passphrases of 7-10 words for everything that you'll need to enter before you have access to an unlocked KeePassXC database (in other words, your Full Disk Encryption passphrase and the KeePassXC master passphrase).
* Generate passwords of 21 random characters for everything that can be stored in a KeePassXC database. Maintain an off-site backup of your KeePassXC database(s) in case it is ever corrupted or seized. 

> **Tip** 
>
> Your memorized diceware passphrases can be easy to forget if you have several to keep track of, especially if you use any of them infrequently. To reduce the risk of forgetting a diceware passphrase permanently, you can use Tails to store all "memorized" passphrases on a LUKS USB then store it off-site where it won't be recovered during a police raid. You should be able to reconstruct the LUKS passphrase of this USB if a lot of time has passed. See the [Threat Library](https://www.notrace.how/threat-library/mitigations/digital-best-practices.html#header-use-strong-passwords) for two different approaches you can take: one relies on a trusted comrade, and the other is self-sufficient. As with all important backups, you should have at least two. 

### Tails passphrases

For Tails, you need to memorize two passphrases:

1) The [LUKS](/glossary/#luks) "personal data" USB passphrase, where your KeePassXC file is stored.
2) The KeePassXC passphrase

If you are using Persistent Storage, this is another passphrase that you will have to enter on the Welcome Screen at boot time, but it can be the same as the LUKS password. Shutdown Tails whenever you are away from the computer for more than a few minutes.

## Encrypted volumes 

[LUKS](/glossary/#luks) is great, but defense-in-depth can't hurt. If the police seize your USB in a house raid, they will try a [variety of tactics to bypass the authentication](https://notrace.how/threat-library/techniques/targeted-digital-surveillance/authentication-bypass.html), so a second layer of defense with a different encryption implementation can be useful for highly sensitive data. 

### Installing SiriKali 

SiriKali is an encrypted volume program that uses [gocryptfs](https://nuetzlich.net/gocryptfs/) behind the scenes. It is [available in the Debian repository](https://packages.debian.org/bookworm/sirikali) and can be easily installed as [additional software](/posts/tails#installing-additional-software). In Synaptic, install both sirikali and gocryptfs (if you are comfortable on the [command line](/glossary/#command-line-interface-cli), you can use gocryptfs directly and you don't actually need sirikali). If you don't want to reinstall SiriKali every session, you will need to [configure Additional Software in Persistent Storage](/posts/tails-best/#unlocking-the-switch).  

### Creating an encrypted volume 

Using SiriKali to create a volume will make two new directories: a "cipher" directory where the encrypted files are actually stored (`VolumeName/` on your "personal data" USB), and a "plain" directory where you access your decrypted volume once it is mounted there (`/home/amnesia/.SiriKali/VolumeName`).  

* Plug in the "personal data" USB where you will store this encrypted volume and enter its LUKS passphrase. 
* Then in SiriKali, press "Create Volume" and select the option "gocryptfs."  
	* You will be prompted for a password. Create a new entry in your KeepassXC file and generate a password using the Generate Password feature (the dice icon). 
	* For the "Volume Path" option, select the "personal data" USB that you just unlocked. 

### Accessing your encrypted volume

Whenever you want to decrypt the volume, click "Mount Volume":

* This happens automatically upon volume creation. 
* You can now add files to your mounted volume: right-click the volume and select "Open Folder". 
	* You can verify SiriKali is working by creating a test file here. This file will show up encrypted in the cipher directory.
* When you are done, right-click the volume and select "Unmount."

Before storing important files in the volume, you should run a test to make sure it works as expected, especially if its your first time using it.  

## Encrypted Communication 

PGP email is the most established form of encrypted communication on Tails in the anarchist space. Unfortunately, PGP does not have [forward secrecy](/glossary/#forward-secrecy) — that is, a single secret (your private key) can decrypt all messages, rather than just a single message, which is the standard in encrypted messaging today. It is the opposite of "metadata protecting", and has [several other shortcomings](/posts/e2ee/#pgp-email). 

For [synchronous](/glossary/#synchronous-communication) and [asynchronous](/glossary/#asynchronous-communication) messaging we recommend [Cwtch](/posts/e2ee/#cwtch), unless its for an anonymous public-facing project, in which case we still recommend PGP. For more information, see [Encrypted Messaging For Anarchists](/posts/e2ee/). 

# To Conclude

Using Tails without any of this advice is still a vast improvement over many other options. Given that anarchists regularly entrust their freedom to Tails, taking these extra precautions can further strengthen your trust in this operating system. 

# Appendix: GPG Explanation

Most Linux users will rarely need to use the [command line interface](/posts/linux/#the-command-line-interface). If you're using Tails, you shouldn't need it at all, although you will need the following commands for a [more secure installation](https://tails.net/install/expert/index.en.html):

* `wget`: this downloads files from the Internet using the Command Line (rather than a web browser)
* `gpg`: this handles [GPG encryption](/glossary/#gnupg-openpgp) operations. This is used to verify the integrity and authenticity of the Tails download. 
* `apt`: this manages packages in Debian. 
* `dd`: this copies a file from one disk to another. 

Using `gpg` during the installation of Tails will be less confusing if you understand how it works. 

First, some clarification. [PGP and GPG](/glossary/#gnupg-openpgp) are terms that can be used interchangeably; PGP (Pretty Good Privacy) is the encryption standard, and GPG (GNU Privacy Guard) is a program that implements it. PGP/GPG is also used for [encrypted email communication](/posts/e2ee/#pgp-email)), but we use it here only to verify the integrity and authenticity of files.  

GPG is a classic example of [public-key cryptography](/glossary/#public-key-cryptography). GPG provides cryptographic functions for [encrypting](/glossary/#encryption), decrypting, and signing files; our concern here is digitally signing files. The Tails team [digitally signs](/glossary/#digital-signatures) their .img releases. GPG gives us a way to verify that the file has actually been "signed" by the developers, which allows us to trust that it hasn't been tampered with.   

Now you need to understand the basics of public-key cryptography. [This Computerphile video](https://www.youtube.com/watch?v=GSIDS_lvRv4) has a great overview with visual aids. To summarize, a **secret/private** key is used to **sign** messages, and only the user who has that key can do so. Each **private** key has a corresponding **public** key — this is called a **key pair**. The public key is shared with everyone and is used to verify the signature. Confused? Watch the video! 

![](/posts/tails-best/signature.png)

Tails signs their releases, and only they can do this because only they have their private key. However, I can verify that this signature is valid by having a copy of their public key. Now I'll explain the `gpg` commands in the [Tails verification instructions](https://tails.net/install/expert/index.en.html). 

## Step: Generate a Key-Pair

Tails recommends this [Riseup guide](https://riseup.net/en/security/message-security/openpgp/gpg-keys#using-the-linux-command-line) to generate your own key-pair. 

* `gpg --gen-key` will prompt you for some configuration options and then generate your key-pair.

## Step: Verify the Tails public key

* `gpg --import < tails-signing.key` imports the Tails public key into your keyring so that it can be used. 
* `gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export chris@chris-lamb.co.uk | gpg --import` imports the public key of a Debian developer into your keyring so that it can be used. 
* `gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F` allows you to verify the Tails public key with the Debian developer's public key by examining the output as instructed. This is so that if the source of the Tails public key (tails.net) is compromised, you have an external source of truth to alert you. 
* `gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F` will certify the Tails public key with the key you created in the last step. 

Now we know that we have a genuine version of the Tails public key.  `gpg` also knows this because we chose to certify it. 

## Step: Verify the downloaded Tails .img file

* `TZ=UTC gpg --no-options --keyid-format long --verify tails-amd64-6.1.img.sig tails-amd64-6.1.img` allows you to verify that the .img file is signed as it should be by examining the output as instructed. Version numbers in the command will change.  

Now that we know that we have a genuine version of the Tails .img file, we can proceed to install it on a USB. 

