+++
title="Home Network Security"
date=2024-04-10
draft=true

[taxonomies]
categories = ["Defensive"]
tags = ["linux", "router", "beginner"]

[extra]
blogimage="/images/bestpractices-dither.png"
toc=true
dateedit=2024-04-10
a4="router-a4-en.pdf"
letter="router-letter-en.pdf"
+++

The security of your home network matters because a malicious network device can be [used to attack any devices that connect to it](https://hak5.org/products/wifi-pineapple) and [State-sponsored malware campaigns are known to compromise network devices](https://arstechnica.com/information-technology/2022/06/a-wide-range-of-routers-are-under-attack-by-new-unusually-sophisticated-malware/). 

A "home network" is the network that connects your devices to the Internet and each other, and this home network is created by "networking devices". You have probably already set up a router — routers specialize in receiving network traffic from your laptop or phone via an "Access Point" and "routing" it onwards to the Internet. Perhaps you've even already encountered a hardware firewall — this networking device specializes in enforcing "firewall rules" and compartmentalizing your home network. That said, the functions of routers and hardware firewalls somewhat overlap. For instance, routers usually also have some basic firewall capabilities. 

Another way of thinking about your home network is that its the *"intranet"* sealed off by your network devices into a *private* network, in contrast to the *Internet* which is a *public* network accessible to anyone. 

We recommend setting up your home network with a hardware firewall that runs the [OPNsense](https://www.privacyguides.org/en/router/#opnsense) operating system, paired with a router that runs the [OpenWrt](https://www.privacyguides.org/en/router/#openwrt) operating system. Although you can get by with just a router, a hardware firewall will enable a more secure set up. In this configuration, the OPNsense firewall does all of the heavy lifting, and the OpenWrt router is limited to the role of a "Wi-Fi Access Point" for your devices to connect to. 

# VPN Kill Switch

Your networking devices should be configured to force all network traffic through a reputable [VPN](/glossary/#vpn-virtual-private-network) — this puts your trust in your VPN instead of an inherently untrustworthy Internet Service Provider. As the [Security Lab](https://securitylab.amnesty.org/latest/2023/10/technical-deep-dive-into-intellexa-alliance-surveillance-products/) notes:

>Using a reputable VPN provider can provide more privacy against surveillance from your ISP or government and prevent network injection attacks from those entities. A VPN will also make traffic correlation attacks — especially those targeting messaging apps — more difficult to perform and less effective.

There are two ways you can run a VPN: from your laptop or phone with a Client app, or from your networking device. When using your laptop or phone from home, we recommend the latter because:

* A "VPN Kill Switch", which blocks non-VPN traffic, is [more effective](https://docs.gl-inet.com/router/en/4/interface_guide/vpn_dashboard/#global-options-of-vpn-client) when the VPN runs from a networking device.
* An adversary that compromises your phone or laptop will need to also compromise the networking device in order to learn your public IP address — your phone or laptop will only know your VPN IP address.
* If all of the network traffic coming from your home runs through the same VPN server, this makes it more challenging for an adversary to perform traffic analysis. 
* VPN speeds will typically be faster because cryptographic operations are performed on dedicated hardware. 
 
## VPNs and Tor

We recommend connecting to a VPN *before* connecting to Tor (i.e. [You → VPN → Tor → Internet](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorPlusVPN#you-vpnssh-tor)) when you are using Tor from your home Internet connection. For more information on the rationale, see [Privacy Guides](https://privacyguides.org/en/advanced/tor-overview/#safely-connecting-to-tor). 

This works perfectly when the VPN runs from your networking device — for instance, when you use Tor Browser on your laptop you will connect to a VPN before connecting to Tor, even though there is no VPN running on your laptop. In fact, this is the only way to connect to a VPN before Tor when using the Tails operating system.

# Configure the Hardware Firewall

This guide will assume that you'll set up a hardware firewall in tandem with a router, but you can skip ahead to the [appendix below](/posts/router/#appendix-configure-a-router-without-a-hardware-firewall) for the less secure option of using only a router. 

The OPNsense operating system can be installed on a [variety of hardware](https://docs.opnsense.org/manual/hardware.html). [Protectli](https://protectli.com) sells firewall hardware that is easy to get started with. The '4 Port' models are sufficient for an apartment sized residence.

This guide will assume that you're using Protectli hardware (but it can be adapted to any OPNsense firewall), and that you're using Tails to set it up (but you could also set it up from Qubes OS). 

TODO: discuss VLANs

# Configure the Router 

The OpenWrt operating system can be installed on a variety of hardware — see their [compatability list](https://openwrt.org/toh/start). [GL-iNet](https://www.gl-inet.com/) sells affordable OpenWrt routers that are easy to get started with. The 'Travel' models are sufficient for an apartment sized residence. 

This guide will assume that you're using a GL-iNet router (but it can be adapted to any OpenWrt router), and that you're using Tails to set it up (but you could also set it up from Qubes OS). 

# Generate Tor Traffic Continuously 

Generating extra Tor traffic continuously from your home Internet connection makes it more challenging for an adversary to perform traffic analysis. For example, if the only active device on your home network is a Tails session, an adversary will see VPN traffic, but its timing will still have some correlation with the Tor traffic of your activity. In contast, if Tor "cover" traffic is being generated that is unrelated to your activity, an adversary can no longer easily distinguish the traffic that matters for a [targeted correlation attack](/posts/tails-best/#non-targeted-and-targeted-correlation-attacks). 

There are many ways to generate the Tor cover traffic. We recommend the AnarSec fork of Noisy, a simple Python script which provides randomized and continuous Tor traffic. For usage instructions, [see here](https://0xacab.org/anarsec/noisy).

There are two approaches you can take with Noisy: 

1) Simply generate the cover traffic from the same machine you are using for your Tor activity (Qubes OS or Tails).
2) Use a dedicated machine to generate the cover traffic, that can be kept running while you are out of the house. 

Which approach you take depends on what type of activity you are doing from your home Internet connection. 

An adversary being able to see when you are *not* using the Internet is potentially a valuable metric. For example, let's say that you are the moderator for a website that has activity on a daily basis — in a given year, only 28 days had no activity. If these periods of inactivity happen to correlate with when there was also no activity on your home Internet connection, that's not good. If this type of traffic analysis would be relevant to the projects you do from your home Internet connection, choose the second approach.

## Noisy from a dedicated machine

You can run Noisy from a Raspberry Pi or mini-PC that stays permanently powered on, although it requires some Linux knowledge to set up. We recommend that you limit the traffic to the daytime — if it was 24/7, it could look like you're running an Onion Service. 

# Appendix: Configure a Router without a Hardware Firewall

 
## First time setup

Start a Tails session, then follow the documentation for [first time setup](https://docs.gl-inet.com/router/en/4/faq/first_time_setup/):

* Use the Unsafe Browser to access the web Admin Panel (Tor Browser blocks connections to the local network). 
	* From Qubes OS, use any web browser other than Tor Browser.
* When prompted to create a new password for the web Admin Panel, use [KeePassXC](/posts/tails/#password-manager-keepassxc) to set a [strong password](/posts/tails-best/#passwords).
* Connect the router to the Internet [via an ethernet cable](https://docs.gl-inet.com/router/en/4/interface_guide/internet_ethernet/) from the WAN port of the GL-iNet router (Wide Area Network, i.e. the Internet) to the modem. You'll need to remove the router that you were previously using. 
* [Enable the 5GHz Wi-Fi](https://docs.gl-inet.com/router/en/4/interface_guide/wireless/), change the SSID to something that doesn't leak router information (for our purposes, "Geologic-5G"), and set a strong Wi-Fi password. 5GHz is faster than 2.4GHz and it travels a shorter distance, so it will be harder to monitor from a van parked outside of your house. 
* [Enable the 5GHz Guest Wi-Fi](https://docs.gl-inet.com/router/en/4/interface_guide/wireless/), change the SSID to something that doesn't leak router information (for our purposes, "Symphony-5G"), and set a strong Wi-Fi password.  

## Obtain VPN configuration files

For your VPN provider, we recommend either [Mullvad](https://www.privacyguides.org/en/vpn/#mullvad) or [IVPN](https://www.privacyguides.org/en/vpn/#ivpn). A VPN subscription should be purchased anonymously — vouchers are available from [Mullvad](https://mullvad.net/en/blog/2022/9/16/mullvads-physical-voucher-cards-are-now-available-in-11-countries-on-amazon/) and [IVPN](https://www.ivpn.net/knowledgebase/billing/voucher-cards-faq/) to purchase the subscription anonymously without [Monero](https://www.privacyguides.org/en/cryptocurrency/#monero). 

* From Tails, connect to the Wi-Fi you just set up, then use the Tor Browser to get the configuration files from [IVPN](https://docs.gl-inet.com/router/en/4/interface_guide/wireguard_client/#ivpn) or [Mullvad](https://docs.gl-inet.com/router/en/4/interface_guide/wireguard_client/#mullvad).

## Set up the VPN on the router

* Use the Unsafe Browser to login to the web Admin Panel of your router. Navigate to the VPN Dashboard. 
	* Under VPN Client, click **Set Up Now** beside WireGuard, then follow [the guide](https://docs.gl-inet.com/router/en/4/interface_guide/wireguard_client/#setup-wireguard-client). 
	* In [VPN Client Options](https://docs.gl-inet.com/router/en/4/interface_guide/vpn_dashboard/#vpn-client-options) enable **IP Masquerading**.  
	* Keep the [proxy mode](https://docs.gl-inet.com/router/en/4/interface_guide/vpn_dashboard/#proxy-mode) set to **Global Proxy**. This means that all network traffic is forced through the VPN. 
	* Click [Global Options](https://docs.gl-inet.com/router/en/4/interface_guide/vpn_dashboard/#global-options-of-vpn-client) and enable **Block Non-VPN Traffic**.  
* Test that the VPN is configured properly with the [Mullvad connection check](https://mullvad.net/en/check) or [IVPN status](https://www.ivpn.net/knowledgebase/privacy/how-do-i-know-when-iandsharp039m-protected-by-ivpn/) using the Unsafe Browser.
	* If you will be connecting to the router via an ethernet cable to the LAN port (Local Area Network, i.e. your home network), also test that.  
* Verify that the router [firmware is set to automatically update](https://docs.gl-inet.com/router/en/4/interface_guide/firmware_upgrade/).

## Using the router 

* Connect to the router via Wi-Fi with "Geologic-5G" or via ethernet to a LAN port. The network traffic from your laptop or phone now connects to a VPN, even though there is no VPN running on your laptop or phone.  
	* In the case of Tails or Qubes-Whonix, this means that you now connect to the VPN *before* connecting to Tor, which is what we want. 
* It's unnecessary to "double up" a VPN — if its running on your networking device, it doesn't need to be running on your phone or laptop, and vice-versa. This means that a phone or laptop running a VPN should disable it before connecting to Wi-Fi configured with a "VPN Kill Switch". 
* Give visitors access to the Guest Wi-Fi so that their devices are on a compartmentalized part of the home network (a VLAN, or "virtual LAN"). Their traffic will still be routed through the VPN. 
