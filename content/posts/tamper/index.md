+++
title="Make Your Electronics Tamper-Evident"
date=2024-04-18

[taxonomies]
categories = ["Defensive"]
tags = ["opsec", "beginner"]

[extra]
blogimage="/images/beads-pink.jpg"
toc=true
dateedit=2024-04-18
a4="tamper-a4-en.pdf"
letter="tamper-letter-en.pdf"
+++

If the police ever have [physical access](/glossary/#physical-attacks) to an electronic device like a laptop, even [for five minutes](https://www.vice.com/en/article/a3q374/hacker-bios-firmware-backdoor-evil-maid-attack-laptop-5-minutes), they can install hardware keyloggers, create images of the storage media, or otherwise trivially compromise it at the hardware, firmware, or software level. One way to minimize this risk is to make devices tamper-evident. As the Threat Library [notes](https://notrace.how/threat-library/mitigations/tamper-evident-preparation.html), "Tamper-evident preparation is the process of taking precautionary measures to make it possible to detect when something has been physically accessed by an adversary."

<!-- more -->
['Evil maid' attacks](https://en.wikipedia.org/wiki/Evil_maid_attack) work like this: an attacker gains temporary access to your [encrypted](/glossary/#encryption) laptop or phone. Although they can’t decrypt your data, they can tamper with your laptop for a few minutes and then leave it exactly where they found it. When you return and enter your credentials, you have been hacked. The attacker may have [modified data on your hard drive](https://media.ccc.de/v/gpn20-32-poc-implementing-evil-maid-attack-on-encrypted-boot), replaced the firmware, or installed a hardware component such as a keylogger.  

"Defense in depth" means that there are multiple layers of security that an adversary must bypass in order to succeed. This article will cover tamper-evident laptop screws, storage and firmware, as well as physical intrusion detection. 

# Tamper-Evident Laptop Screws 

Let's start with your laptop. For a seal to effectively alert you to intruders, it must be impossible to remove and replace without leaving a trace, and it must also be unique—otherwise, the adversary could simply replicate the seal and you’d never know they’d been there. Glitter nail polish creates a unique pattern that is impossible to replicate, and if you take a photo of this pattern, you can use it to verify that the nail polish has not been removed and reapplied in your absence, such as during a [covert house search](https://notrace.how/threat-library/techniques/covert-house-search.html). The presentation "[Thwarting Evil Maid Attacks](https://media.ccc.de/v/30C3_-_5600_-_en_-_saal_1_-_201312301245_-_thwarting_evil_maid_attacks_-_eric_michaud_-_ryan_lackey)" introduced this technique in 2013. 

Mullvad VPN [created a guide](https://mullvad.net/en/help/how-tamper-protect-laptop/) for applying this technique: first apply stickers over the laptop case screws, then apply the nail polish. An [independent test](https://dys2p.com/en/2021-12-tamper-evident-protection.html#glitzer-nagellack-mit-aufklebern) noted:

> Attackers without a lot of practice can use a needle or scalpel, for example, to drive under the sticker and push it partially upward to get to the screws relatively easily. The broken areas in the paint could be repaired with clear nail polish, although we did not need to do this in most of our tests. The picture below is a pre-post-comparison of one of our first attempts. Except for 3-4 glitter elements at the top left edge of the sticker, all others are still in the same place. This could be further reduced in subsequent attempts, so we rate this method as only partially suitable. [...] The relevant factor in this process is the amount of elements on the edge of the sticker. In addition, there are special seal stickers available which break when peeled off. They are probably more suitable for this method.

<p>
<span class="is-hidden">
![](/posts/tamper/mullvad.png)
</span>
<img src="/posts/tamper/mullvad.png" class="no-dark">
</p>

For this reason, it is preferable to apply nail polish directly to the screws rather than over a sticker. This direct application is done for [NitroKey](https://docs.nitrokey.com/nitropad/qubes/sealed-hardware) and [Purism](https://puri.sm/posts/anti-interdiction-update-six-month-retrospective/) laptops. Keep these nuances in mind:

> The screws holes are particularly relevant here. If they are too deep, it is difficult to take a suitable photo of the seal under normal conditions. If the hole is shallow or if it is completely filled with nail polish, there is a risk that if a lot of polish is used, the top layer can be cut off and reapplied after manipulation with clear polish. If the nail polish contains too few elements, they could be manually arranged back to the original location after manipulation if necessary.

<p>
<span class="is-hidden">
![](/posts/tamper/X230.jpg)
</span>
<img src="/posts/tamper/X230.jpg" class="no-dark">
</p>

Glitter nail polish was successfully bypassed during a Tamper Evident Challenge in 2018 — the winner [explained](https://hoodiepony.medium.com/bypassing-the-glitter-nail-polish-tamper-evident-seal-25d6973d617d) how they managed to do it. Notably, a brand of nail polish with relatively large pieces of glitter in only two colors was used. It would be difficult to apply this bypass to inset screw holes; if the glitter was applied with a high density of elements, but not too thick, this would also increase the difficulty. Finally, [using an adhesive](https://dys2p.com/en/2021-12-tamper-evident-protection.html#glitzer-nagellack-mit-klebstoff) would also make the bypass less feasible. 

Verification that the random pattern hasn't changed can be done manually with what astronomers call a "blink comparison". This is used in astronomy to detect small changes in the night sky: you quickly flick between the original photo and the current one, which makes it easier to see any changes. Alternatively, if you have an Android smartphone (either [GrapheneOS](/posts/grapheneos/) or a cheap one for [intrusion detection](/posts/tamper/#physical-intrusion-detection)), you can use an app called [Blink Comparison](https://github.com/proninyaroslav/blink-comparison), which makes it less likely that you will miss something. It can be installed like any other [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software), i.e. not through F-Droid. 

The Blink Comparison app encrypts its storage to prevent an adversary from easily replacing the photos, and provides a helpful interface for comparing them. The app helps you take the comparison photo from the same angle and distance as the original photo. Blink Comparison then switches between the two images when you touch the screen, making direct comparison much easier than manually comparing two photos. 

## In practice 

Now that you understand the nuances of applying nail polish to the screws of your laptop case, let's actually do it — if you are going to [install Heads firmware](/posts/tamper/#tamper-evident-software-and-firmware), do that first so the nail polish doesn't have to be removed and repeated. Before you start, you can also take a picture of the inside of the laptop in case you ever need to check if the internal components have been tampered with despite the nail polish protection (keep in mind that not all components are visible). Use a nail polish that has different colors and sizes of glitter, like the one shown above. 

* First, take a photo of the bottom of the computer and use a program like GIMP to number the screws to make it easier to verify. For example, the ThinkPad X230 shown above has 13 screws that need to be numbered so that in the future you know which screw the photo `3.jpg` refers to. 
* Apply the glitter nail polish directly to each screw, making sure there are enough glitter elements without it being too thick. 
* Once it is dry, take good close-up photos of each screw — either with the Blink Comparison app on a smartphone or with a regular camera. It is a good idea to use lighting that is reproducible, so close the blinds on any windows and rely on the indoor lighting and the camera flash. Number the file names of the photos and back them up to a second storage location. 

If you ever need to remove the nail polish to access the inside of the laptop, you can use a syringe to apply the nail polish remover to avoid applying too much and damaging the internal electronics. 

# Tamper-Evident Storage 

You also need a tamper-evident storage solution for all sensitive electronics when you are away from home (laptops, external drives, USBs, phones, external keyboards and mice) — a laptop can be tampered with in ways that don't require removing the screws. Safes are often used to protect valuable items, but they can be bypassed in many ways, and some of these bypasses are difficult to detect (see [below](/posts/tamper/#appendix-cracking-safes)). It is not trivial or inexpensive to make a safe tamper-evident, if it can be done at all. 

<p>
<span class="is-hidden">
![](/posts/tamper/lentils.jpg) 
</span>
<img src="/posts/tamper/lentils.jpg" class="no-dark">
</p>

A better and cheaper solution is to implement [dys2p's guide](https://dys2p.com/en/2021-12-tamper-evident-protection.html#kurzzeitige-lagerung):

>  When we need to leave a place and leave items or equipment behind, we can store them in a box that is transparent from all sides. Then we fill the box with our colorful mixture so that our devices are covered. The box should be stored in such a way that shocks or other factors do not change the mosaic. For example, the box can be positioned on a towel or piece of clothing on an object in such a way that this attenuates minor vibrations of the environment, but the box cannot slide off it.
>
>For an overall comparison, we can photograph the box from all visible sides and store these photos on a device that is as secure as possible, send it to a trusted person via an encrypted and verified channel, or send it to another device of our own. The next step is to compare the found mosaic with the original one. The app Blink Comparison is ideal for this purpose.
>
>To protect an object from damage, e.g., by staining or by the substance leaking into, say, the ports of a laptop, it can be wrapped in cling film, a bag, or otherwise.

Several colorful mixtures are described: [red lentils & beluga lentils](https://dys2p.com/en/2021-12-tamper-evident-protection.html#rote-linsen-und-belugalinsen), [yellow peas & white beans](https://dys2p.com/en/2021-12-tamper-evident-protection.html#gelbe-erbsen-und-wei%C3%9Fe-bohnen), etc. For a box that is transparent on all sides and fits a laptop, a small fish tank works well. For longer-term storage, [vacuum seals](https://dys2p.com/en/2021-12-tamper-evident-protection.html#laengerfristige-lagerung-oder-versand) can be used. 

This excerpt assumes that we take the cell phone with us, but [as discussed elsewhere](/posts/nophones/#do-you-really-need-a-phone), this has its own security issues and is not recommended. So the smartphone we use to take a picture of the storage will have to stay in the house outside of the storage. [As discussed below](/posts/tamper/#physical-intrusion-detection), we recommend that you get a cheap Android phone that only runs an app called Haven when you are out of the house. This device will stay out of storage anyway, so you can use it to take pictures of the storage. Alternatively, if you don't have a dedicated Haven phone but do have a [GrapheneOS](/posts/grapheneos/) device, you can use it to take photos of the storage and then hide it somewhere in your house while you're away. If you don't have a phone, you can use a camera. However, cameras don't have encryption, so it's much easier for an adversary to modify the photos and you won't be able to use the Blink Comparison app to facilitate the comparison. 

## In practice 

* Once you have placed the bagged electronic devices in the container and covered them with a colorful mixture, take photos using the Blink Comparison app. Optionally, send them to another device of your own (that is currently in storage) via [Molly](/posts/e2ee/#signal) or [SimpleX Chat](/posts/e2ee#simplex-chat). Close Blink Comparison so that the storage is encrypted. 
	* *If you are using a dedicated Haven phone (preferred)*: Set up Haven for physical intrusion detection before leaving, as described below. 
	* *If you are using a GrapheneOS phone*: Turn off the device and hide it somewhere. If the phone is found and the firmware or software is modified, Auditor will notify you. 
* When you return, use Blink Comparison to verify the mosaic with new photos. 
	* Optionally, if you sent the photos to yourself on Molly/SimpleX Chat, once your devices are out of storage you can verify that they don't differ from the reference photos saved in Blink Comparison. However, the Blink Comparison encryption makes it very unlikely that these reference photos were modified in your absence. 

# Tamper-Evident Software and Firmware  

So far, we have only looked at making hardware compromise tamper-evident. It is also possible to make software and firmware tamper-evident. This is required for "defense in depth" — to trust an electronic device, you must trust the hardware, firmware, and software. Software or firmware compromise [can occur remotely](/posts/tails-best#2-running-tails-on-a-computer-with-a-compromised-bios-firmware-or-hardware) (over the Internet) as well as with physical access, so it is especially important because the other measures won't necessarily detect it. Tamper-evident firmware is compatible with our [recommendations](/recommendations): Qubes OS or Tails on laptops, or GrapheneOS on a smartphone. 

For GrapheneOS, [Auditor](/posts/grapheneos/#auditor) is an app that allows you to be notified if firmware or operating system software has been tampered with — you will receive an email when Auditor performs a remote attestation. 

For Tails or Qubes OS, [Heads](https://osresearch.net/) can do the same before you enter your boot password (on [supported devices](https://osresearch.net/Prerequisites#supported-devices)). However, installing Heads is advanced, though using it is not. Keep the Heads USB security dongle with you when you leave the house, and have a backup hidden at a trusted friend's house in case it ever falls into a puddle. For more information, see [Tails Best Practices](/posts/tails-best/#to-mitigate-against-remote-attacks). 

If Auditor or Heads ever detects tampering, you should immediately treat the device as untrusted. [Forensic analysis](https://www.notrace.how/threat-library/mitigations/computer-and-mobile-forensics.html) may be able to reveal how the compromise occured, which helps to prevent it from happening again. You can get in touch with a service like [Access Now’s Digital Security Helpline](https://accessnow.org/help), though we recommend not sending them any personal data.

# Physical Intrusion Detection 

[Physical intrusion detection](https://notrace.how/threat-library/mitigations/physical-intrusion-detection.html) is the process of detecting when an adversary enters or attempts to enter a space. As the Threat Library notes:

> A video surveillance system that monitors a space can have the following characteristics:
>
>* The cameras can be motion-activated and send you an alert if they are detected and tampered with.
>* The cameras can be positioned with the space entrances in their line of sight and/or in a discreet location.
>* To prevent the system from monitoring you while you are in the space, you can turn it on just before you leave the space and turn it off as soon as you return.

We recommend employing physical intrusion detection in addition to all of the tamper-evident measures. That way, even if a covert house search doesn't interact with the tamper-evident storage (for example, because the goal is to install [covert surveillance devices](https://notrace.how/threat-library/techniques/covert-surveillance-devices.html)), you can still find out about it. 

Haven is an Android app developed by the Freedom of Press Foundation that uses the smartphone’s many sensors — microphone, motion detector, light detector, and cameras — to monitor the room for changes, and it logs everything it notices. Unfortunately Haven is currently unmaintained, remote notifications are [broken](https://github.com/guardianproject/haven/issues/454), and it is unreliable on many devices. 

Until [Haven is fully functional](https://github.com/guardianproject/haven/issues/465), we recommend also using a video surveillance system so that you can receive remote notifications — this is important to protect against the local logs being modified by an intruder. Choose a model with privacy features (e.g. it doesn't function through the cloud) so that the police cannot easily learn the timing of your comings and goings from it. For instance, [motionEye OS](https://github.com/motioneye-project/motioneyeos/wiki/Features) supports remote notifications for motion detection, but it requires Linux knowledge to set up. 

## In practice 

Haven should be used on a dedicated cheap Android device that is otherwise empty. An older [Pixel](https://www.privacyguides.org/android/#google-pixel) is a good choice because it is cheap but has good cameras, which is important for both Haven and Blink Comparison — it may even [still be supported by GrapheneOS](https://grapheneos.org/faq#device-lifetime). Make sure that [full disk encryption](/glossary/#full-disk-encryption-fde) is enabled. If you have a smartphone in addition to the dedicated Haven phone, it should be turned off in the tamper-evident storage — if Haven was running on it instead and was discovered by the intruder, they would now have physical access to the device while it was turned on. 

* Place the Haven smartphone in a location that has a line of sight to where an intruder would have to pass, such as a hallway that must be used to move between rooms or to access where the tamper-evident storage is located. It should be plugged in so the battery doesn't die; fairly long cables are available for this purpose. 
* Set a countdown to turn Haven on before you leave the house. The Haven app will log everything locally on the Android device. As mentioned above, sending remote notifications is currently broken.
* Check the Haven log when you get home. 

# Wrapping Up 

With the measures described above, any 'evil maid' would have to bypass:

1) Physical intrusion detection, and
2) The tamper-evident storage, and
3) The tamper-evident glitter nail polish (for an attack that requires opening the laptop), or Heads/Auditor (for a software or firmware attack)

These layers are all important, although they may seem redundant. The expertise and cost required to successfully execute the attack increases significantly with each layer, making it much less likely that an adversary will attempt it in the first place. The best practice is to [obtain a fresh device in such a way that it cannot be intercepted](/posts/tails-best/#to-mitigate-against-physical-attacks), and then consistently implement all of these layers from the beginning. 

## In practice 

To summarize, take the following measures every time you leave the house with no one home for a significant amount of time: 

1) Put the turned-off devices into tamper-evident storage 
2) Take the necessary photos 
3) Activate Haven 

This may sound tedious, but it can be done in less than a minute if you leave unused devices in storage. When you get home: 

1) Start by checking the Haven log 
2) Next, verify the tamper-evident storage with Blink Comparison

Laptop screws can be verified when something suspicious happens. Neither Heads nor Auditor require much effort to use properly once set up; Auditor runs without interaction and Heads becomes part of your boot process. 

# Further Reading

* [Random Mosaic — Detecting unauthorized physical access with beans, lentils and colored rice](https://dys2p.com/en/2021-12-tamper-evident-protection.html)

# Appendix: Cracking Safes

* [Rare-earth magnets](https://en.wikipedia.org/wiki/Safe-cracking#Magnet_risk) can unlock safes that use a [solenoid](https://www.youtube.com/watch?v=Y6cZrieFw-k) as the locking device in an undetectable manner.
* [Safe bouncing](https://en.wikipedia.org/wiki/Safe-cracking#Safe_bouncing) is when the locking mechanism can be moved sufficiently by [banging or bouncing the safe](https://mosandboo.com/how-to-open-a-safe-without-the-key-or-code/) to open it in an undetectable manner. Safes that use a gear mechanism are less susceptible to mechanical attacks. 
* Many safe models have a "management reset code" (also known as a "try-out combination") — if this code is not changed from its default setting the safe can be unlocked in an undetectable manner.  
* [Spiking](https://en.wikipedia.org/wiki/Safe-cracking#Spiking_the_lock) is when the wires leading to the reset button, solenoid, or motor can be exposed and spiked with a battery. This should be possible to make tamper-evident, as it requires access to the wires.
* [Brute force](/glossary#brute-force-attack) attacks — trying all possible combinations — are possible if the adversary has time. Dialing mechanisms can be brute-forced with a [computerized autodialer](https://learn.sparkfun.com/tutorials/building-a-safe-cracking-robot) that [doesn't need supervision](https://www.youtube.com/watch?v=vkk-2QEUvuk). Electronic keypads are less susceptible to brute force if they have a well-designed incremental lockout feature; for example, get it wrong 10 times and you're locked out for a few minutes, 5 more wrong codes and you're locked out for an hour, etc. 
* There are several tools that can automatically retrieve or reset the combination of an electronic lock, such as the Little Black Box and Phoenix. Tools like these are often connected to wires inside the lock that can be accessed without damaging the lock or container. This should be possible to make tamper-evident, as it requires access to the wires.
* There are several [keypad-based attacks](https://en.wikipedia.org/wiki/Safe-cracking#Keypad-based_attacks), and some can be mitigated with proper operational security. 

