+++
title="Encrypted Messaging for Anarchists"
date=2024-04-19

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "e2ee", "beginner"]

[extra]
blogimage="/images/cwtch-dither.png"
toc=true
dateedit=2024-04-19
a4="e2ee-a4-en.pdf"
letter = "e2ee-letter-en.pdf"
+++
There are several different options for [end-to-end encrypted](/glossary/#end-to-end-encryption-e2ee) communication, each with different trade-offs. This article provides an overview and installation instructions for Tails, Qubes OS, and GrapheneOS. 
<!-- more -->
Before proceeding, let’s go over a few concepts to help you distinguish between the different options. 

* **End-to-end encryption** means (in theory) that only you and the person you are communicating with can read messages. However, not all [encryption](/glossary/#encryption) is created equal. The quality of the encryption is determined by the *encryption protocol* used and how it's implemented at the software level. 
* **Metadata protection** means that the message [*metadata*](/glossary/#metadata) (the data about the data) is obscured. Even if the message itself is encrypted, metadata can reveal who is communicating with whom, when, how often, the sizes of any files that may have been transferred, and so on. Metadata exposure is [a major concern](https://docs.cwtch.im/security/risk#threat-model). 
* **Peer-to-peer** means that the messages do not pass through a centralized server. 
* **Tor** is an [anonymity network](/glossary/#tor-network). Some applications route your messages through Tor by default. 

For a more in-depth look at these various considerations, we recommend [The Guide to Peer-to-Peer, Encryption, and Tor: New Communication Infrastructure for Anarchists](https://www.notrace.how/resources/#pet-guide). This text criticizes Signal for not being peer-to-peer and not using Tor by default, and goes on to compare Signal, Cwtch, and Briar. 

Since anonymous public-facing projects such as counter-info websites interact with unknown (i.e. untrusted) contacts, they need more from encrypted communication than people using applications for private communication. These additional needs include:

* That anyone can contact the project
* Resiliency to [correlation attacks](/glossary/#correlation-attack) 
* Resiliency to [exploits](/glossary/#exploit) 
* For multiple project members to be able to access the same messages

The following recommendations for encrypted messaging are listed in order of highest to lowest metadata protection. 

**TL;DR** 

* Cwtch for text communication 
* SimpleX Chat or Signal for voice/video calls
* PGP Email for anonymously-run public projects

# Cwtch

![](/posts/e2ee/cwtch.png)

* **Mediums**: Text
* **Metadata protection**: Yes (strong)
* **Encryption protocol**: Tor Onion Services (v3) + [Tapir](https://docs.cwtch.im/security/components/tapir/authentication_protocol)
* **Peer-to-peer**: Yes
* **Tor**: Yes

Cwtch is our preference for text communication by a long shot. Cwtch is designed with metadata protection in mind; it's peer-to-peer, uses the Tor network, and stores all data locally on the device, encrypted.

<br>

<video controls="" width="99%">
   <source src="cwtch-explainer.mp4" type="video/mp4">
 </video>

<br>
<br>

Like all peer-to-peer communication, Cwtch requires *[synchronous](/glossary/#synchronous-communication)* communication, meaning that both people must be online at the same time. However, its server feature also allows *[asynchronous](/glossary/#asynchronous-communication)* communication by providing offline delivery:

>"Cwtch contact to contact chat is fully peer to peer, which means if one peer is offline, you cannot chat, and there is no mechanism for multiple people to chat. To support group chat (and offline delivery) we have created untrusted Cwtch servers which can host messages for a group. [...] the server has no way to know what messages for what groups it might be holding, or who is accessing it."

Cwtch servers enable group communication through untrusted infrastructure — these servers are "untrusted" because the protocol is [designed to be secure against a malicious Cwtch server](https://docs.cwtch.im/security/components/cwtch/server). Once the server exists, contacts can be invited to use it. For asynchronous direct messaging, create a group chat with only two people. 

Any Cwtch user can turn the app on their phone or computer into a server to host a group chat, though this is best for temporary needs like an event or short-term coordination, as the device must remain powered on for it to work. Fortunately, [Anarchy Planet](https://anarchyplanet.org/chat.html#cwtch) runs a public server that is suitable for long-term groups. 

Asynchronous conversations on Cwtch need to be started from a synchronous conversation — in other words, you need to be online at the same time as your contact to invite them to a group, and then you no longer need to be online at the same time. This "first contact" dynamic is not unique to Cwtch, but is present in all peer-to-peer applications. In the future, Cwtch plans to improve this with [hybrid groups](https://docs.cwtch.im/blog/path-to-hybrid-groups/). Until hybrid groups are implemented, you will need to establish your asynchronous Cwtch conversations by using a second channel to set a time when you will both be online. 

You can learn more about how to use Cwtch with the [Cwtch Handbook](https://docs.cwtch.im/).

>**Note**
>
>**[Briar](https://briarproject.org)** is another application that works in a similar way (with peer-to-peer and Tor), using the [Bramble Transport Protocol](https://code.briarproject.org/briar/briar/-/wikis/A-Quick-Overview-of-the-Protocol-Stack) (BTP). Briar's main distinguishing feature is that it continues to work [even when the underlying network infrastructure is down](https://briarproject.org/how-it-works/). It was [audited in 2017](https://code.briarproject.org/briar/briar/-/wikis/FAQ#has-briar-been-independently-audited). Unfortunately, Briar Desktop does not yet work with Tails or Qubes-Whonix because it cannot [use the system Tor](https://code.briarproject.org/briar/briar/-/issues/2095). Unlike Cwtch, to connect to a contact on Briar, you both have to add each other first. You can either exchange `briar://` links or scan a contact’s QR code if they are nearby. [Briar Mailbox](https://briarproject.org/download-briar-mailbox/) allows asynchronous communication. 
>
>**[OnionShare](https://docs.onionshare.org/2.6/en/features.html#chat-anonymously)** has a chat feature that creates an ephemeral peer-to-peer chat room that is routed over the Tor network. The metadata protection works in the same way as Cwtch; it uses the Tor network as a shield and stores everything (ephemerally) locally on the device running OnionShare. OnionShare doesn’t implement any chat encryption on its own — it relies on the Tor onion service’s encryption. Cwtch and Briar both have more features (including the additional Tapir and BTP encryption protocols). The only advantage of OnionShare is that it is installed on Tails by default. 

## For Anonymous Public-facing Projects

**Need #1: That anyone can contact the project**

Anyone can connect to a public Cwtch account when it's online. If the account is offline, it's not currently possible to establish first contact, though this will be supported in the future. 

**Need #2: Resiliency to correlation attacks**

Real-time messaging applications are particularly susceptible to end-to-end correlation attacks because of the ability of an adversary, once they know their target's ID on the messaging platform, to trigger incoming network traffic on the target's side by sending them messages on the platform (when the target is online). "Appear Offline Mode" in Cwtch allows a user to selectively connect to trusted contacts and groups, while appearing offline to everyone else. An [issue](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/712) is open to further address this. 

[Content padding exists](https://docs.cwtch.im/security/components/tapir/packet_format) to frustrate correlation attacks via message size.

**Need #3: Resiliency to exploits**

A vulnerability in any application can be targeted with exploits — a severe vulnerability can allow an adversary to hack your system, such as by permitting [Remote Code Execution](https://en.wikipedia.org/wiki/Arbitrary_code_execution). Cwtch libraries are written in memory-safe languages (Go and Rust) and Cwtch does [fuzz testing](https://openprivacy.ca/discreet-log/07-fuzzbot/) to find bugs. See the [Security Handbook](https://docs.cwtch.im/security/intro) to learn more. For public-facing project accounts, we recommend against enabling the "file sharing experiment" or the "image previews and profile pictures experiment" in the settings.

**Need #4: For multiple project members to be able to access the same messages**

If a project has multiple members, all of them should be able to access the same messages independently. Currently, this is not possible with Cwtch.  

## Installation

<details>
<summary>

**Cwtch Installation on GrapheneOS**

</summary>
<br>

Install Cwtch the same way you would install any [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software) (we don't recommend F-Droid). 

<br>
</details>

<details>
<summary>

**Cwtch Installation on Tails**

</summary>
<br>

Cwtch support for Tails is very new and not thoroughly tested.

* Start Tails with an Adminstration Password.
* Download [Cwtch for Linux](https://cwtch.im/download/#linux) with Tor Browser
* According to our [Tails Best Practices](/posts/tails-best/#using-a-write-protect-switch), personal data should be stored on a second LUKS USB and Persistent Storage should not be enabled.  Extract the folder with the file manager (right click, select "Extract"), then move the `cwtch` folder to such a "personal data" LUKS USB.
* Run the install script
	* In the File Manager, enter the `cwtch` directory you just moved, so that you can see a file named "install-tails.sh". Right click in the File Manager and select "Open in Terminal"
	* Run `./install-tails.sh` and enter the Administration Password when prompted.
* You can now launch Cwtch from the "Activities" overview. 
* With Persistent Storage disabled, profile data must be restored from backup every session you need to install Cwtch. Export your profile when you are done using Cwtch, copy it to the "personal data" LUKS USB, and import it again the next time you install Cwtch. 
	* Alternatively, you can backup `/home/amnesia/.cwtch` to the "personal data" LUKS USB, and copy it back to `/home/amnesia/` before you open Cwtch during your next session, in order to also persist configuration settings. "Show Hidden Files" will need to be enabled in the File Manager. 
* When a new version of Cwtch is released, you will have to update manually. Download the new version, extract it, and replace the `cwtch` folder. 

<br>
</details>

<details>
<summary>

**Cwtch Installation on Qubes-Whonix**

</summary>
<br>

Cwtch on Whonix does not guarantee Tor [Stream Isolation](/posts/qubes/#whonix-and-tor) from other applications in the same qube, so we will install it in a dedicated qube. Cwtch is installed in an App qube, follow the [installation instructions](https://docs.cwtch.im/docs/platforms/whonix/).

<br>
</details>

<br>

# SimpleX Chat

![](/posts/e2ee/network.png) 

* **Mediums**: Video call, voice call, text
* **Metadata protection**: Yes (Moderate)
* **Encryption protocol**: [SimpleX Messaging Protocol](https://simplex.chat/docs/protocol/simplex-chat.html), audited ([2022](https://simplex.chat/blog/20221108-simplex-chat-v4.2-security-audit-new-website.html)), and [SimpleX File Transfer Protocol](https://simplex.chat/blog/20230301-simplex-file-transfer-protocol.html)
* **Peer-to-peer**: No 
* **Tor**: Not default

SimpleX Chat allows voice and video calls, but this [inherently provides less metadata protection](https://mastodon.social/@sarahjamielewis/112311305534271974). As a design choice to facilitate asynchronous communication, SimpleX Chat is not peer-to-peer — it uses decentralized servers that [anyone can host](https://simplex.chat/docs/server.html) and does not rely on any centralized component. Servers do not store any user information (no user profiles or contacts, or messages once they are delivered), and primarily use in-memory persistence. To understand what a server can and cannot see, read the [threat model](https://github.com/simplex-chat/simplexmq/blob/stable/protocol/overview-tjr.md#simplex-messaging-protocol-server). 

Since SimpleX requires that users [place some trust in the SimpleX servers](https://github.com/simplex-chat/simplexmq/blob/stable/protocol/overview-tjr.md#trust-in-servers), **we recommend prioritizing Cwtch over SimpleX Chat for text communication with other anarchists, and using SimpleX Chat or Signal for voice and video calls**. Unlike Signal, SimpleX Chat doesn't require a phone number or smartphone. 

If SimpleX is served with a warrant, their [privacy policy](https://github.com/simplex-chat/simplex-chat/blob/stable/PRIVACY.md) is quite specific. Servers have the [records of the message queues](https://github.com/simplex-chat/simplex-chat/blob/stable/PRIVACY.md#connections-with-other-users) and any [undelivered encrypted messages](https://github.com/simplex-chat/simplex-chat/blob/stable/PRIVACY.md#messages-and-files) — no data is stored that links the queues or messages to particular users, and the data which is stored is not very useful without access to the user's device.  

SimpleX Chat will work with Tor if used on an operating system that forces it to, such as Whonix or Tails. However, voice and video calls generally don't work very well over Tor regardless of which application you use. 

You can learn more about how to use SimpleX Chat with their [guide](https://simplex.chat/docs/guide/readme.html). Make sure to set a [database passphrase](https://simplex.chat/docs/guide/privacy-security.html#database-passphrase). 

## For Anonymous Public-facing Projects

**Need #1: That anyone can contact the project**

Unlike the one-time invitation links that are normally used by SimpleX Chat and shared through a separate channel, you also have a [long term address](https://simplex.chat/docs/guide/app-settings.html#your-profile-settings) that can be published online so that anyone can connect to you. We recommend against enabling "Auto-accept". 

**Need #2: Resiliency to correlation attacks**

Real-time messaging applications are particularly susceptible to end-to-end correlation attacks because once an adversary knows their target's ID on the messaging platform, they can trigger incoming network traffic on the target's side by sending them messages on the platform (when the target is online). An [issue](https://github.com/simplex-chat/simplex-chat/issues/3197) is open to address this. Message "mixing" is also [planned](https://github.com/simplex-chat/simplex-chat#privacy-and-security-technical-details-and-limitations).

[Content padding exists](https://github.com/simplex-chat/simplex-chat#privacy-and-security-technical-details-and-limitations) to frustrate correlation attacks via message size.

**Need #3: Resiliency to exploits**

A vulnerability in any application can be targeted with exploits — a severe vulnerability can allow an adversary to hack your system, such as by permitting [Remote Code Execution](https://en.wikipedia.org/wiki/Arbitrary_code_execution). For public-facing project accounts, we recommend that you set SimpleX Chat preferences to only allow text (prohibiting voice messages and attachments).

**Need #4: For multiple project members to be able to access the same messages**

If a project has multiple members, all of them should be able to access the same messages independently. Currently, this is not possible with SimpleX Chat.  

## Installation

<details>
<summary>

**SimpleX Chat Installation on GrapheneOS**

</summary>
<br>

Install SimpleX Chat the same way you would install any [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software) (we don't recommend F-Droid). If you're using a VPN (as [we recommend](/posts/grapheneos/#how-to-install-software)) then the default relay for calls is redundant and can be turned off to improve call quality: **Settings → Audio & video calls**, disable **Always use relay**   

<br>
</details>

<details>
<summary>

**SimpleX Chat Installation on Tails**

</summary>
<br>

* Tails does *not* need an Administration Password to run an AppImage package.
* Download the [AppImage](https://simplex.chat/downloads/#desktop-app) with Tor Browser
* According to our [Tails Best Practices](/posts/tails-best/#using-a-write-protect-switch), personal data should be stored on a second LUKS USB and Persistent Storage should not be enabled. Copy the .AppImage file to such a "personal data" LUKS USB.
* Make the AppImage executable
	* In the File Manager, right-click "Properties". Under "Permissions", enable "Executable as Program". 
* You can now launch SimpleX Chat by double-clicking the AppImage file.
* In **Settings → Network & Servers**, enable "Use SOCKS proxy (port 9050)" (to configure SimpleX Chat [to go through Tor](https://tails.net/doc/persistent_storage/additional_software/index.en.html#index5h2)). You can now create a SimpleX address.
* With Persistent Storage disabled, configuration and profile data must be restored from backup every session you use SimpleX Chat. Export your database (**Settings → Database passphrase & export**) when you are done using SimpleX Chat, copy it to the "personal data" LUKS USB, and import it again the next time you use SimpleX Chat. 
	* Alternatively, you can backup `/home/amnesia/.local/share/simplex` to the "personal data" LUKS USB, and copy it back to `/home/amnesia/.local/share` before you open SimpleX during your next session. "Show Hidden Files" will need to be enabled in the File Manager.
* When a new version of SimpleX Chat is released, you will have to update manually. Download the new version and replace the .AppImage file. 

<br>
</details>


<details>
<summary>

**SimpleX Chat Installation on Qubes-Whonix**

</summary>
<br>

SimpleX Chat on Whonix does not guarantee Tor [Stream Isolation](/posts/qubes/#whonix-and-tor) from other applications in the same qube, so we will install it in a dedicated qube. SimpleX Chat is installed in an App qube, not a Template (because it is an AppImage).

* Download the [AppImage](https://simplex.chat/downloads/#desktop-app) using Tor Browser in a disposable Whonix qube.
* [Create an App qube](/posts/qubes/#how-to-organize-your-qubes) with the Template `whonix-workstation-17` and networking `sys-whonix`.
* Copy the file to your new App qube 
* Make the AppImage executable
	* In the File Manager, right-click "Properties". Under "Permissions", enable "Allow this file to run as a program". 
* You can now launch SimpleX Chat by double-clicking the AppImage file.
* When a new version of SimpleX Chat is released, you will have to update manually. Download the new version and replace the .AppImage file. 

<br>
</details>

<br>

# Signal

![](/posts/e2ee/signal.jpg) 

* **Mediums**: Video call, voice call, text
* **Metadata protection**: Yes (Moderate)
* **Encryption protocol**: Signal Protocol, audited ([2017](https://en.wikipedia.org/wiki/Signal_Protocol))
* **Peer-to-peer**: No 
* **Tor**: Not default

The Signal Protocol has a moderate amount of metadata protection; [sealed sender](https://signal.org/blog/sealed-sender/), [private contact discovery](https://signal.org/blog/private-contact-discovery/), and the [private group system](https://signal.org/blog/signal-private-group-system/). Message recipient identifiers are only stored on Signal's servers for as long as it takes to deliver each message. As a result, if Signal is served with a warrant, they [will only be able to provide](https://signal.org/bigbrother/) the time of account creation and the date of the account's last connection to the Signal servers. Still, Signal relies on the Google Services Framework (though it's possible to use Signal without it), and the sealed sender metadata protection applies only to contacts (by default).  

Signal is not peer-to-peer; it uses centralized servers that we must trust. Signal will work with Tor if used on an operating system that forces it to, such as Whonix or Tails. 

Signing up for a Signal account is difficult to do anonymously. The account is tied to a phone number that the user must retain control of — due to [changes in "registration lock"](https://blog.privacyguides.org/2022/11/10/signal-number-registration-update/), it is no longer sufficient to register with a disposable phone number. An anonymous phone number can be obtained [on a burner phone or online](https://anonymousplanet.org/guide.html#getting-an-anonymous-phone-number) and must be maintained  as long as you’re using it, which takes some technical know-how and money, limiting the amount of people who will do this. 

Another barrier to anonymous registration is that Signal Desktop will only work if Signal is first registered from a smartphone. For users familiar with the [command line](/glossary/#command-line-interface-cli), it is possible to register an account from a computer using [Signal-cli](https://0xacab.org/about.privacy/messengers-on-tails-os/-/wikis/HowTo#signal). The [VoIP](/glossary#voip-voice-over-internet-protocol) account used for registration would have to be obtained anonymously. 

These barriers to anonymous registration mean that Signal is rarely used anonymously. This has significant implications if the State gains [physical](/glossary/#physical-attacks) or [remote](/glossary/#remote-attacks) access to the device. One of the primary goals of State surveillance of anarchists is [network mapping](https://notrace.how/threat-library/techniques/network-mapping.html), and it's common for them to gain physical access to devices through [house raids](https://notrace.how/threat-library/techniques/house-raid.html) or arrests. For example, if police bypass your device's [authentication](https://notrace.how/threat-library/techniques/targeted-digital-surveillance/authentication-bypass.html), they can identify Signal contacts (as well as the members of any groups you are in) simply by their phone numbers, if those contacts haven't changed their settings to hide their phone number. 

In a recent [repressive operation in France against a riotous demonstration](https://www.notrace.how/resources/#lafarge), the police did exactly that. Police got physical access to suspects' phones during arrests and house raids, remote access through spyware, and then identified Signal contacts and group members. These identities were added to the list of suspects who were subsequently investigated.

The risk of a compromised device aiding the police in network mapping is partly mitigated by the [username feature](https://signal.org/blog/phone-number-privacy-usernames/) — use it to prevent a Signal contact from being able to learn your phone number. In **Settings → Privacy → Phone Number**, set both **Who can see my number** and **Who can find me by number** to **Nobody**. We recommend that you select a profile name and photo that won't be useful for establishing your identity. For voice and video calls, Signal reveals the IP address of both parties by default, which could also be used to identify Signal contacts. If you aren't using Signal from behind a VPN or Tor, then in **Settings → Privacy → Advanced**, enable **Always relay calls** to prevent this. 

A private company that sells spyware to governments has a product called JASMINE that is [marketed to deanonymize Signal users](https://securitylab.amnesty.org/latest/2023/10/technical-deep-dive-into-intellexa-alliance-surveillance-products), based on the analysis of metadata.  

>In its targeted interception mode — which starts from a single target — JASMINE has claimed it is able to identify communicating parties in encrypted but peer-to-peer applications [...]  the JASMINE documentation explicitly claims support for identifying the IP addresses of participants in encrypted apps such as WhatsApp and Signal during voice and video calls where peer-to-peer connections are also used for calling by default.
>
>The JASMINE documentation also explains that by analysing encrypted traffic “events” for a whole country — in mass interception mode — JASMINE has the ability to correlate and identify the participants in encrypted group chats on messaging apps. 

A similar surveillance product would not work against Cwtch because it uses Tor by default. Without a Tor or VPN proxy, an adversary can see that you are connecting to Signal servers which is what enables this type of timing correlation attack. Although it is possible to configure Signal to use a VPN or Tor, it is opt-in so most people will not use it like this.  

Signal was designed to bring encrypted communication to the masses, not for an anarchist threat model. Because it's very difficult to register for Signal anonymously, and because you must first install Signal on a phone to use it on a computer, **we recommend prioritizing Cwtch over Signal for text communication with other anarchists, and using SimpleX Chat or Signal for voice and video calls.** For the same reasons, Signal is not well-suited for anonymous public-facing projects.  

## Installation

<details>
<summary>

**Signal Installation on GrapheneOS**

</summary>
<br>

We recommend the [Signal Configuration and Hardening Guide](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/). As noted above, unless you are familiar with the [Command Line Interface](/glossary/#command-line-interface-cli), Signal needs to be registered on a smartphone before it can be connected to a computer. Install Signal the same way you would install any [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software) (we don't recommend F-Droid). If you are using Signal from behind a VPN (as [we recommend](/posts/grapheneos/#how-to-install-software)) then a relay for calls is redundant and should be turned off: **Settings → Privacy → Advanced**, disable **Always relay calls**


[Molly-FOSS](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/#molly-android) is a fork of Signal with hardening and anti-forensic features available on Android — we recommend it over Signal, and trusting the Molly team is made easier by its [reproducible builds](https://github.com/mollyim/mollyim-android/tree/main/reproducible-builds). Follow the instructions for [installing software that isn't available in the Play Store](/posts/grapheneos/#software-that-isn-t-on-the-play-store). You can [migrate from an existing Signal account](https://github.com/mollyim/mollyim-android#compatibility-with-signal). Turn on database encryption. 

<br>
</details>

<details>
<summary>

**Signal Installation on Tails**

</summary>
<br>

About.Privacy [maintains a guide](https://0xacab.org/about.privacy/messengers-on-tails-os/-/wikis/HowTo) for installing Signal Desktop on Tails. There is a guide for registering an account from Tails without a smartphone (using Signal-cli), and another guide for if you already have a Signal account.  

Some of the [Signal Configuration and Hardening Guide](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/) also applies to Signal Desktop.

<br>
</details>


<details>
<summary>

**Signal Installation on Qubes-Whonix**

</summary>
<br>

Signal Desktop on Whonix is not guaranteed to have Tor [Stream Isolation](/posts/qubes/#whonix-and-tor) from other applications in the same qube, so we will install it in a dedicated qube. Signal Desktop is installed in a Template, not an App qube (because it is available as a .deb from a third party repository). 

Some of the [Signal Configuration and Hardening Guide](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/) also applies to Signal Desktop.

* Go to **Applications menu → Qubes Tools → Qube Manager**
* Clone whonix-workstation-17 and name it something like whonix-workstation-17-signal. 
	* We do this to avoid adding attack surface to the base Whonix Workstation template. If you also install other messaging applications, they could share a cloned template with a name like whonix-workstation-17-e2ee 
* Open a Terminal in the new Template: **Applications menu → Template: whonix-workstation-17-signal: Xfce Terminal**
* Run the commands in the [Signal installation guide](https://www.signal.org/download/linux/) to install Signal Desktop in the Template. 
	* Note that the layout of the Signal installation guide is a bit confusing for users unfamiliar with the command line; `wget` and `cat` are separate commands, but `echo` in #2 is a command so long that it takes two lines (which is why the second line is indented). 
	* Template qubes require a proxy for `wget`. Before running the command, create a configuration file at `~/.wgetrc` in the Template, with the following contents:
```bash
use_proxy = on
http_proxy = 127.0.0.1:8082
https_proxy = 127.0.0.1:8082
```
* [Create an App qube](/posts/qubes/#creating-qubes) with the Template `whonix-workstation-17-signal` and networking `sys-whonix`. 
* In the **Settings → Applications** tab of the new App qube, you may need to click "Refresh applications" for Signal to show up. Move Signal to the Selected column and press "OK".
* Updates will be handled by **Qubes Update** as you would expect. 
	
>**Alternative method**
>
>You can install Signal Desktop in a Whonix Workstation App qube using [Qube Apps](https://micahflee.com/2021/11/introducing-qube-apps/) and not need to bother with Templates. Signal Desktop on Flathub is [community maintained](https://github.com/flathub/org.signal.Signal), not official, which [is a security consideration](https://www.kicksecure.com/wiki/Install_Software#Flathub_Package_Sources_Security). 

<br>
</details>

<br>

# PGP Email

<p>
<span class="is-hidden">
![](/posts/e2ee/pgp.webp)
</span>
<img src="/posts/e2ee/pgp.webp" class="no-dark">
</p>

* **Mediums**: Text
* **Metadata protection**: No
* **Encryption protocol**: [RSA](https://blog.trailofbits.com/2019/07/08/fuck-rsa/) or ed25519, no forward secrecy
* **Peer-to-peer**: No 
* **Tor**: Not default

PGP (Pretty Good Privacy) is not so much a messaging platform as it is a way to encrypt messages on top of existing messaging platforms (in this case, email). PGP email does not have the encryption property of [*forward secrecy*](/glossary/#forward-secrecy). The goal of forward secrecy is to protect past sessions from future key or password compromises. It maintains the secrecy of past communications even if the current communication is compromised. This means that an adversary could decrypt all past PGP messages in one fell swoop. When you also consider the metadata exposure inherent in email, PGP simply doesn't meet the standards of modern cryptography. For a more technical critique, see [The PGP Problem](https://latacora.micro.blog/2019/07/16/the-pgp-problem.html) and [Stop Using Encrypted Email](https://latacora.micro.blog/2020/02/19/stop-using-encrypted.html). [Privacy Guides](https://www.privacyguides.org/en/basics/email-security/) agrees that "email is best used for receiving transactional emails [...], not for communicating with others." **We recommend that anarchists still using PGP email use Cwtch groups instead.** 

**There is an exception: for anonymous public-facing projects, we still recommend using PGP email** because it is currently the best option that meets the additional needs required by a public account. Use a [radical server](https://riseup.net/en/security/resources/radical-servers) that doesn't require an invite code. You can learn more about how to use PGP email with the [Riseup Guide to Encrypted Email](https://riseup.net/en/security/message-security/openpgp). 

>**Note**
>
>PGP is used for another purpose outside of communication: verifying the integrity and authenticity of files. For this use case, see our [explanation](/posts/tails-best/#appendix-gpg-explanation).

## For Anonymous Public-facing Projects

**Need #1: That anyone can contact the project**

Anyone can send a message to a public email account regardless of whether the recipient is online or offline. 

**Need #2: Resiliency to correlation attacks**

Email is not a real-time messaging application — this means that it is not particularly susceptible to end-to-end correlation attacks via time. 

No content padding exists to frustrate correlation attacks via message size in email protocols, but if you access the mail servers through Tor then the traffic is padded. 

**Need #3: Resiliency to exploits**

A vulnerability in any application can be targeted with exploits — a severe vulnerability can allow an adversary to hack your system, such as by permitting [Remote Code Execution](https://en.wikipedia.org/wiki/Arbitrary_code_execution). Email can be accessed through webmail (via Tor Browser) or through a client like Thunderbird — these have different attack surfaces. For example, a Cwtch developer found an exploit to [turn Thunderbird into a decryption oracle](https://pseudorandom.resistant.tech/disclosing-security-and-privacy-issues-in-thunderbird.html) when it displays messages with HTML.  

We recommend using Thunderbird (which is available in Tails and Qubes-Whonix by default) with the setting to display email as "Plain Text" rather than as HTML: **View → Message Body As → Plain Text**. Most webmail will not function with Tor Browser in "Safest" mode. 

**Need #4: For multiple project members to be able to access the same messages**

If a project has multiple members, all of them should be able to access the same messages independently. This is straight forward with email, if all project members have the email password and the private PGP key.  

# Applications we do not recommend

We do *not* recommend:

* **Telegram**: Telegram has no end-to-end encryption for group chats, and it is opt-in for one-on-one chats. The encryption doesn't use established protocols, and has had cryptographers describe it as ["the most backdoor-looking bug I’ve ever seen"](https://words.filippo.io/dispatches/telegram-ecdh/). 
* **Matrix/Element**: Matrix has a problem that is inherent in federated networks — terrible [metadata leakage](https://anarc.at/blog/2022-06-17-matrix-notes/#metadata-handling) and [data ownership](https://anarc.at/blog/2022-06-17-matrix-notes/#data-retention-defaults). It has no forward secrecy, the Element client has a large attack surface, and there is a [long list of other issues](https://telegra.ph/why-not-matrix-08-07). What's more, the developers are very friendly with various [national police agencies](https://element.io/blog/bundesmessenger-is-a-milestone-in-germanys-ground-breaking-vision/). 
* **XMPP Clients**: Regardless of the client, an XMPP server will [always be able to see your contact list](https://coy.im/documentation/security-threat-model/). Additionally, server-side parties (e.g., administrators, attackers, law enforcement) can [inject arbitrary messages, modify address books, log passwords in cleartext](https://web.archive.org/web/20211215132539/https://infosec-handbook.eu/articles/xmpp-aitm/) and [act as a man-in-the-middle](https://notes.valdikss.org.ru/jabber.ru-mitm/). 
