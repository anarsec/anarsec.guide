+++
title = "Словарь"
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

> В этом словаре даются определения терминов, которые часто используются в статьях на AnarSec 

### Asynchronous Communication

Unlike [synchronous communication](/glossary/#synchronous-communication), both parties do not need to be online at the same time. This relies on some sort of server to store messages until the message recipients come online. This is the type of messaging that most people are familiar with (email, Signal, etc.).   

### Backdoor

A backdoor in software or hardware allows an unauthorized party to bypass access controls. For example, an undocumented developer account in a router allows the developer of that product to bypass the login form. Third parties can also use backdoors to access software/hardware. Hackers want to create backdoors, as do law enforcement agencies. 

### Brute-force attack

An attacker who “simply” tries every possible key to access a service or decrypt a file is using “brute force.” This process is called a brute-force attack. More powerful computers make brute-force attacks more feasible. Modern cryptographic protocols are designed to force an adversary (who does not have the cryptographic key) to spend (nearly) as much time as it would take to try every possible key to break the code. The parameters of a good protocol are chosen to make this amount of time impractical. 

### Checksums / Fingerprints

Checksums are digital fingerprints: small-sized blocks of data derived from another block of digital data for the purpose of detecting any changes that may have been made. For example, when you download an operating system .iso file, a checksum is listed that looks like: `sha512: 9f923361887ac4b1455bc5ae51c06f2457c6d(continued...)`. You can use [hash functions](https://open.oregonstate.education/defenddissent/chapter/cryptographic-hash/) like SHA512 to create fingerprints. Essentially, this mathematical operation converts the 0's and 1's of the file into a unique "fingerprint". Changing a single 1 or 0 results in a completely different fingerprint. It is often important to know if a file has changed, such as when downloading the image file for an operating system. Fingerprints are often used in cryptography (e.g. in certificates or to verify [public keys](/glossary/#public-key-cryptography) in general). GtkHash is a program that allows you to calculate checksums without using a command line interface.  

### Command Line Interface (CLI)

The "command line" is an all-text alternative to the graphical "point and click" tool that most of us are more familiar with; the Command Line Interface (CLI) allows us to do some things that a Graphical User Interface (GUI) does not. Often, either a GUI or a CLI would work, and which you use is a matter of preference. For example, in [Tails](/glossary/#tails), you can verify the [checksum](/glossary/#checksums-fingerprints) of a file using either a GUI (the GtkHash program) or a CLI command (`sha256sum`). 

For more information, see [Linux Essentials](/posts/linux/#the-command-line-interface). The Tech Learning Collective's "Foundations: Linux Journey" course on the [command line](https://techlearningcollective.com/foundations/linux-journey/the-shell) is our recommended introduction to using the CLI/terminal. 

### Correlation Attack 

An end-to-end correlation attack is a theoretical way that a global adversary could break the anonymity of the [Tor network](/glossary/#tor-network). For more information, see [Protecting against determined, skilled attackers](/posts/tails-best/#2-protecting-against-determined-skilled-attackers) and [Make Correlation Attacks More Difficult](/posts/tails/#make-correlation-attacks-more-difficult). For research papers on the subject, see [Thirteen Years of Tor Attacks](https://github.com/Attacks-on-Tor/Attacks-on-Tor#correlation-attacks) and the design proposal on [information leaks in Tor](https://spec.torproject.org/proposals/344-protocol-info-leaks.html).

### CVE

CVE stands for Common Vulnerabilities and Exposures. It is a globally unique identifier for [security vulnerabilities](/glossary/#vulnerability) in software. Identifiers look like “CVE-YEAR-NUMBER.” The year in the identifier is the year the CVE ID was assigned, not the year the vulnerability was publicly disclosed.

### DDoS Attack

A Distributed Denial of Service (DDoS) attack attempts to overload or crash the services of the target system by sending a large number of requests from many sources. The goal of a DDoS attack is to affect the availability of a service or system, such as making a web server unavailable to web browsers.

### Digital Signatures

Digital signatures are based on [public-key cryptography](/glossary/#public-key-cryptography). A private key is used to digitally sign data, while the corresponding public key is used by third parties to verify the signature. Before a public key is used to verify a signature, its authenticity should be verified.

To learn more, [watch this video](https://www.youtube.com/watch?v=s22eJ1eVLTU&listen=false). For a more detailed look, see [Defend Dissent: Authenticity through Cryptographic Signing](https://open.oregonstate.education/defenddissent/chapter/cryptographic-signing/) or our [GPG explanation](/posts/tails-best/#appendix-gpg-explanation).

### Doxxing

The publication of private information about an individual or organization is called doxxing. Before publication, the person doing the doxing may use public databases, social media, or [social engineering](/glossary/#social-engineering) to obtain information.

### Encryption

Encryption is the process of scrambling a message so that it can only be unscrambled (and read) by the intended parties. The method you use to scramble the original message, or *plaintext*, is called the *cipher* or *encryption protocol*. In almost all cases, the cipher is not intended to be kept secret. The scrambled, unreadable, encrypted message is called the ciphertext and can be safely shared. Most ciphers require an additional piece of information, called a *cryptographic key*, to encrypt and decrypt (scramble and unscramble) messages.

For more information, see [symmetric cryptography](/glossary/#symmetric-cryptography), [asymmetric cryptography](/glossary/#public-key-cryptography), or [Defend Dissent: What is Encryption?](https://open.oregonstate.education/defenddissent/chapter/what-is-encryption/)

### End-to-end encryption (e2ee)

Data is [encrypted](/glossary/#encryption) as it travels from one device to another — endpoint to endpoint —  and cannot be decrypted by any intermediary. It can only be decrypted by the endpoints. This is different from "encryption at rest", such as [Full Disk Encryption](/glossary/#full-disk-encryption-fde), where the data stored on your device is encrypted when the device is turned off. Both are important! 

For more information, check out [Encrypted Messaging for Anarchists](/posts/e2ee), and [Defend Dissent: Protecting Your Communications](https://open.oregonstate.education/defenddissent/chapter/protecting-your-communications/).

### Exploit

An exploit is designed to take advantage of a [vulnerability](/glossary/#vulnerability). Even worse (or better, depending on whether you are the attacker or the target) are [zero-day exploits](/glossary/#zero-day-exploit).

### Forward secrecy

Forward secrecy (FS, also known as “Perfect Forward Secrecy”) combines a system of long-term keys and session keys to protect encrypted communications from future key compromise. An attacker who can record every encrypted message ([man-in-the-middle](/glossary/#man-in-the-middle-attack)) won’t be able to decrypt those messages if the keys are compromised in the future. Modern encryption protocols such as [TLS](/glossary/#https) 1.3 and the Signal Protocol provide FS. For more information, see [Anonymous Planet](https://anonymousplanet.org/guide.html#forward-secrecy). 

### Full Disk Encryption (FDE)

FDE means that the entire disk is [encrypted](/glossary/#encryption) until a password is entered when the device is powered on. Not all FDE is created equal. For example, the quality of how FDE is implemented on a phone depends not only on your operating system, but also on your hardware (the model of your phone). FDE uses [symmetric cryptography](/glossary/#symmetric-cryptography), and on Linux it typically uses the [LUKS specification](/glossary/#luks). 

### GnuPG / OpenPGP

GnuPG (GPG) is a program that implements the OpenPGP (Pretty Good Privacy) standard. GPG provides cryptographic functions for encrypting, decrypting, and signing text and files. It is a classic example of [public-key cryptography](/glossary/#public-key-cryptography). When used with email, [metadata](/glossary/#metadata) (such as email addresses) remains unencrypted. It does not provide [forward secrecy](/glossary/#forward-secrecy).

For more information, see [this primer](https://github.com/AnarchoTechNYC/meta/wiki/Pretty-Good-Privacy-%28PGP%29). We don't recommend it for encrypted communications, [here's why](/posts/e2ee/#pgp-email). 

### Hardening

Hardening is a general term for the process of securing systems against attacks. 

### HTTPS

The "S" in HTTPS stands for "secure"; which means that your Internet connection is encrypted using the [Transport Layer Security (TLS)](https://www.youtube.com/watch?v=0TLDTodL7Lc&listen=false) protocol. This involves the website generating a certificate using [public-key cryptography](/glossary/#public-key-cryptography) that can be used to verify its authenticity — that you are actually connecting to the web server you intended, and that this connection is encrypted.   

For more information, see [our explanation](/posts/tails/#what-is-https) or [Defend Dissent: Protecting Your Communications](https://open.oregonstate.education/defenddissent/chapter/protecting-your-communications/).

### Linux

Linux is an [open-source](/glossary/#open-source) "kernel" upon which operating systems are built. Unlike Windows or macOS, there are many flavors of Linux operating systems. For example, Ubuntu, Kali, and Tails are based on Debian. Manjaro is based on Arch. For more information, see [Linux Essentials](/posts/linux).

### LUKS 

The [Linux Unified Key Setup (LUKS)](https://gitlab.com/cryptsetup/cryptsetup) is a platform-independent specification for disk encryption. It is the standard used in [Tails](/glossary/#tails), [Qubes OS](/glossary/#qubes-os), Ubuntu, etc. LUKS encryption is only effective when the device is powered off. LUKS should use [Argon2id](/posts/tails-best/#passwords) to make it less vulnerable to brute-force attacks. 

### Malware

Malware (malicious software) is a generic term for software that contains unwanted or malicious functionality. Malware includes ransomware, Trojan horses, computer viruses, worms, spyware, scareware, adware, etc. Today, malware is more difficult to categorize because sophisticated malware often combines characteristics of different categories. For example, [WannaCry](https://en.wikipedia.org/wiki/WannaCry_ransomware_attack) spread like a worm, but encrypted files and held them for ransom (ransomware).

### Man-in-the-middle attack

An example of a man-in-the-middle attack is when Alice communicates with Bob over the Internet, Eve (eavesdropper) joins the conversation “in the middle” and becomes the man-in-the-middle. Eve can modify, insert, replay, or read messages at will. Protective measures include encryption (confidentiality) and checking the authenticity and integrity of all messages. However, you must also make sure that you are communicating with the expected party. You must verify that you have the real public key of the recipient. For example, this is what you do when you verify a contract's "Safety Number" in the Signal encrypted messaging app.

For a more detailed look, see [Defend Dissent: The Man in the Middle](https://open.oregonstate.education/defenddissent/chapter/the-man-in-the-middle/) and the [Whonix documentation](https://www.whonix.org/wiki/Warning#Man-in-the-middle_Attacks).

### Metadata

Metadata is data that provides information about other data. For example, a JPG file contains the actual image (data) but it may also contain metadata such as the date the file was created, the type of camera, GPS coordinates, and so on. Metadata can be valuable to attackers (to find appropriate exploits for outdated software the target is using), government agencies (to collect information about people to create social graphs), and other parties (to target location-based advertising). Whenever you use a computer, you are likely leaving metadata behind.

For more information, see [Remove Identifying Metadata From Files](/posts/metadata) and [Defend Dissent: Metadata](https://open.oregonstate.education/defenddissent/chapter/metadata/).

### Open-source

The only software we can trust because the "source code" that it is written in is "open" for anyone to examine.

### Operating system (OS)

The system software that runs your device before any other software. Some common examples include Windows, macOS, Linux, Android, and iOS. Linux and some versions of Android are the only open-source options on this list. 

### Passphrase

A passphrase is similar to a [password](/glossary/#password), but is made up of words instead of random characters. 

### Password

A password is a string of characters used for authentication. A strong password consists of randomly chosen characters that all have the same probability of occurrence and can be created with the KeePassXC Password Generator. 

For more information, see [Defend Dissent: Passwords](https://open.oregonstate.education/defenddissent/chapter/passwords/)

### Phishing

Phishing is a technique of [social engineering](/glossary/#social-engineering). Attackers send SMS messages, emails, chat messages, etc. to their targets to get their personal information. The attackers can then try to impersonate their victims. It can also be used to get the victim to download [malware](/glossary/#malware) onto a system, which can be used as a starting point for hacking. [Spear phishing](/glossary/#spear-phishing) is a more sophisticated form of phishing. For more information, see the [Kicksecure documentation](https://www.kicksecure.com/wiki/Social_Engineering).

### Physical attacks

A physical attack is a situation where an adversary first gains physical access to your device through loss, theft, or confiscation. For example, your phone may be confiscated when you cross a border or are arrested. This is in contrast to a [remote attack](/glossary/#remote-attacks). 

For more information, see [Making Your Electronics Tamper-Evident](/posts/tamper), the [Threat Library](https://notrace.how/threat-library/techniques/targeted-digital-surveillance/physical-access.html), the [KickSecure documentation](https://www.kicksecure.com/wiki/Protection_Against_Physical_Attacks), and [Defend Dissent: Protecting Your Devices](https://open.oregonstate.education/defenddissent/chapter/protecting-your-devices/).

### Plausible deniability

Plausible deniability can be a security objective. It is achieved when you can’t prove that a person/system sent a particular message. Then that person/system can plausibly deny being the sender of the message.

### Public-key cryptography

Public-key cryptography (or asymmetric cryptography) is the opposite of [symmetric cryptography](/glossary/#symmetric-cryptography). Each party has two keys (public and private). The private key must be kept secret and is used for decryption; the public key must be made public, and is used for encryption. This is the model used for encrypted communication, since the public key cannot be used for decryption. All other parties must verify that a published public key belongs to its intended owner to avoid [man-in-the-middle attacks](/glossary/#man-in-the-middle-attack).

There are several approaches to public-key cryptography. For example, some cryptosystems are based on the algebraic structure of elliptic curves over finite fields (ECC). Others are based on the difficulty of factoring the product of two large prime numbers (RSA). Public-key cryptography can also be used for [digital signatures](/glossary/#digital-signatures). 

To learn more, watch [this video](https://www.youtube.com/watch?v=GSIDS_lvRv4), or for a more detailed look, see [Defend Dissent: Public-Key Cryptography](https://open.oregonstate.education/defenddissent/chapter/public-key-cryptography/).

### Qubes OS

You can think of [Qubes OS](https://www.qubes-os.org/) as Linux + [virtual machines](/glossary/#virtual-machine-vm). We [recommend](/recommendations) it as an everyday operating system for intermediate Linux users. 

### Remote attacks

By remote attack, we mean that an adversary would access the data on your phone or laptop through an Internet or data connection. There are companies that develop and sell the ability to infect your device (usually focusing on smartphones) with [malware](/glossary/#malware) that would allow their customer (your adversary, be it a corporate or state agent) to remotely access some or all of your information. This is in contrast to a [physical attack](/glossary/#physical-attacks).

For a more detailed look, see [Defend Dissent: Protecting Your Devices](https://open.oregonstate.education/defenddissent/chapter/protecting-your-devices/).

### Sandboxing

Sandboxing is the software-based isolation of applications to mitigate system failures or vulnerabilities. For example, if an attacker hacks an application that is "sandboxed", the attacker must escape the sandbox to hack the entire system. [Virtualization](/glossary/#virtualization) is the most powerful implementation of sandboxing. 

### Security goal

Security goals are concepts in information security that define what needs to be achieved. Well-known security goals are confidentiality, integrity, and availability (known as the CIA triad).

### Social engineering

Social engineering is a general term for the psychological manipulation of people to perform actions. Social engineering doesn't depend on technology; it's quite common in everyday life. For example, children cry to manipulate their parents; commercials manipulate their viewers. In information security, [phishing](/glossary/#phishing) is a common social engineering technique.

### Spear phishing

Spear phishing is more sophisticated than regular [phishing](/glossary/#phishing), which casts a wide net. In spear phishing, attackers customize their forged messages and send them to a smaller number of potential victims. Spear phishing requires more research on the part of the attacker; however, the success rate of spear phishing attacks is higher than the success rate of regular phishing attacks.

### Supply-chain attack

A supply-chain attack can affect any user of hardware or software components. Attackers manipulate a component during the manufacturing process. In most cases, the actual attack occurs before the targeted user has the manipulated component. Examples include tampered compilers or firmware, and attacks such as [Stuxnet](https://en.wikipedia.org/wiki/Stuxnet) or [SolarWinds](https://en.wikipedia.org/wiki/2020_SolarWinds_watering_hole_attack).

### Symmetric cryptography

Symmetric cryptography is the opposite of [public-key cryptography](/glossary/#public-key-cryptography). Two parties need the same private key to communicate with each other. They both use this key to encrypt and decrypt data. Symmetric cryptography is faster than public-key cryptography, but you must exchange keys securely. AES is a well-known example of symmetric cryptography. This is the model used for [Full Disk Encryption](/glossary/#full-disk-encryption-fde) (e.g. used by [LUKS](/glossary/#luks) in Linux Full Disk Encryption). 

### Synchronous communication

Unlike [asynchronous communication](/glossary/#asynchronous-communication), both parties must be online at the same time. This does not require servers for the communication and is often referred to as "peer to peer". 

### Tails

Tails is an operating system that makes secure and anonymous computer use accessible to everyone. Tails runs from a USB drive and is designed to leave no trace of your activity on your computer unless you explicitly want it to.

Tails uses the [Tor anonymity network](/glossary/#tor-network) to protect your privacy online; all software is configured to connect to the Internet through Tor. If an application tries to connect to the Internet directly, it is automatically blocked for security reasons.

For more information, see [Tails for Anarchists](/posts/tails).

### Threat model

Threat modeling is a family of activities for improving security by identifying a set of adversaries, [security goals](/glossary/#security-goal), and [vulnerabilities](/glossary/#vulnerability), and then defining countermeasures to prevent or mitigate the effects of threats to the system. A threat is a potential or actual undesirable event that can be malicious (such as a [DDoS attack](/glossary/#ddos-attack)) or accidental (such as a hard drive failure). Threat modeling is the deliberate activity of identifying and assessing threats and vulnerabilities.

For more information, see [the No Trace Project Threat Library](https://notrace.how/threat-library/), [Defend Dissent: Digital Threats to Social Movements](https://open.oregonstate.education/defenddissent/chapter/digital-threats/) and [Defending against Surveillance and Suppression](https://open.oregonstate.education/defenddissent/chapter/surveillance-and-suppression/).

### Tor network  

[Tor](https://www.torproject.org/) (short for The Onion Router) is an open and distributed network that helps defend against traffic analysis. Tor protects you by routing your communications through a network of relays run by volunteers around the world: it prevents someone monitoring your Internet connection from learning what sites you visit, and it prevents the operators of the sites you visit from learning your physical location.

Every website visited through the Tor network passes through 3 relays. Relays are servers hosted by different people and organizations around the world. No single relay ever knows both where the encrypted connection is coming from and where it is going. An excerpt from a leaked top-secret NSA assessment calls Tor "the King of high secure, low latency Internet anonymity" with "no contenders for the throne in waiting". The Tor network can be accessed through the Tor Browser on any operating system. The [Tails](/glossary/#tails) operating system forces every program to use the Tor network when accessing the Internet. 

For more information, see [Tails for Anarchists](/posts/tails/#tor) and [Privacy Guides](https://www.privacyguides.org/en/advanced/tor-overview/). To understand the limitations of Tor, see the [Whonix documentation](https://www.whonix.org/wiki/Warning). 

### Two-Factor Authentication (2FA)

Two-factor authentication (or “2FA”) is a way for a user to identify themselves to a service provider by requiring a combination of two different authentication methods. These can be something the user knows (such as a password or PIN) or something the user has (such as a hardware token or mobile phone). 

### Virtualization

Virtualization is a technology that creates a virtual version of something, including virtual computer hardware. A [Virtual Machine](/glossary/#virtual-machine-vm) takes advantage of this technology.

### Virtual Machine (VM)

A virtual machine is a [virtualization](/glossary/#virtualization)/emulation of a computer system. Virtual machines are based on computer architectures and provide the functionality of a physical computer. This can provide the security benefit of [sandboxing](/glossary/#sandboxing). [Qubes OS](/glossary/#qubes-os) consists of VMs that [run directly on the hardware](https://www.qubes-os.org/faq/#how-does-qubes-os-compare-to-running-vms-in-a-conventional-os) (referred to as "bare metal"). According to the Qubes project, "virtualization is currently the only practically viable approach to implementing strong isolation while simultaneously providing compatibility with existing applications and drivers."

### VoIP  (Voice over Internet Protocol)

Google Voice is a well-known and insecure VoIP service; this technology routes your calls over the Internet (as Signal does) instead of using standard cell tower transmission. Unlike Signal, VoIP allows you to receive calls from anyone, not just other Signal users. The advantage of using VoIP for calls over a data plan is that you can create different numbers for different activities (one for bills, one for signing up for a Signal account, etc.), and you never need to turn off Airplane mode. The advantage of using a data plan instead is that you can use it away from Wi-Fi, at the cost of geolocation (i.e. it will be possible for your service provider and possibly other parties to know where your device is at any given time). 

### VPN (Virtual Private Network)

A VPN extends a private network (like your home network) over a public network (like the Internet). Devices connected to the VPN are part of the private network, even if they are physically located elsewhere. Applications that use a VPN are subject to the functionality, security, and management of the private network. 

In other words, it is a technology that essentially makes it appear that you are connecting to the Internet from the network of the company providing the service, rather than from your home network. Your connection to the company is through an encrypted "tunnel". A VPN is not the best tool for anonymity (defined as knowing who you are — Tor is far better), but it can partially enhance your privacy (defined as knowing what you are doing). 

It is important to emphasize this to cut through the widespread marketing hype; [a VPN is not enough to keep you anonymous](https://www.ivpn.net/privacy-guides/will-a-vpn-protect-me/). Using a VPN can be thought of as simply shifting your trust from a local Internet Service Provider which is guaranteed to be a snitch to a remote company that claims to limit its ability to effectively snitch on you.  

For more information, see [Privacy Guides](https://www.privacyguides.org/en/basics/vpn-overview/), and for an excellent comparison of a VPN and [Tor](/glossary/#tor-network), see [Defend Dissent: Anonymous Routing](https://open.oregonstate.education/defenddissent/chapter/anonymous-routing/).

### Vulnerability

Vulnerabilities are [exploitable](/glossary/#exploit) security flaws in software or hardware. Well-known vulnerabilities have names like Heartbleed, Shellshock, Spectre, or Stagefright and at least one [CVE](/glossary/#cve) identifier. Vulnerabilities don't always have exploits. A popular vulnerability severity rating system is [CVSS](https://en.wikipedia.org/wiki/Common_Vulnerability_Scoring_System). 

### Zero-day exploit

A zero-day [exploit](/glossary/#exploit) is unknown to the public, the vendor, or other parties that would normally mitigate it. As a result, it is extremely powerful and highly valued. Governments can either develop their own zero-day exploits or purchase them from a [zero-day broker](https://www.wired.com/story/untold-history-americas-zero-day-market/). 

</div>
