+++
title = "Glossaire"
sort_by = "date"
paginate_by = 5
+++
<br>
<div class="column is-8 is-offset-2">

> Ce glossaire contient des termes couramment utilisés dans les articles d'AnarSec. 

### Authentification à deux facteurs (2FA)

L'authentification à deux facteurs (ou "2FA") est une manière pour un utilisateur de s'authentifier auprès d'un fournisseur de services qui impose la combinaison de deux méthodes d'authentification différentes. Ces méthodes peuvent être quelque chose que l'utilisateur sait (comme un mot de passe ou un code PIN) ou que l'utilisateur a (comme une clé matérielle ou un téléphone portable).

### Chiffrement de bout en bout

Les données sont [chiffrées](/fr/glossary/#encryption) lors de leur trajet d'un appareil à un autre — de bout en bout — et ne peuvent être déchiffrées par aucun intermédiaire. Elles ne peuvent être déchiffrées qu'au niveau des deux bouts. Cela diffère du "chiffrement au repos" tel que [chiffrement complet du disque](/fr/glossary/#full-disk-encryption-fde), où les données stockées sur ton appareil sont chiffrées lorsque l'appareil est éteint. Les deux sont importants !

Pour plus d'informations, voir [Encrypted Messaging for Anarchists](/posts/e2ee/), et [Defend Dissent: Protecting Your Communications](https://open.oregonstate.education/defenddissent/chapter/protecting-your-communications) (Défendre la contestation : Protéger nos communications).

### Communication asynchrone

Au contraire de la [communication synchrone](/fr/glossary/#communication-synchrone), les deux participant·e·s n'ont pas besoin d'être connecté·e·s au même moment. Cela grâce à un serveur qui stocke les messages jusqu'à ce que les destinataires des messages se connectent. C'est le type de communication le plus courant (email, etc.)

### Communication synchrone

Contrairement à la [communication asynchrone](/fr/glossary/#communication-asynchrone), les deux participant·e·s doivent être connecté·e·s au même moment. Il n'y a pas besoin de serveurs pour la communication, on dit souvent que c'est "de pair à pair" (*peer to peer*).

### Métadonnées

Les métadonnées sont des données qui donnent des informations sur d'autres données. Par exemple, un fichier JPG contient l'image en elle-même (données) mais peut aussi contenir des métadonnées comme la date à laquelle le fichier a été créé, le type d'appareil photo, des coordonnées GPS, etc. Les métadonnées peuvent être précieuses pour le piratage (pour trouver des failles dans des logiciels obsolètes utilisés par une cible), pour des agences gouvernementales (pour récolter des informations sur des gens et les relations entre elleux), et d'autres acteurs (pour de la publicité ciblée). Quand tu utilises un ordinateur, tu laisses généralement des métadonnées derrière toi.

Pour plus d'informations, voir [Remove Identifying Metadata From Files](/posts/metadata) et [Defend Dissent: Metadata](https://open.oregonstate.education/defenddissent/chapter/metadata/) (Défendre la contestation : Métadonnées).

### Modèle de menaces

La modélisation de menaces est un ensemble d'activités pour améliorer sa sécurité en identifiant un ensemble d'adversaires, d'[objectifs de sécurité](/fr/glossary/#security-goal), et de [vulnérabilités](/fr/glossary/#vulnerability), puis en définissant des contre-mesures pour prévenir ou remédier aux effets des menaces envers un système. Une menace est un évènement possiblement ou assurément non désiré qui peut être malveillant (comme une [attaque par déni de service](/fr/glossary/#ddos-attack)) ou accidentel (comme un disque dur qui meurt). La modélisation de menaces est l'activité délibérée d'identification et d'évaluation des menaces et vulnérabilités.

Pour plus d'informations, voir la [Bibliothèque de menaces du No Trace Project](https://notrace.how/threat-library/fr), [Defend Dissent: Digital Threats to Social Movements](https://open.oregonstate.education/defenddissent/chapter/digital-threats/) (Défendre la contestation : Menaces numériques contre les mouvements sociaux) et [Defending against Surveillance and Suppression](https://open.oregonstate.education/defenddissent/chapter/surveillance-and-suppression/) (Se défendre contre la surveillance et la répression).

### Réseau Tor

[Tor](https://www.torproject.org/) (acronyme pour The Onion Router) est un réseau ouvert et décentralisé qui aide à se protéger des analyses de traffic réseau. Tor te protège en faisant passer tes communications à travers un réseau de relais maintenus par des volontaires à travers le monde : cela empêche une personne qui surveillerait ta connexion Internet de savoir quels sites web tu visites, et cela empêche les administrateurs des sites que tu visites de découvrir ta position géographique.

Chaque visite d'un site web via Tor fait passer par trois relais. Les relais sont des serveurs hébergés par différentes personnes et organisations à travers le monde. Un relai donné ne sait jamais à la fois d'où vient la communication chiffrée ni où elle va. Un extrait d'une évaluation top-secrète de la NSA désigne Tor comme "le roi de l'anonymat sécurisé sur Internet pour des usages en temps réel", "sans concurrents au trône en vue". On peut accéder au réseau Tor sur n'importe quel système d'exploitation grâce au Navigateur Tor. Le système d'exploitation [Tails](/fr/glossary/#tails) force tous les logiciels à passer par le réseau Tor pour accéder à Internet.

Pour plus d'informations, voir [Tails for Anarchists](/posts/tails/#tor) et [Privacy Guides](https://www.privacyguides.org/fr/advanced/tor-overview/). Pour comprendre les limites de Tor, voir la [documentation de Whonix](https://www.whonix.org/wiki/Warning).

### Système d'exploitation (OS)

Le logiciel système qui s'exécute sur ton appareil avant tout autre logiciel. Par exemple Windows, macOS, Linux, Android, et iOS. Linux et certaines versions d'Android sont les seules options open-source de cette liste.

### Voix sur IP (VoIP, Voice over Internet Protocol)

Google Voice est un service de voix sur IP bien connu et peu sécurisé ; cette technologie fait passer tes appels par Internet (tout comme Signal par exemple) au lieu d'utiliser la transmission classique par les antennes téléphoniques. Contrairement à Signal, la voix sur IP te permet de recevoir des appels de n'importe qui, pas juste d'autres utilisateurs de Signal. L'avantage d'utiliser la voix sur IP pour des appels par rapport à un abonnement téléphonique est que tu peux créer des numéros différents pour différentes activités (un pour les factures, un autre pour créer un compte Signal, etc.) et que tu n'as jamais besoin de désactiver le mode avion. L'avantage d'un abonnement téléphonique est que tu peux l'utiliser loin d'un accès au Wi-Fi, au prix d'être géolocalisé (c'est-à-dire qu'il est possible pour ton opérateur téléphonique et possiblement d'autres acteurs de savoir à tout moment où est ton appareil).

### Backdoor

A backdoor in software or hardware allows an unauthorized party to bypass access controls. For example, an undocumented developer account in a router allows the developer of that product to bypass the login form. Third parties can also use backdoors to access software/hardware. Hackers want to create backdoors, as do law enforcement agencies. 

### Brute-force attack

An attacker who “simply” tries every possible key to access a service or decrypt a file is using “brute force.” This process is called a brute-force attack. More powerful computers make brute-force attacks more feasible. Modern cryptographic protocols are designed to force an adversary (who does not have the cryptographic key) to spend (nearly) as much time as it would take to try every possible key to break the code. The parameters of a good protocol are chosen to make this amount of time impractical. 

### Checksums / Fingerprints

Checksums are digital fingerprints: small-sized blocks of data derived from another block of digital data for the purpose of detecting any changes that may have been made. For example, when you download an operating system .iso file, a checksum is listed that looks like: `sha512: 9f923361887ac4b1455bc5ae51c06f2457c6d(continued...)`. You can use [hash functions](https://open.oregonstate.education/defenddissent/chapter/cryptographic-hash/) like SHA512 to create fingerprints. Essentially, this mathematical operation converts the 0's and 1's of the file into a unique "fingerprint". Changing a single 1 or 0 results in a completely different fingerprint. It is often important to know if a file has changed, such as when downloading the image file for an operating system. Fingerprints are often used in cryptography (e.g. in certificates or to verify [public keys](/fr/glossary/#public-key-cryptography) in general). GtkHash is a program that allows you to calculate checksums without using a command line interface.  

### Command Line Interface (CLI)

The "command line" is an all-text alternative to the graphical "point and click" tool that most of us are more familiar with; the Command Line Interface (CLI) allows us to do some things that a Graphical User Interface (GUI) does not. Often, either a GUI or a CLI would work, and which you use is a matter of preference. For example, in [Tails](/fr/glossary/#tails), you can verify the [checksum](/fr/glossary/#checksums-fingerprints) of a file using either a GUI (the GtkHash program) or a CLI command (`sha256sum`). 

For more information, see [Linux Essentials](/posts/linux/#the-command-line-interface). The Tech Learning Collective's "Foundations: Linux Journey" course on the [command line](https://techlearningcollective.com/foundations/linux-journey/the-shell) is our recommended introduction to using the CLI/terminal. 

### Correlation Attack 

An end-to-end correlation attack is a theoretical way that a global adversary could break the anonymity of the [Tor network](/fr/glossary/#tor-network). For more information, see [Protecting against determined, skilled attackers](/posts/tails-best/#2-protecting-against-determined-skilled-attackers) and [Make Correlation Attacks More Difficult](/posts/tails/#make-correlation-attacks-more-difficult). For research papers on the subject, see [Thirteen Years of Tor Attacks](https://github.com/Attacks-on-Tor/Attacks-on-Tor#correlation-attacks) and the design proposal on [information leaks in Tor](https://spec.torproject.org/proposals/344-protocol-info-leaks.html).

### CVE

CVE stands for Common Vulnerabilities and Exposures. It is a globally unique identifier for [security vulnerabilities](/fr/glossary/#vulnerability) in software. Identifiers look like “CVE-YEAR-NUMBER.” The year in the identifier is the year the CVE ID was assigned, not the year the vulnerability was publicly disclosed.

### DDoS Attack

A Distributed Denial of Service (DDoS) attack attempts to overload or crash the services of the target system by sending a large number of requests from many sources. The goal of a DDoS attack is to affect the availability of a service or system, such as making a web server unavailable to web browsers.

### Digital Signatures

Digital signatures are based on [public-key cryptography](/fr/glossary/#public-key-cryptography). A private key is used to digitally sign data, while the corresponding public key is used by third parties to verify the signature. Before a public key is used to verify a signature, its authenticity should be verified.

To learn more, [watch this video](https://www.youtube.com/watch?v=s22eJ1eVLTU&listen=false). For a more detailed look, see [Defend Dissent: Authenticity through Cryptographic Signing](https://open.oregonstate.education/defenddissent/chapter/cryptographic-signing/) or our [GPG explanation](/posts/tails-best/#appendix-gpg-explanation).

### Doxxing

The publication of private information about an individual or organization is called doxxing. Before publication, the person doing the doxing may use public databases, social media, or [social engineering](/fr/glossary/#social-engineering) to obtain information.

### Encryption

Encryption is the process of scrambling a message so that it can only be unscrambled (and read) by the intended parties. The method you use to scramble the original message, or *plaintext*, is called the *cipher* or *encryption protocol*. In almost all cases, the cipher is not intended to be kept secret. The scrambled, unreadable, encrypted message is called the ciphertext and can be safely shared. Most ciphers require an additional piece of information, called a *cryptographic key*, to encrypt and decrypt (scramble and unscramble) messages.

For more information, see [symmetric cryptography](/fr/glossary/#symmetric-cryptography), [asymmetric cryptography](/fr/glossary/#public-key-cryptography), or [Defend Dissent: What is Encryption?](https://open.oregonstate.education/defenddissent/chapter/what-is-encryption/)

### Exploit

An exploit is designed to take advantage of a [vulnerability](/fr/glossary/#vulnerability). Even worse (or better, depending on whether you are the attacker or the target) are [zero-day exploits](/fr/glossary/#zero-day-exploit).

### Forward secrecy

Forward secrecy (FS, also known as “Perfect Forward Secrecy”) combines a system of long-term keys and session keys to protect encrypted communications from future key compromise. An attacker who can record every encrypted message ([man-in-the-middle](/fr/glossary/#man-in-the-middle-attack)) won’t be able to decrypt those messages if the keys are compromised in the future. Modern encryption protocols such as [TLS](/fr/glossary/#https) 1.3 and the Signal Protocol provide FS. For more information, see [Anonymous Planet](https://anonymousplanet.org/guide.html#forward-secrecy). 

### Full Disk Encryption (FDE)

FDE means that the entire disk is [encrypted](/fr/glossary/#encryption) until a password is entered when the device is powered on. Not all FDE is created equal. For example, the quality of how FDE is implemented on a phone depends not only on your operating system, but also on your hardware (the model of your phone). FDE uses [symmetric cryptography](/fr/glossary/#symmetric-cryptography), and on Linux it typically uses the [LUKS specification](/fr/glossary/#luks). 

### GnuPG / OpenPGP

GnuPG (GPG) is a program that implements the OpenPGP (Pretty Good Privacy) standard. GPG provides cryptographic functions for encrypting, decrypting, and signing text and files. It is a classic example of [public-key cryptography](/fr/glossary/#public-key-cryptography). When used with email, [metadata](/fr/glossary/#metadata) (such as email addresses) remains unencrypted. It does not provide [forward secrecy](/fr/glossary/#forward-secrecy).

For more information, see [this primer](https://github.com/AnarchoTechNYC/meta/wiki/Pretty-Good-Privacy-%28PGP%29). We don't recommend it for encrypted communications, [here's why](/posts/e2ee/#pgp-email). 

### Hardening

Hardening is a general term for the process of securing systems against attacks. 

### HTTPS

The "S" in HTTPS stands for "secure"; which means that your Internet connection is encrypted using the [Transport Layer Security (TLS)](https://www.youtube.com/watch?v=0TLDTodL7Lc&listen=false) protocol. This involves the website generating a certificate using [public-key cryptography](/fr/glossary/#public-key-cryptography) that can be used to verify its authenticity — that you are actually connecting to the web server you intended, and that this connection is encrypted.   

For more information, see [our explanation](/posts/tails/#what-is-https) or [Defend Dissent: Protecting Your Communications](https://open.oregonstate.education/defenddissent/chapter/protecting-your-communications/).

### Linux

Linux is an [open-source](/fr/glossary/#open-source) "kernel" upon which operating systems are built. Unlike Windows or macOS, there are many flavors of Linux operating systems. For example, Ubuntu, Kali, and Tails are based on Debian. Manjaro is based on Arch. For more information, see [Linux Essentials](/posts/linux).

### LUKS 

The [Linux Unified Key Setup (LUKS)](https://gitlab.com/cryptsetup/cryptsetup) is a platform-independent specification for disk encryption. It is the standard used in [Tails](/fr/glossary/#tails), [Qubes OS](/fr/glossary/#qubes-os), Ubuntu, etc. LUKS encryption is only effective when the device is powered off. LUKS should use [Argon2id](/posts/tails-best/#passwords) to make it less vulnerable to brute-force attacks. 

### Malware

Malware (malicious software) is a generic term for software that contains unwanted or malicious functionality. Malware includes ransomware, Trojan horses, computer viruses, worms, spyware, scareware, adware, etc. Today, malware is more difficult to categorize because sophisticated malware often combines characteristics of different categories. For example, [WannaCry](https://en.wikipedia.org/wiki/WannaCry_ransomware_attack) spread like a worm, but encrypted files and held them for ransom (ransomware).

### Man-in-the-middle attack

An example of a man-in-the-middle attack is when Alice communicates with Bob over the Internet, Eve (eavesdropper) joins the conversation “in the middle” and becomes the man-in-the-middle. Eve can modify, insert, replay, or read messages at will. Protective measures include encryption (confidentiality) and checking the authenticity and integrity of all messages. However, you must also make sure that you are communicating with the expected party. You must verify that you have the real public key of the recipient. For example, this is what you do when you verify a contract's "Safety Number" in the Signal encrypted messaging app.

For a more detailed look, see [Defend Dissent: The Man in the Middle](https://open.oregonstate.education/defenddissent/chapter/the-man-in-the-middle/) and the [Whonix documentation](https://www.whonix.org/wiki/Warning#Man-in-the-middle_Attacks).

### Open-source

The only software we can trust because the "source code" that it is written in is "open" for anyone to examine.

### Passphrase

A passphrase is similar to a [password](/fr/glossary/#password), but is made up of words instead of random characters. 

### Password

A password is a string of characters used for authentication. A strong password consists of randomly chosen characters that all have the same probability of occurrence and can be created with the KeePassXC Password Generator. 

For more information, see [Defend Dissent: Passwords](https://open.oregonstate.education/defenddissent/chapter/passwords/)

### Phishing

Phishing is a technique of [social engineering](/fr/glossary/#social-engineering). Attackers send SMS messages, emails, chat messages, etc. to their targets to get their personal information. The attackers can then try to impersonate their victims. It can also be used to get the victim to download [malware](/fr/glossary/#malware) onto a system, which can be used as a starting point for hacking. [Spear phishing](/fr/glossary/#spear-phishing) is a more sophisticated form of phishing. For more information, see the [Kicksecure documentation](https://www.kicksecure.com/wiki/Social_Engineering).

### Physical attacks

A physical attack is a situation where an adversary first gains physical access to your device through loss, theft, or confiscation. For example, your phone may be confiscated when you cross a border or are arrested. This is in contrast to a [remote attack](/fr/glossary/#remote-attacks). 

For more information, see [Making Your Electronics Tamper-Evident](/posts/tamper), the [Threat Library](https://notrace.how/threat-library/techniques/targeted-digital-surveillance/physical-access.html), the [KickSecure documentation](https://www.kicksecure.com/wiki/Protection_Against_Physical_Attacks), and [Defend Dissent: Protecting Your Devices](https://open.oregonstate.education/defenddissent/chapter/protecting-your-devices/).

### Plausible deniability

Plausible deniability can be a security objective. It is achieved when you can’t prove that a person/system sent a particular message. Then that person/system can plausibly deny being the sender of the message.

### Public-key cryptography

Public-key cryptography (or asymmetric cryptography) is the opposite of [symmetric cryptography](/fr/glossary/#symmetric-cryptography). Each party has two keys (public and private). The private key must be kept secret and is used for decryption; the public key must be made public, and is used for encryption. This is the model used for encrypted communication, since the public key cannot be used for decryption. All other parties must verify that a published public key belongs to its intended owner to avoid [man-in-the-middle attacks](/fr/glossary/#man-in-the-middle-attack).

There are several approaches to public-key cryptography. For example, some cryptosystems are based on the algebraic structure of elliptic curves over finite fields (ECC). Others are based on the difficulty of factoring the product of two large prime numbers (RSA). Public-key cryptography can also be used for [digital signatures](/fr/glossary/#digital-signatures). 

To learn more, watch [this video](https://www.youtube.com/watch?v=GSIDS_lvRv4), or for a more detailed look, see [Defend Dissent: Public-Key Cryptography](https://open.oregonstate.education/defenddissent/chapter/public-key-cryptography/).

### Qubes OS

You can think of [Qubes OS](https://www.qubes-os.org/) as Linux + [virtual machines](/fr/glossary/#virtual-machine-vm). We [recommend](/recommendations) it as an everyday operating system for intermediate Linux users. 

### Remote attacks

By remote attack, we mean that an adversary would access the data on your phone or laptop through an Internet or data connection. There are companies that develop and sell the ability to infect your device (usually focusing on smartphones) with [malware](/fr/glossary/#malware) that would allow their customer (your adversary, be it a corporate or state agent) to remotely access some or all of your information. This is in contrast to a [physical attack](/fr/glossary/#physical-attacks).

For a more detailed look, see [Defend Dissent: Protecting Your Devices](https://open.oregonstate.education/defenddissent/chapter/protecting-your-devices/).

### Sandboxing

Sandboxing is the software-based isolation of applications to mitigate system failures or vulnerabilities. For example, if an attacker hacks an application that is "sandboxed", the attacker must escape the sandbox to hack the entire system. [Virtualization](/fr/glossary/#virtualization) is the most powerful implementation of sandboxing. 

### Security goal

Security goals are concepts in information security that define what needs to be achieved. Well-known security goals are confidentiality, integrity, and availability (known as the CIA triad).

### Social engineering

Social engineering is a general term for the psychological manipulation of people to perform actions. Social engineering doesn't depend on technology; it's quite common in everyday life. For example, children cry to manipulate their parents; commercials manipulate their viewers. In information security, [phishing](/fr/glossary/#phishing) is a common social engineering technique.

### Spear phishing

Spear phishing is more sophisticated than regular [phishing](/fr/glossary/#phishing), which casts a wide net. In spear phishing, attackers customize their forged messages and send them to a smaller number of potential victims. Spear phishing requires more research on the part of the attacker; however, the success rate of spear phishing attacks is higher than the success rate of regular phishing attacks.

### Supply-chain attack

A supply-chain attack can affect any user of hardware or software components. Attackers manipulate a component during the manufacturing process. In most cases, the actual attack occurs before the targeted user has the manipulated component. Examples include tampered compilers or firmware, and attacks such as [Stuxnet](https://en.wikipedia.org/wiki/Stuxnet) or [SolarWinds](https://en.wikipedia.org/wiki/2020_SolarWinds_watering_hole_attack).

### Symmetric cryptography

Symmetric cryptography is the opposite of [public-key cryptography](/fr/glossary/#public-key-cryptography). Two parties need the same private key to communicate with each other. They both use this key to encrypt and decrypt data. Symmetric cryptography is faster than public-key cryptography, but you must exchange keys securely. AES is a well-known example of symmetric cryptography. This is the model used for [Full Disk Encryption](/fr/glossary/#full-disk-encryption-fde) (e.g. used by [LUKS](/fr/glossary/#luks) in Linux Full Disk Encryption). 

### Tails

Tails is an operating system that makes secure and anonymous computer use accessible to everyone. Tails runs from a USB drive and is designed to leave no trace of your activity on your computer unless you explicitly want it to.

Tails uses the [Tor anonymity network](/fr/glossary/#tor-network) to protect your privacy online; all software is configured to connect to the Internet through Tor. If an application tries to connect to the Internet directly, it is automatically blocked for security reasons.

For more information, see [Tails for Anarchists](/posts/tails).

### Virtualization

Virtualization is a technology that creates a virtual version of something, including virtual computer hardware. A [Virtual Machine](/fr/glossary/#virtual-machine-vm) takes advantage of this technology.

### Virtual Machine (VM)

A virtual machine is a [virtualization](/fr/glossary/#virtualization)/emulation of a computer system. Virtual machines are based on computer architectures and provide the functionality of a physical computer. This can provide the security benefit of [sandboxing](/fr/glossary/#sandboxing). [Qubes OS](/fr/glossary/#qubes-os) consists of VMs that [run directly on the hardware](https://www.qubes-os.org/faq/#how-does-qubes-os-compare-to-running-vms-in-a-conventional-os) (referred to as "bare metal"). According to the Qubes project, "virtualization is currently the only practically viable approach to implementing strong isolation while simultaneously providing compatibility with existing applications and drivers."

### VPN (Virtual Private Network)

A VPN extends a private network (like your home network) over a public network (like the Internet). Devices connected to the VPN are part of the private network, even if they are physically located elsewhere. Applications that use a VPN are subject to the functionality, security, and management of the private network. 

In other words, it is a technology that essentially makes it appear that you are connecting to the Internet from the network of the company providing the service, rather than from your home network. Your connection to the company is through an encrypted "tunnel". A VPN is not the best tool for anonymity (defined as knowing who you are — Tor is far better), but it can partially enhance your privacy (defined as knowing what you are doing). 

It is important to emphasize this to cut through the widespread marketing hype; [a VPN is not enough to keep you anonymous](https://www.ivpn.net/privacy-guides/will-a-vpn-protect-me/). Using a VPN can be thought of as simply shifting your trust from a local Internet Service Provider which is guaranteed to be a snitch to a remote company that claims to limit its ability to effectively snitch on you.  

For more information, see [Privacy Guides](https://www.privacyguides.org/en/basics/vpn-overview/), and for an excellent comparison of a VPN and [Tor](/fr/glossary/#tor-network), see [Defend Dissent: Anonymous Routing](https://open.oregonstate.education/defenddissent/chapter/anonymous-routing/).

### Vulnerability

Vulnerabilities are [exploitable](/fr/glossary/#exploit) security flaws in software or hardware. Well-known vulnerabilities have names like Heartbleed, Shellshock, Spectre, or Stagefright and at least one [CVE](/fr/glossary/#cve) identifier. Vulnerabilities don't always have exploits. A popular vulnerability severity rating system is [CVSS](https://en.wikipedia.org/wiki/Common_Vulnerability_Scoring_System). 

### Zero-day exploit

A zero-day [exploit](/fr/glossary/#exploit) is unknown to the public, the vendor, or other parties that would normally mitigate it. As a result, it is extremely powerful and highly valued. Governments can either develop their own zero-day exploits or purchase them from a [zero-day broker](https://www.wired.com/story/untold-history-americas-zero-day-market/). 

</div>
