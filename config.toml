base_url = "https://www.anarsec.guide"
title = "AnarSec"
default_language = "en"
theme = "DeepThought"
generate_feed = true 

taxonomies = [
    { name = "categories", feed = true, paginate_by = 10 },
    { name = "tags", feed = true, paginate_by = 10},
]

[markdown]
highlight_code = true
highlight_theme = "one-dark"
smart_punctuation = false 

[link_checker]
internal_level = "error"

[translations]
language_name = "en"
language_selection = "Language"
search = "Search"
published = "Published on"
edited = "Last edited on"
letter = "Letter booklet"
a4 = "A4 booklet"
read = "Read More"
contents = "Contents"
categories = "Categories"
defensive = "Defensive"
offensive = "Offensive"
tags = "Tags"
beginner = "beginner"
e2ee = "e2ee"
intermediate = "intermediate"
intro = "intro"
linux = "linux"
metadata = "metadata"
mobile = "mobile"
opsec = "opsec"
qubes = "qubes"
tails = "tails"
windows = "windows"

[languages.fr]
generate_feed = true
taxonomies = [
	{name = "categories"},
	{name = "tags"},
]
title = "AnarSec"

[languages.fr.translations]
language_name = "fr"
language_selection = "Langue"
search = "Recherche"
published = "Publié le"
edited = "Modifié le"
letter = "Brochure Letter"
a4 = "Brochure A4"
read = "Lire la suite"
contents = "Sommaire"
categories = "Catégories"
defensive = "Défensif"
offensive = "Offensif"
tags = "Tags"
mobile = "téléphonie"

[languages.el]
generate_feed = true
taxonomies = [
	{name = "categories"},
	{name = "tags"},
]
title = "AnarSec"

[languages.el.translations]
language_name = "el"
language_selection = "Γλώσσα"
search = "Αναζήτηση"
published = "Δημοσιεύθηκε στις"
edited = "Τελευταία τροποποίηση στις"
letter = "Letter booklet"
a4 = "A4 booklet"
read = "Διάβασε περισσότερα"
contents = "Περιεχόμενα"
categories = "Κατηγορίες"
defensive = "Άμυνα"
offensive = "Επίθεση"
tags = "Ετικέτες"
beginner = "Αρχάριο επίπεδο γνώσης"
e2ee = "Κρυπτογράφηση"
intermediate = "Ενδιάμεσο επίπεδο γνώσης"
intro = "Εισαγωγή"
linux = "Linux"
metadata = "Μεταδεδομένα"
mobile = "Κινητό"
opsec = "Επιχειρησιακή ασφάλεια"
qubes = "Qubes"
tails = "Tails"
windows = "Windows"

[languages.pt]
generate_feed = true
taxonomies = [
	{name = "categories"},
	{name = "tags"},
]
title = "AnarSec"

[languages.pt.translations]
language_name = "pt"
language_selection = "Idioma"
search = "Busca"
published = "Publicado em"
edited = "Editado pela última vez em"
letter = "Para leitura"
a4 = "Para Impressão"
read = "Leia Mais"
contents = "Tópicos"
categories = "Categorias"
defensive = "Defensivo"
offensive = "Ofensiva"
tags = "Tags"
beginner = "Iniciante"
e2ee = "Criptografia"
intermediate = "Intermediário"
intro = "Introdução"
linux = "Linux"
metadata = "Metadados"
mobile = "Celular"
opsec = "Opsec"
qubes = "Qubes"
tails = "Tails"
windows = "Windows"

[languages.ru]
generate_feed = true
taxonomies = [
	{name = "categories"},
	{name = "tags"},
]
title = "AnarSec"

[languages.ru.translations]
language_name = "ru"
language_selection = "Язык"
search = "Поиск"
published = "Опубликовано"
edited = "Последняя редакция"
letter = "Буклет в формате письма"
a4 = "Буклет в формате А4"
read = "Читать еще"
contents = "Оглавление"
categories = "Категории"
defensive = "Защита"
offensive = "Нападение"
tags = "Тэги"
beginner = "для начинающих"
e2ee = "Шифрование"
intermediate = "Средний уровень"
intro = "Введение"
linux = "Linux"
metadata = "Метаданные"
mobile = "Мобильные устройства"
opsec = "Операционная безопасность"
qubes = "Qubes"
tails = "Tails"
windows = "Windows"

[extra]
navbar_items = [
    { code = "en", nav_items = [
        { url = "$BASE_URL/posts/", name = "Guides" },
        { url = "$BASE_URL/series/", name = "Series" },
        { url = "$BASE_URL/recommendations/", name = "Recommendations" },
        { url = "$BASE_URL/glossary/", name = "Glossary" },
        { url = "$BASE_URL/contact/", name = "Contact" },
    ] },
    { code = "fr", nav_items = [
        { url = "$BASE_URL/fr/posts/", name = "Guides" },
        { url = "$BASE_URL/fr/series/", name = "Séries" },
        { url = "$BASE_URL/fr/recommendations/", name = "Recommendations" },
        { url = "$BASE_URL/fr/glossary/", name = "Glossaire" },
        { url = "$BASE_URL/fr/contact/", name = "Contact" },
    ] },
    { code = "el", nav_items = [
        { url = "$BASE_URL/el/posts/", name = "Οδηγοί" },
        { url = "$BASE_URL/el/series/", name = "Σειρές" },
        { url = "$BASE_URL/el/recommendations/", name = "Προτάσεις" },
        { url = "$BASE_URL/el/glossary/", name = "Γλωσσάρι" },
        { url = "$BASE_URL/el/contact/", name = "Επικοινωνία" },
    ] },
    { code = "pt", nav_items = [
        { url = "$BASE_URL/pt/posts/", name = "Guias" },
        { url = "$BASE_URL/pt/series/", name = "Séries" },
        { url = "$BASE_URL/pt/recommendations/", name = "Recomendações" },
        { url = "$BASE_URL/pt/glossary/", name = "Glossário" },
        { url = "$BASE_URL/pt/contact/", name = "Contato" },
    ] },
    { code = "ru", nav_items = [
        { url = "$BASE_URL/ru/posts/", name = "Пособия" },
        { url = "$BASE_URL/ru/series/", name = "Серии" },
        { url = "$BASE_URL/ru/recommendations/", name = "Рекомендации" },
        { url = "$BASE_URL/ru/glossary/", name = "Словарь" },
        { url = "$BASE_URL/ru/contact/", name = "Контакты" },
    ] }
]

[extra.author]
name = "AnarSec"
avatar = "/gifs/gay.gif"

[extra.social]
