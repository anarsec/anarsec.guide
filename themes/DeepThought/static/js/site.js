"use strict";

  const menuBarHeight = document.querySelector("nav.navbar").clientHeight;
  const tocItems = document.querySelectorAll(".toc");
  const navSections = new Array(tocItems.length);

  tocItems.forEach((el, i) => {
    let id = el.getAttribute("id").substring(5);
    navSections[i] = document.getElementById(id);
  })

  function isVisible(tocIndex) {
    const current = navSections[tocIndex];
    const next = tocIndex < tocItems.length - 1 ? navSections[tocIndex + 1] : document.querySelector("section.section").nextElementSibling;

    const c = current.getBoundingClientRect();
    const n = next.getBoundingClientRect();
    const h = (window.innerHeight || document.documentElement.clientHeight);

    return (c.top <= h) && (n.top - menuBarHeight >= 0);
  }

  function highlightTocItem(tocIndex) {
  const bottomGap = 50;
  tocItems[tocIndex].classList.add('is-active');
  const bodyRect = document.body.getBoundingClientRect();
  const elemRect = tocItems[tocIndex].getBoundingClientRect();
  const menuRect = document.querySelector(".menu").getBoundingClientRect();
  const menuAbsTop = menuRect.top - bodyRect.top;
  const elemAbsBottom = elemRect.bottom - bodyRect.top;
  const h = (window.innerHeight || document.documentElement.clientHeight);
  document.querySelector(".menu").style = "position: sticky; top: 48px;";
  const minScroll = tocItems[tocIndex].offsetTop - document.querySelector(".menu").clientHeight + tocItems[tocIndex].offsetHeight + 50;
  const maxScroll = tocItems[tocIndex].offsetTop - tocItems[tocIndex].offsetHeight - 50;
  if(document.querySelector(".menu").scrollTop < minScroll){
    document.querySelector(".menu").scrollTop = minScroll;
  }
  else if(document.querySelector(".menu").scrollTop > maxScroll){
    document.querySelector(".menu").scrollTop = maxScroll;
  }
}

  function activateIfVisible() {
     let b = true;
     for (let i = 0; i < tocItems.length; i++) {
      if (b && isVisible(i)) {
	highlightTocItem(i);
        b = false;
      } else
        tocItems[i].classList.remove('is-active');
    }
  }

  var isTicking = null;
  window.addEventListener('scroll', () => {
    if (!isTicking) {
      window.requestAnimationFrame(() => {
        activateIfVisible();
        isTicking = false;
      });
      isTicking = true;
    }
  }, false);

function documentReadyCallback() {

  if (localStorage.getItem("theme") === "dark") {
    document.body.setAttribute("theme", "dark");
    document.querySelectorAll("img, picture, video, pre").forEach(img => img.setAttribute("theme", "dark"));
    document.querySelectorAll(".vimeo, .youtube, .chart").forEach(video => video.setAttribute("theme", "dark"));
    document.getElementById("dark-mode").setAttribute("title", "Switch to light theme");
  }

  document.getElementById("dark-mode").style.display = "flex";

  document.getElementById("dark-mode").addEventListener("click", () => {
    if (
      localStorage.getItem("theme") == null ||
      localStorage.getItem("theme") == "light"
    ) {
      localStorage.setItem("theme", "dark");
      document.body.setAttribute("theme", "dark");
      document.querySelectorAll("img, picture, video, pre").forEach(img => img.setAttribute("theme", "dark"));
      document.querySelectorAll(".vimeo, .youtube, .chart").forEach(video => video.setAttribute("theme", "dark"));

      document.getElementById("dark-mode").setAttribute("title", "Switch to light theme");
    } else {
      localStorage.setItem("theme", "light");
      document.body.removeAttribute("theme", "dark");
      document.querySelectorAll("img, picture, video, pre").forEach(img => img.removeAttribute("theme", "dark"))
      document.querySelectorAll(".vimeo, .youtube, .chart").forEach(video => video.removeAttribute("theme", "dark"));

      document.getElementById("dark-mode").setAttribute("title", "Switch to dark theme");
    }
  });
}

if (document.readyState === 'loading') {  // Loading hasn't finished yet
  document.addEventListener('DOMContentLoaded', documentReadyCallback);
} else {
  documentReadyCallback();
}
